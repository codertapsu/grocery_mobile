import 'package:flutter/material.dart';
import 'package:grocery_mobile/helpers/color_helper.dart';
// import 'package:hexcolor/hexcolor.dart';

class AppColors {
  static final AppColors _instance = AppColors._internal();
  factory AppColors() => _instance;

  AppColors._internal();

  static const primaryColor = Color(0xff53B175);
  static const errorColor = Color(0xFFFF2020);
  static const darkGrey = Color(0xff7C7C7C);

  static final Color kPrimaryColor = ExtendedColor.fromHex('#53B175');
  static final Color kShadowColor = ExtendedColor.fromHex('#A8A8A8');
  static final Color kBlackColor = ExtendedColor.fromHex('#181725');
  static final Color kSubtitleColor = ExtendedColor.fromHex('#7C7C7C');
  static final Color kSecondaryColor = ExtendedColor.fromHex('#F2F3F2');
  static final Color kBorderColor = ExtendedColor.fromHex('#E2E2E2');

  static final TextStyle kTitleStyle = TextStyle(
    fontSize: 15,
    fontWeight: FontWeight.bold,
    color: kBlackColor,
  );

  static final TextStyle kDescriptionStyle = TextStyle(
    color: kSubtitleColor,
    fontSize: 13,
  );
}
