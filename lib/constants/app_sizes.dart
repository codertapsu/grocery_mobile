import 'package:flutter/material.dart';

class AppSizes {
  static final AppSizes _instance = AppSizes._internal();
  factory AppSizes() => _instance;

  AppSizes._internal();

  static const appBarHeight = 60.00;
  static const adsBannerHeight = 150.00;
}
