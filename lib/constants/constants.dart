import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

const defaultPadding = 20.0;
const cartBarHeight = 100.0;
const headerHeight = 85.0;

const bgColor = Color(0xFFF6F5F2);
const primaryColor = Color(0xFF40A944);

const panelTransition = Duration(milliseconds: 500);

const kTitleKey = Key('FLUTTER_LOGIN_TITLE');
const kRecoverPasswordIntroKey = Key('RECOVER_PASSWORD_INTRO');
const kRecoverPasswordDescriptionKey = Key('RECOVER_PASSWORD_DESCRIPTION');
const kDebugToolbarKey = Key('DEBUG_TOOLBAR');

const kMinLogoHeight = 50.0; // hide logo if less than this
const kMaxLogoHeight = 125.0;
