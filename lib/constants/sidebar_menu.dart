import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:grocery_mobile/models/sidebar_menu_item_model.dart';

List<SidebarMenuItem> sideBarMenuItems = [
  SidebarMenuItem(
    title: 'Shop',
    icon: FontAwesomeIcons.store,
    isSelected: true,
  ),
  SidebarMenuItem(
    icon: FontAwesomeIcons.search,
    title: 'Explore',
  ),
  SidebarMenuItem(
    icon: FontAwesomeIcons.solidHeart,
    title: 'Favorite',
  ),
  SidebarMenuItem(
    icon: FontAwesomeIcons.userAlt,
    title: 'Profile',
  ),
];
