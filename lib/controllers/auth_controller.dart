import 'package:get/get.dart';
import 'package:grocery_mobile/models/auth_model.dart';

class AuthController extends GetxController {
  RxBool isOpenAuthForm = false.obs;
  Rx<AuthMode> authMode = AuthMode.signIn.obs;
  Rx<AuthType> authType = AuthType.userPassword.obs;

  void openAuthForm() {
    isOpenAuthForm.value = true;
  }

  void closeAuthForm() {
    isOpenAuthForm.value = false;
  }

  void changeAuthMode(AuthMode mode) {
    authMode.value = mode;
  }

  void changeAuthType(AuthType type) {
    authType.value = type;
  }

  Future<AuthenticatedUser> signInWithEmailAndPassword(
    String email,
    String password,
  ) async {
    await Future.delayed(Duration(seconds: 2));
    return AuthenticatedUser();
  }

  Future<AuthenticatedUser> signUpWithEmailAndPassword(
    String email,
    String password,
  ) async {
    await Future.delayed(Duration(seconds: 2));
    return AuthenticatedUser();
  }

  Future<AuthenticatedUser> signInWithPhoneAndPassword(
    String phone,
    String password,
  ) async {
    await Future.delayed(Duration(seconds: 2));
    return AuthenticatedUser();
  }

  Future<AuthenticatedUser> signUpWithPhoneAndPassword(
    String phone,
    String password,
  ) async {
    await Future.delayed(Duration(seconds: 2));
    return AuthenticatedUser();
  }

  Future<bool> recoverPasswordByEmail(String email) async {
    await Future.delayed(Duration(seconds: 2));
    return true;
  }

  Future<bool> recoverPasswordByPhone(String phone) async {
    await Future.delayed(Duration(seconds: 2));
    return true;
  }

  Future<AuthenticatedUser> confirmAuthCode(
    String code,
  ) async {
    await Future.delayed(Duration(seconds: 2));
    return AuthenticatedUser();
  }
}
