import 'package:flutter/material.dart';
import 'package:grocery_mobile/models/auth_model.dart';
import 'package:grocery_mobile/models/term_of_service.dart';

class AuthenticationController with ChangeNotifier {
  AuthenticationController({
    // this.onLogin,
    // this.onSignup,
    // this.onRecoverPassword,
    // this.onConfirmRecover,
    // this.onConfirmSignup,
    // this.onResendCode,
    String email = '',
    String password = '',
    String confirmPassword = '',
    AuthMode initialAuthMode = AuthMode.signIn,
    this.termsOfService = const [],
  })  : _email = email,
        _password = password,
        _confirmPassword = confirmPassword,
        _mode = initialAuthMode;

  // final LoginCallback? onLogin;
  // final SignupCallback? onSignup;
  // final RecoverCallback? onRecoverPassword;
  // final ConfirmRecoverCallback? onConfirmRecover;
  // final ConfirmSignupCallback? onConfirmSignup;
  // final SignupCallback? onResendCode;
  final List<TermOfService> termsOfService;

  AuthType _authType = AuthType.userPassword;

  /// Used to decide if the login/signup comes from a provider or normal login
  AuthType get authType => _authType;
  set authType(AuthType authType) {
    _authType = authType;
    notifyListeners();
  }

  AuthMode _mode = AuthMode.signIn;
  AuthMode get mode => _mode;
  set mode(AuthMode value) {
    _mode = value;
    notifyListeners();
  }

  bool get isLogin => _mode == AuthMode.signIn;
  bool get isSignup => _mode == AuthMode.signUp;
  int currentCardIndex = 0;

  AuthMode opposite() {
    return _mode == AuthMode.signIn ? AuthMode.signUp : AuthMode.signIn;
  }

  AuthMode switchAuth() {
    if (mode == AuthMode.signIn) {
      mode = AuthMode.signUp;
    } else if (mode == AuthMode.signUp) {
      mode = AuthMode.signIn;
    }
    return mode;
  }

  String _email = '';
  String get email => _email;
  set email(String email) {
    _email = email;
    notifyListeners();
  }

  String _password = '';
  String get password => _password;
  set password(String password) {
    _password = password;
    notifyListeners();
  }

  String _confirmPassword = '';
  String get confirmPassword => _confirmPassword;
  set confirmPassword(String confirmPassword) {
    _confirmPassword = confirmPassword;
    notifyListeners();
  }

  Map<String, String>? _additionalSignupData;
  Map<String, String>? get additionalSignupData => _additionalSignupData;
  set additionalSignupData(Map<String, String>? additionalSignupData) {
    _additionalSignupData = additionalSignupData;
    notifyListeners();
  }

  List<TermOfServiceResult> getTermsOfServiceResults() {
    return termsOfService
        .map((e) => TermOfServiceResult(term: e, accepted: e.getStatus()))
        .toList();
  }
}
