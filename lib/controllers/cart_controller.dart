import 'dart:async';

import 'package:get/get.dart';
import 'package:grocery_mobile/models/cart_item_model.dart';
import 'package:grocery_mobile/models/product_model.dart';
import 'package:uuid/uuid.dart';

class CartController extends GetxController with StateMixin<List<CartItem>> {
  RxBool isAddLoading = false.obs;

  RxInt get length$ => value!.length.obs;

  int get totalCartItems => value!
      .fold(0, (previousValue, element) => previousValue + element.quantity);

  @override
  void onInit() {
    super.onInit();
    change([], status: RxStatus.success());
  }

  void removeItem(String productId) {
    value!.removeWhere((element) => element.productId == productId);
    update();
  }

  void clear() {
    change([]);
  }

  void addToCart(Product product, int quantity) {
    isAddLoading.value = true;
    update();
    try {
      if (_isItemAlreadyAdded(product.id)) {
        var item = value!
            .firstWhereOrNull((element) => element.productId == product.id);
        if (item != null) {
          item.quantity = quantity;
          notifyChildrens();
          refresh();
          Get.snackbar("Check your cart", "${product.title} is already added");
        }
      } else {
        // value!.add(
        //   CartItem.fromJson({
        //     'id': Uuid().v1(),
        //     'productId': product.id,
        //     'quantity': quantity,
        //   }),
        // );
        // isAddLoading.value = false;
        // notifyChildrens();
        // update();
        // Get.snackbar("Item added", "${product.title} was added to your cart");
        // --------------------------------------------------
        // Timer(const Duration(seconds: 5), () {
        //   value!.add(
        //     CartItem.fromJson({
        //       'id': Uuid().v1(),
        //       'productId': product.id,
        //       'quantity': quantity,
        //     }),
        //   );
        //   isAddLoading.value = false;
        //   update();
        //   Get.snackbar("Item added", "${product.title} was added to your cart");
        // });
        value!.add(
          CartItem.fromJson({
            'id': Uuid().v1(),
            'productId': product.id,
            'quantity': quantity,
          }),
        );
        isAddLoading.value = false;
        update();
      }
    } catch (e) {
      Get.snackbar("Error", "Cannot add this item");
    }
  }

  bool _isItemAlreadyAdded(String productId) {
    return value!.where((item) => item.productId == productId).isNotEmpty;
  }
}
