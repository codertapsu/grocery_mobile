import 'package:get/get.dart';
import 'package:grocery_mobile/mock/products.dart';
import 'package:grocery_mobile/models/category_model.dart';

class CategoryController extends GetxController
    with StateMixin<List<Category>> {
  @override
  void onInit() {
    change(generateDemoCategory(), status: RxStatus.success());
    super.onInit();
  }
}
