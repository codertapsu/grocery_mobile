import 'package:get/get.dart';

enum CartViewState { normal, cart }

const String additionalTag = '_cartTag';

class LayoutController extends GetxController {
  RxInt currentMenuIndex = 0.obs;
  RxBool isOpenMenu = false.obs;
  Rx<CartViewState> cartViewState = CartViewState.normal.obs;

  void toggleMenu() {
    isOpenMenu.value = !isOpenMenu.value;
  }

  void closeMenu() {
    isOpenMenu.value = false;
  }

  void openMenu() {
    isOpenMenu.value = true;
  }

  void changeHomeState(CartViewState state) {
    if (cartViewState.value != state) {
      cartViewState.value = state;
    }
  }

  void selectMenu(int index) {
    if (currentMenuIndex.value != index) {
      currentMenuIndex.value = index;
    }
    closeMenu();
  }
}
