import 'package:get/get.dart';
import 'package:grocery_mobile/mock/products.dart';
import 'package:grocery_mobile/models/product_model.dart';
import 'package:grocery_mobile/models/review_model.dart';

class ProductController extends GetxController with StateMixin<List<Product>> {
// final repository;
  @override
  void onInit() {
    super.onInit();
    change(generateDemoProduct(), status: RxStatus.success());
    // this.repository.getData().then((resp) {
    //   change(resp, status: RxStatus.success());
    // }, onError: (err) {
    //   print(err);
    //   change(
    //     null,
    //     status: RxStatus.error('Error get data'),
    //   );
    //   super.onInit();
    // });
  }

  void markFavorite(Product product) {
    product.isFavorite = true;
    refresh();
  }

  void markUnFavorite(Product product) {
    product.isFavorite = false;
    refresh();
  }

  void updateRating(Product product, double rating) {
    product.rating = rating;
    refresh();
  }

  Future<bool> addReview(Product product, String comment, double rating) async {
    product.reviews.add(Review(
      id: '${product.reviews.length + 1}',
      reviewBy: '${product.title}',
      comment: comment,
      rating: rating,
    ));
    refresh();
    await Future.delayed(Duration(seconds: 2));
    return true;
  }
}
