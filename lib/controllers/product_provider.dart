import 'dart:collection';
import 'package:flutter/material.dart';
import 'package:grocery_mobile/mock/products.dart';
import 'package:grocery_mobile/models/cart_item_model.dart';
import 'package:grocery_mobile/models/product_model.dart';

class ProductsOperationsController extends ChangeNotifier {
  List<CartItem> _shoppingCart = [];
  late VoidCallback onCheckOutCallback;

  void onCheckOut({required VoidCallback onCheckOutCallback}) {
    this.onCheckOutCallback = onCheckOutCallback;
  }

  UnmodifiableListView<Product> get productsInStock {
    return UnmodifiableListView(generateDemoProduct());
  }

  UnmodifiableListView<CartItem> get cart {
    return UnmodifiableListView(_shoppingCart);
  }

  void addProductToCart(int index, {int bulkOrder = 0}) {
    bool inCart = false;
    int indexInCard = 0;
    if (_shoppingCart.length != 0) {
      // for (int i = 0; i < _shoppingCart.length; i++) {
      //   if (_shoppingCart[i].name == _productsInStock[index].name &&
      //       _shoppingCart[i].picPath == _productsInStock[index].picPath) {
      //     indexInCard = i;
      //     inCart = true;
      //     break;
      //   }
      // }
    }
    if (inCart == false) {
      // _shoppingCart.add(
      //   Product(
      //     name: _productsInStock[index].name,
      //     picPath: _productsInStock[index].picPath,
      //     price: _productsInStock[index].price,
      //     weight: _productsInStock[index].weight,
      //     orderedQuantity:
      //         _productsInStock[index].orderedQuantity + (bulkOrder - 1),
      //   ),
      // );
      notifyListeners();
    } else {
      // _shoppingCart[indexInCard].makeOrder(bulkOrder: bulkOrder);
      notifyListeners();
    }
  }

  double _totalCost = 0.00;
  void returnTotalCost() {
    if (_totalCost == 0) {
      for (int i = 0; i < _shoppingCart.length; i++) {
        // _totalCost += (double.parse(_shoppingCart[i]
        //         .product
        //         .price
        //         .toString()
        //         .replaceAll('\$', '')) *
        //     _shoppingCart[i].quantity);
      }
      notifyListeners();
    } else {
      _totalCost = 0.0;
      for (int i = 0; i < _shoppingCart.length; i++) {
        // _totalCost += (double.parse(_shoppingCart[i]
        //         .product
        //         .price
        //         .toString()
        //         .replaceAll('\$', '')) *
        //     _shoppingCart[i].quantity);
      }
      notifyListeners();
    }
  }

  void deleteFromCart(int index) {
    _shoppingCart.removeAt(index);
    notifyListeners();
  }

  double get totalCost {
    return double.parse(_totalCost.toStringAsExponential(3));
  }

  void clearCart() {
    _shoppingCart.clear();
    onCheckOutCallback();
    notifyListeners();
  }
}
