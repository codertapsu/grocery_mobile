import 'package:get/get.dart';
import 'package:grocery_mobile/mock/products.dart';
import 'package:grocery_mobile/models/product_model.dart';

class ProductRecommendationController extends GetxController
    with StateMixin<List<Product>> {
// final repository;
  @override
  void onInit() {
    change(_generateRecommendation(), status: RxStatus.success());
    super.onInit();
  }

  List<Product> _generateRecommendation() {
    return generateDemoProduct();
  }
}
