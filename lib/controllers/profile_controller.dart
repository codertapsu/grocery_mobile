import 'package:get/get.dart';
import 'package:grocery_mobile/models/address_model.dart';

class ProfileController extends GetxController {
  Rx<List<Address>> addresses$ = Rx<List<Address>>([]);

  @override
  void onInit() {
    addresses$.value = [
      Address(
        name: 'Office',
        id: '1',
        country: 'Vietnam',
        city: 'Hanoi',
        district: 'Quan 1',
        ward: 'Phuong 1',
        street: 'Phuong 1',
        isDefault: true,
      ),
      Address(
        name: 'Home',
        id: '2',
        country: 'Vietnam',
        city: 'Hanoi',
        district: 'Quan 2',
        ward: 'Phuong 2',
        street: 'Phuong 2',
        isDefault: false,
      ),
      Address(
        name: 'Home 2',
        id: '3',
        country: 'Vietnam',
        city: 'Hanoi',
        district: 'Quan 3',
        ward: 'Phuong 3',
        street: 'Phuong 3',
        isDefault: false,
      ),
      Address(
        name: 'Home 3',
        id: '4',
        country: 'Vietnam',
        city: 'Hanoi',
        district: 'Quan 4',
        ward: 'Phuong 4',
        street: 'Phuong 4',
        isDefault: false,
      ),
      Address(
        name: 'Parent home',
        id: '5',
        country: 'Vietnam',
        city: 'Hanoi',
        district: 'Quan 5',
        ward: 'Phuong 5',
        street: 'Phuong 5',
        isDefault: false,
      ),
    ];
    super.onInit();
  }

  void markAddressAsDefault(Address address) {
    for (var i = 0; i < addresses$.value.length; i++) {
      if (addresses$.value[i].id == address.id) {
        addresses$.value[i].isDefault = true;
      } else {
        addresses$.value[i].isDefault = false;
      }
    }
    refresh();
  }

  void removeAddress(Address address) {
    addresses$.value.removeWhere((element) => element.id == address.id);
    refresh();
  }

  void addNewAddress(Map<String, dynamic> json) {
    json['id'] = addresses$.value.length.toString();
    var newAddress = Address.fromJson(json);
    addresses$.value.add(newAddress);
    refresh();
  }

  void updateAddress(Address address) {
    for (var i = 0, length = addresses$.value.length; i < length; i++) {
      if (addresses$.value[i].id == address.id) {
        addresses$.value[i] = address;
        break;
      }
    }
    refresh();
  }
}
