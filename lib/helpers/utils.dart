class Utils {
  static bool isNullOrEmpty(String? value) => value == '' || value == null;
}

// ThemeData _mergeTheme(
//     {required ThemeData theme, required SignInTheme signInTheme}) {
//   final blackOrWhite =
//       theme.brightness == Brightness.light ? Colors.black54 : Colors.white;
//   final primaryOrWhite =
//       theme.brightness == Brightness.light ? theme.primaryColor : Colors.white;
//   final originalPrimaryColor = signInTheme.primaryColor ?? theme.primaryColor;
//   final primaryDarkShades = getDarkShades(originalPrimaryColor);
//   final primaryColor = primaryDarkShades.length == 1
//       ? lighten(primaryDarkShades.first!)
//       : primaryDarkShades.first;
//   final primaryColorDark = primaryDarkShades.length >= 3
//       ? primaryDarkShades[2]
//       : primaryDarkShades.last;
//   final accentColor = signInTheme.accentColor ?? theme.colorScheme.secondary;
//   final errorColor = signInTheme.errorColor ?? theme.errorColor;
//   // the background is a dark gradient, force to use white text if detect default black text color
//   final isDefaultBlackText = theme.textTheme.headline3!.color ==
//       Typography.blackMountainView.headline3!.color;
//   final titleStyle = theme.textTheme.headline3!
//       .copyWith(
//         color: signInTheme.accentColor ??
//             (isDefaultBlackText
//                 ? Colors.white
//                 : theme.textTheme.headline3!.color),
//         fontSize: signInTheme.beforeHeroFontSize,
//         fontWeight: FontWeight.w300,
//       )
//       .merge(signInTheme.titleStyle);
//   final footerStyle = theme.textTheme.bodyText1!
//       .copyWith(
//         color: signInTheme.accentColor ??
//             (isDefaultBlackText
//                 ? Colors.white
//                 : theme.textTheme.headline3!.color),
//       )
//       .merge(signInTheme.footerTextStyle);
//   final textStyle = theme.textTheme.bodyText2!
//       .copyWith(color: blackOrWhite)
//       .merge(signInTheme.bodyStyle);
//   final textFieldStyle = theme.textTheme.subtitle1!
//       .copyWith(color: blackOrWhite, fontSize: 14)
//       .merge(signInTheme.textFieldStyle);
//   final buttonStyle = theme.textTheme.button!
//       .copyWith(color: Colors.white)
//       .merge(signInTheme.buttonStyle);
//   final cardTheme = signInTheme.cardTheme;
//   final inputTheme = signInTheme.inputTheme;
//   final roundBorderRadius = BorderRadius.circular(100);

//   // signInThemeHelper.loginTextStyle = titleStyle;

//   TextStyle labelStyle;

//   if (signInTheme.primaryColorAsInputLabel) {
//     labelStyle = TextStyle(color: primaryColor);
//   } else {
//     labelStyle = TextStyle(color: blackOrWhite);
//   }

//   return theme.copyWith(
//     primaryColor: primaryColor,
//     primaryColorDark: primaryColorDark,
//     errorColor: errorColor,
//     cardTheme: theme.cardTheme.copyWith(
//       clipBehavior: cardTheme.clipBehavior,
//       color: cardTheme.color ?? theme.cardColor,
//       elevation: cardTheme.elevation ?? 12.0,
//       margin: cardTheme.margin ?? const EdgeInsets.all(4.0),
//       shape: cardTheme.shape ??
//           RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
//     ),
//     inputDecorationTheme: theme.inputDecorationTheme.copyWith(
//       filled: inputTheme.filled,
//       fillColor: inputTheme.fillColor ??
//           Color.alphaBlend(
//             primaryOrWhite.withOpacity(.07),
//             Colors.grey.withOpacity(.04),
//           ),
//       contentPadding: inputTheme.contentPadding ??
//           const EdgeInsets.symmetric(vertical: 4.0),
//       errorStyle: inputTheme.errorStyle ?? TextStyle(color: errorColor),
//       labelStyle: inputTheme.labelStyle ?? labelStyle,
//       enabledBorder: inputTheme.enabledBorder ??
//           inputTheme.border ??
//           OutlineInputBorder(
//             borderSide: const BorderSide(color: Colors.transparent),
//             borderRadius: roundBorderRadius,
//           ),
//       focusedBorder: inputTheme.focusedBorder ??
//           inputTheme.border ??
//           OutlineInputBorder(
//             borderSide: BorderSide(color: primaryColor!, width: 1.5),
//             borderRadius: roundBorderRadius,
//           ),
//       errorBorder: inputTheme.errorBorder ??
//           inputTheme.border ??
//           OutlineInputBorder(
//             borderSide: BorderSide(color: errorColor),
//             borderRadius: roundBorderRadius,
//           ),
//       focusedErrorBorder: inputTheme.focusedErrorBorder ??
//           inputTheme.border ??
//           OutlineInputBorder(
//             borderSide: BorderSide(color: errorColor, width: 1.5),
//             borderRadius: roundBorderRadius,
//           ),
//       disabledBorder: inputTheme.disabledBorder ??
//           inputTheme.border ??
//           OutlineInputBorder(
//             borderSide: const BorderSide(color: Colors.transparent),
//             borderRadius: roundBorderRadius,
//           ),
//     ),
//     floatingActionButtonTheme: theme.floatingActionButtonTheme.copyWith(
//       backgroundColor: primaryColor,
//       splashColor: theme.colorScheme.secondary,
//       elevation: 4.0,
//       highlightElevation: 2.0,
//       shape: StadiumBorder(),
//     ),
//     highlightColor: theme.highlightColor,
//     textTheme: theme.textTheme.copyWith(
//       headline3: titleStyle,
//       bodyText2: textStyle,
//       subtitle1: textFieldStyle,
//       subtitle2: footerStyle,
//       button: buttonStyle,
//     ),
//     colorScheme: Theme.of(context).colorScheme.copyWith(secondary: accentColor),
//   );
// }
