class Regex {
  // https://stackoverflow.com/a/32686261/9449426
  static final email = RegExp(r'^[^\s@]+@[^\s@]+\.[^\s@]+$');
}

class Validators {
  static String? defaultEmailValidator(value) {
    if (value!.isEmpty || !Regex.email.hasMatch(value)) {
      return 'Invalid email!';
    }
    return null;
  }

  static String? defaultPasswordValidator(value) {
    if (value!.isEmpty || value.length <= 2) {
      return 'Password is too short!';
    }
    return null;
  }
}
