import 'package:flutter/material.dart';
import 'package:get/get.dart';

Size? getWidgetSize(GlobalKey key) {
  final renderBox = key.currentContext?.findRenderObject() as RenderBox?;
  return renderBox?.size;
}

showSuccessToast(BuildContext context, String title, String message,
    [Duration? duration]) {
  return Get.bottomSheet(
    Container(
      child: Padding(
        padding: EdgeInsets.all(8.0),
        child: Column(
          children: [
            ListTile(
              title: Text('Option 1'),
              trailing: Icon(Icons.access_alarm),
            )
          ],
        ),
      ),
    ),
    elevation: 20.0,
    enableDrag: false,
    backgroundColor: Colors.white,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(20.0),
        topRight: Radius.circular(20.0),
      ),
    ),
  );
}

showErrorToast(BuildContext context, String title, String message) {
  return Get.bottomSheet(
    Container(
      child: Padding(
        padding: EdgeInsets.all(8.0),
        child: Column(
          children: [
            ListTile(
              title: Text('Option 1'),
              trailing: Icon(Icons.access_alarm),
            )
          ],
        ),
      ),
    ),
    elevation: 20.0,
    enableDrag: false,
    backgroundColor: Colors.white,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(20.0),
        topRight: Radius.circular(20.0),
      ),
    ),
  );
}
