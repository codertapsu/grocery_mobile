import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:grocery_mobile/controllers/controllers.dart';

import 'app.dart';

Future<void> initialize() async {
  WidgetsFlutterBinding.ensureInitialized();
  Get.put(GetStorage());
  Get.put(LayoutController());
  Get.put(CategoryController());
  Get.put(ProductController());
  Get.put(CartController());
  Get.put(AuthController());
  Get.put(ProfileController());
  await GetStorage.init();
  await SystemChrome.setPreferredOrientations(
    [
      DeviceOrientation.portraitUp,
      // DeviceOrientation.portraitDown,
    ],
  );
}

void main() async {
  await initialize();
  runApp(const MyApp());
}
