import 'dart:math';

import 'package:grocery_mobile/models/category_model.dart';
import 'package:grocery_mobile/models/product_model.dart';
import 'package:uuid/uuid.dart';

import 'dart:math';

const _chars = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
Random _rnd = Random();

String getRandomString(int length) => String.fromCharCodes(
      Iterable.generate(
        length,
        (_) => _chars.codeUnitAt(
          _rnd.nextInt(_chars.length),
        ),
      ),
    );

int randomPrice() {
  Random random = Random();
  return random.nextInt(500);
}

var productImages = [
  "assets/images/grocery_images/apple.png",
  "assets/images/grocery_images/banana.png",
  "assets/images/grocery_images/beef.png",
  "assets/images/grocery_images/chicken.png",
  "assets/images/grocery_images/ginger.png",
  "assets/images/grocery_images/pepper.png",
];

List<Product> generateDemoProduct() {
  List<Product> products = [];
  for (int i = 0; i < 30; i++) {
    var images = [...productImages]..shuffle();
    products.add(
      Product(
        id: Uuid().v1(),
        title: getRandomString(10),
        description: getRandomString(100),
        price: randomPrice().toDouble(),
        images: images,
      ),
    );
  }
  return products;
}

List<Category> generateDemoCategory() {
  List<Category> categories = [];
  for (int i = 0; i < 50; i++) {
    categories.add(
      Category(
        id: Uuid().v1(),
        name: getRandomString(20),
        image: "assets/images/grocery_images/apple.png",
      ),
    );
  }
  return categories;
}
