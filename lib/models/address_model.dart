class Address {
  String name;
  String id;
  String country;
  String city;
  String? district;
  String? ward;
  String? street;
  bool isDefault;

  Address({
    required this.name,
    required this.id,
    required this.country,
    required this.city,
    this.district,
    this.ward,
    this.street,
    this.isDefault = false,
  });

  factory Address.fromJson(Map<String, dynamic> json) {
    return Address(
      name: json["name"],
      id: json["id"],
      country: json["country"],
      city: json["city"],
      district: json["district"],
      ward: json["ward"],
      street: json["street"],
      isDefault: json["isDefault"] ?? false,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "name": name,
      "id": id,
      "country": country,
      "city": city,
      "district": district,
      "ward": ward,
      "street": street,
      "isDefault": isDefault,
    };
  }
}
