import 'package:flutter/material.dart';

enum AuthMode { signUp, signIn }

enum AuthType { provider, userPassword }

class AuthenticatedUser {
  late String? id;
  late String? email;
  late String? name;
  late String? photoUrl;
  late String? providerId;
  late String? provider;
  late String? token;

  AuthenticatedUser({
    this.id,
    this.email,
    this.name,
    this.photoUrl,
    this.providerId,
    this.provider,
    this.token,
  });

  factory AuthenticatedUser.fromJson(Map<String, dynamic> json) {
    return AuthenticatedUser(
      id: json['id'],
      email: json['email'],
      name: json['name'],
      photoUrl: json['photoUrl'],
      providerId: json['providerId'],
      provider: json['provider'],
      token: json['token'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'email': email,
      'name': name,
      'photoUrl': photoUrl,
      'providerId': providerId,
      'provider': provider,
      'token': token,
    };
  }
}
