import 'package:flutter/material.dart';

class CartItem {
  late String id;
  late String productId;
  late int quantity;

  CartItem({Key? key, this.quantity = 1, required this.productId});

  void increment() {
    quantity++;
  }

  CartItem.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    productId = json['productId'];
    quantity = json['quantity'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['productId'] = productId;
    data['quantity'] = quantity;

    return data;
  }
}
