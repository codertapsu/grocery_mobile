class Category {
  final String name;
  final String image;
  final String id;
  final String description;

  Category({
    required this.name,
    required this.image,
    required this.id,
    this.description = '',
  });

  factory Category.fromJson(Map<String, dynamic> json) {
    return Category(
      name: json['name'],
      image: json['image'],
      id: json['id'],
      description: json['description'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['name'] = name;
    data['image'] = image;
    data['id'] = id;
    data['description'] = description;

    return data;
  }
}
