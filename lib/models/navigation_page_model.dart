import 'package:flutter/material.dart';

class NavigationPage {
  String title;
  IconData icon;
  bool isSelected;
  Widget page;

  NavigationPage({
    required this.title,
    required this.icon,
    required this.page,
    this.isSelected = false,
  });
}
