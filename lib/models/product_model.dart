import 'package:flutter/material.dart';
import 'package:grocery_mobile/models/review_model.dart';

class Product {
  late List<Color> colors;
  List<String>? images;
  late String description;
  late String title;
  late String id;
  late bool isFavorite;
  late bool isPopular;
  late double price;
  late double rating;
  late double averageRating;
  late List<Review> reviews;

  Product({
    required this.price,
    required this.title,
    this.images,
    this.id = '',
    this.colors = const [],
    this.description = '',
    this.isFavorite = false,
    this.isPopular = false,
    this.rating = 5.0,
    this.averageRating = 3.5,
    this.reviews = const [],
  });

  Product.fromJson(Map<String, dynamic> json) {
    colors = json['colors'];
    description = json['description'];
    id = json['id'];
    images = json['images'];
    isFavorite = json['isFavorite'];
    isPopular = json['isPopular'];
    price = json['price'];
    title = json['title'];
    rating = json['rating'];
    averageRating = json['averageRating'];
    reviews = (json['reviews'] as List<dynamic>)
        .map((e) => Review.fromJson(e))
        .toList();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['colors'] = colors;
    data['description'] = description;
    data['id'] = id;
    data['images'] = images;
    data['isFavorite'] = isFavorite;
    data['isPopular'] = isPopular;
    data['price'] = price;
    data['title'] = title;
    data['rating'] = rating;
    data['averageRating'] = averageRating;
    data['reviews'] = reviews;

    return data;
  }
}
