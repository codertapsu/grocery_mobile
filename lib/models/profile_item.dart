import 'package:flutter/foundation.dart';

class ProfileItem {
  String imagePath;
  String title;
  String description;
  VoidCallback onTap;

  ProfileItem({
    required this.imagePath,
    required this.title,
    required this.description,
    required this.onTap,
  });
}
