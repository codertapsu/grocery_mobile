class Review {
  late String? id;
  late String? reviewBy;
  late String? comment;
  late double? rating;

  Review({
    this.id,
    this.reviewBy,
    this.comment,
    this.rating,
  });

  Review.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    reviewBy = json['reviewBy'];
    comment = json['comment'];
    rating = json['rating'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['reviewBy'] = reviewBy;
    data['comment'] = comment;
    data['rating'] = rating;
    return data;
  }
}
