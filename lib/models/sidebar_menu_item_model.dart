import 'package:flutter/widgets.dart';

class SidebarMenuItem {
  String title;
  IconData icon;
  bool isSelected;

  SidebarMenuItem({
    required this.title,
    required this.icon,
    this.isSelected = false,
  });
}
