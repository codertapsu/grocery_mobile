import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:grocery_mobile/controllers/controllers.dart';
import 'package:grocery_mobile/widgets/animated_text_form_field.dart';
import 'package:grocery_mobile/widgets/buttons/loading_indicator_button.dart';
import 'package:grocery_mobile/widgets/buttons/primary_button.dart';
import 'package:grocery_mobile/widgets/text_fields/animated_text_fields.dart';

class AddressFormScreen extends StatefulWidget {
  AddressFormScreen({Key? key}) : super(key: key);

  @override
  State<AddressFormScreen> createState() => _AddressFormScreenState();
}

class _AddressFormScreenState extends State<AddressFormScreen>
    with TickerProviderStateMixin {
  final _profileController = Get.find<ProfileController>();
  final _formKey = GlobalKey<FormState>();
  final _nameTextFieldController = TextEditingController();
  final _cityTextFieldController = TextEditingController();
  final _districtTextFieldController = TextEditingController();
  final _wardTextFieldController = TextEditingController();
  final _addressTextFieldController = TextEditingController();
  final _phoneTextFieldController = TextEditingController();

  LoadingButtonState _submitButtonState = LoadingButtonState.init;

  void _goBack() {
    Navigator.of(context).pop();
  }

  void _saveAddress() async {
    setState(() {
      _submitButtonState = LoadingButtonState.loading;
    });
    await Future.delayed(Duration(seconds: 3));
    setState(() {
      _submitButtonState = LoadingButtonState.done;
    });
    await Future.delayed(Duration(milliseconds: 500));
    setState(() {
      _submitButtonState = LoadingButtonState.init;
    });
    _goBack();
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);
          if (!currentFocus.hasPrimaryFocus &&
              currentFocus.focusedChild != null) {
            FocusManager.instance.primaryFocus!.unfocus();
          }
        },
        child: _buildBody(),
      ),
      bottomNavigationBar: _buildBottomNavigationBar(),
    );
  }

  AppBar _buildAppBar() {
    return AppBar(
      backgroundColor: Colors.amber,
      elevation: 0,
      centerTitle: true,
      automaticallyImplyLeading: false,
      leading: IconButton(
        iconSize: 20,
        icon: const Icon(
          Icons.arrow_back_ios_new,
        ),
        onPressed: _goBack,
      ),
      title: const Text(
        'New Address',
      ),
    );
  }

  Widget _buildBody() {
    var screenWidth = MediaQuery.of(context).size.width;
    var padding = 20.0;
    var textFieldWidth = screenWidth - padding * 2;
    return Container(
      padding: EdgeInsets.all(padding),
      child: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              _buildNameTextField(textFieldWidth),
              const SizedBox(height: 20),
              _buildCityTextField(textFieldWidth),
              const SizedBox(height: 20),
              _buildDistrictTextField(textFieldWidth),
              const SizedBox(height: 20),
              _buildWardTextField(textFieldWidth),
              const SizedBox(height: 20),
              _buildAddressTextField(textFieldWidth),
              const SizedBox(height: 20),
              _buildPhoneTextField(textFieldWidth),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildBottomNavigationBar() {
    return Container(
      height: 60,
      padding: EdgeInsets.symmetric(
        horizontal: 10,
        vertical: 5,
      ),
      // child: PrimaryButton(
      //   label: 'Save Address',
      //   fontWeight: FontWeight.w600,
      //   padding: EdgeInsets.symmetric(vertical: 2),
      //   // trailingWidget: getButtonPriceWidget(),
      //   onPressed: _updateAddress,
      // ),
      child: Center(
        child: LoadingIndicatorButton(
          label: 'Save Address',
          state: _submitButtonState,
          onPressed: _saveAddress,
        ),
      ),
    );
  }

  Widget _buildNameTextField(double width) {
    return AnimatedTextField(
      labelText: 'Address title',
      width: width,
      controller: _nameTextFieldController,
    );
  }

  Widget _buildCityTextField(double width) {
    return AnimatedTextField(
      labelText: 'City',
      width: width,
      controller: _cityTextFieldController,
    );
  }

  Widget _buildDistrictTextField(double width) {
    return AnimatedTextField(
      width: width,
      controller: _districtTextFieldController,
    );
  }

  Widget _buildWardTextField(double width) {
    return AnimatedTextField(
      width: width,
      controller: _wardTextFieldController,
    );
  }

  Widget _buildAddressTextField(double width) {
    return AnimatedTextField(
      width: width,
      controller: _addressTextFieldController,
    );
  }

  Widget _buildPhoneTextField(double width) {
    return AnimatedTextField(
      width: width,
      controller: _phoneTextFieldController,
    );
  }
}
