import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:grocery_mobile/controllers/controllers.dart';
import 'package:grocery_mobile/models/address_model.dart';
import 'package:grocery_mobile/screens/address/address_form_screen.dart';

import 'widgets/address_card.dart';

class AddressScreen extends StatefulWidget {
  AddressScreen({Key? key}) : super(key: key);

  @override
  State<AddressScreen> createState() => _AddressScreenState();
}

class _AddressScreenState extends State<AddressScreen> {
  final _profileController = Get.find<ProfileController>();
  final TextEditingController _searchController = TextEditingController();
  late List<Address> _addresses;
  late int _selectedIndex;

  void _navigateToAddressFormPage(BuildContext context) {
    Navigator.push(
      context,
      PageRouteBuilder(
        transitionDuration: const Duration(milliseconds: 500),
        reverseTransitionDuration: const Duration(milliseconds: 500),
        pageBuilder: (context, animation, secondaryAnimation) {
          return FadeTransition(
            opacity: animation,
            child: AddressFormScreen(),
          );
        },
      ),
    );
  }

  void _markAddressAsDefault(Address address) {
    _profileController.markAddressAsDefault(address);
  }

  void onFocusAddress(index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  void initState() {
    _addresses = _profileController.addresses$.value;
    _selectedIndex = _addresses.indexWhere((element) => element.isDefault);
    _profileController.addresses$.listen((addresses) {
      setState(() {
        _addresses = addresses;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus &&
            currentFocus.focusedChild != null) {
          FocusManager.instance.primaryFocus!.unfocus();
        }
      },
      child: Scaffold(
        appBar: _buildAppBar(),
        body: _buildBody(),
      ),
    );
  }

  AppBar _buildAppBar() {
    return AppBar(
      backgroundColor: Colors.amber,
      elevation: 0,
      centerTitle: true,
      automaticallyImplyLeading: false,
      leading: IconButton(
        iconSize: 20,
        icon: const Icon(
          Icons.arrow_back_ios_new,
        ),
        onPressed: () => Navigator.of(context).pop(),
      ),
      actions: [
        PopupMenuButton(
          onSelected: (value) {
            print('value: $value');
            _navigateToAddressFormPage(context);
          },
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(15.0),
            ),
          ),
          itemBuilder: (_) {
            return const [
              PopupMenuItem(
                child: Text('Add new address'),
                value: 'add_new_address',
              ),
              // PopupMenuItem(
              //   child: Text("Flutter"),
              // ),
              // PopupMenuItem(
              //   child: Text("Google.com"),
              // ),
              // PopupMenuItem(
              //   child: Text("https://blogdeveloperspot.blogspot.com"),
              // ),
            ];
          },
        ),
      ],
      title: const Text(
        'Addresses',
      ),
    );
  }

  Widget _buildBody() {
    return Container(
      padding: EdgeInsets.all(10),
      child: Column(
        children: [
          _buildSearchField(),
          SizedBox(height: 10),
          Expanded(
            child: ListView.separated(
                keyboardDismissBehavior:
                    ScrollViewKeyboardDismissBehavior.onDrag,
                separatorBuilder: (_, index) {
                  return const SizedBox(height: 10);
                },
                physics: BouncingScrollPhysics(),
                shrinkWrap: true,
                itemCount: _addresses.length,
                itemBuilder: (_, index) {
                  return AddressCard(
                    address: _addresses[index],
                    isFocused: _selectedIndex == index,
                    onTap: () {
                      onFocusAddress(index);
                    },
                  );
                }),
          ),
        ],
      ),
    );
  }

  Widget _buildSearchField() {
    return TextField(
      controller: _searchController,
      decoration: InputDecoration(
        hintText: 'Search',
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.amber),
          borderRadius: BorderRadius.circular(10),
        ),
        suffixIcon: Icon(Icons.search),
      ),
      onChanged: (query) {
        print('query: $query');
      },
    );
  }
}
