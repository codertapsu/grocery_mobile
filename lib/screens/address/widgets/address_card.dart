import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:grocery_mobile/models/address_model.dart';

class AddressCard extends StatefulWidget {
  final Address address;
  final bool isFocused;
  final VoidCallback? onTap;

  AddressCard({
    Key? key,
    required this.address,
    this.isFocused = false,
    this.onTap,
  }) : super(key: key);

  @override
  State<AddressCard> createState() => _AddressCardState();
}

class _AddressCardState extends State<AddressCard> {
  final neutralColor = Colors.green;
  final focusedColor = const Color.fromARGB(255, 128, 51, 215);

  late bool _isFocused;
  late VoidCallback? _onTap;

  @override
  void didUpdateWidget(oldWidget) {
    super.didUpdateWidget(oldWidget);
    setState(() {
      _isFocused = widget.isFocused;
    });
  }

  @override
  void initState() {
    super.initState();
    _isFocused = widget.isFocused;
    _onTap = widget.onTap;
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 4.0,
      shape: RoundedRectangleBorder(
        side: BorderSide(
          color: _isFocused ? focusedColor : Colors.white,
          width: _isFocused ? 2.0 : 0,
        ),
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: InkWell(
        borderRadius: BorderRadius.circular(20.0),
        splashColor:
            (_isFocused ? focusedColor : neutralColor).withOpacity(0.2),
        onTap: _onTap,
        onLongPress: () {},
        child: Stack(
          children: [
            _buildAddressCard(),
            if (widget.address.isDefault) _buildRotatedCornerDecoration(),
          ],
        ),
      ),
    );
  }

  Widget _buildRotatedCornerDecoration() {
    var height = 30.0;
    var width = 300.0;
    return Positioned(
      right: (-width / 2) + height,
      top: height / 2,
      child: Transform.rotate(
        angle: math.pi / 4,
        child: Container(
          width: width,
          height: height,
          decoration: BoxDecoration(
            color: _isFocused ? focusedColor : neutralColor,
          ),
          child: Center(
            child: Text(
              'Default',
              style: TextStyle(
                color: Colors.white,
                fontSize: 15,
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildAddressCard() {
    return Container(
      padding: const EdgeInsets.all(20),
      decoration: const BoxDecoration(
        color: Colors.transparent,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(8),
          bottomRight: Radius.circular(8),
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              _buildCircleIcon(),
              const SizedBox(width: 10),
              Text(
                'Office',
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 20,
                ),
              )
            ],
          ),
          const SizedBox(height: 15),
          _buildAddress(),
          const SizedBox(height: 15),
          _buildPhone(),
        ],
      ),
    );
  }

  Widget _buildCircleIcon() {
    return Stack(
      children: [
        Container(
          decoration: BoxDecoration(
            border: Border.all(
              color: _isFocused ? focusedColor : neutralColor,
              width: 2.0,
            ),
            shape: BoxShape.circle,
          ),
          child: ClipOval(
            child: SizedBox.fromSize(
              size: const Size.fromRadius(25),
              child: Icon(
                Icons.home,
                color: _isFocused ? focusedColor : neutralColor,
              ),
            ),
          ),
        ),
        Positioned(
          right: 0,
          child: Container(
            decoration: const BoxDecoration(
              color: Colors.white,
              shape: BoxShape.circle,
            ),
            child: Icon(
              Icons.check_circle,
              size: 20,
              color: _isFocused ? focusedColor : neutralColor,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildAddress() {
    return Text(
      'The US address provided on this page was randomly generated.'
      'All these generated addresses covering all cities and states in the United States.',
      overflow: TextOverflow.ellipsis,
      maxLines: 10,
    );
  }

  Widget _buildPhone() {
    return Text('+1 (555) 555-5555');
  }
}

class TriangleClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path();
    path.moveTo(size.width / 2, 0.0);
    path.lineTo(size.width, size.height);
    path.lineTo(0.0, size.height);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(TriangleClipper oldClipper) => false;
}
