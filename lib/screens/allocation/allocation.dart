import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:grocery_mobile/controllers/layout_controller.dart';
import 'package:grocery_mobile/screens/dashboard/dashboard_screen.dart';
import 'package:grocery_mobile/widgets/custom_home_appBar.dart';
// import 'package:grocery_mobile/widgets/home_animal_card.dart';
// import 'package:grocery_mobile/widgets/search_field.dart';

class AllocationPage extends StatefulWidget {
  final VoidCallback toggleMenu;

  const AllocationPage({
    Key? key,
    required this.toggleMenu,
  }) : super(key: key);

  @override
  State<AllocationPage> createState() => _AllocationPageState();
}

class _AllocationPageState extends State<AllocationPage> {
  final _layoutController = Get.find<LayoutController>();
  late Widget _child;

  List<Widget> navigatorItems = List.unmodifiable([
    DashBoardScreen(),
    // ProfileScreen(),
    // HomeScreen(),
    // ProfileScreen(),
  ]);

  void _handleNavigationChange(int index) {
    setState(() {
      _child = AnimatedSwitcher(
        switchInCurve: Curves.easeOut,
        switchOutCurve: Curves.easeIn,
        duration: const Duration(milliseconds: 500),
        child: navigatorItems[index],
      );
    });
  }

  @override
  void initState() {
    _child = DashBoardScreen();
    super.initState();
    _layoutController.currentMenuIndex.listen((index) {
      // _handleNavigationChange(index);
    });
  }

  @override
  void dispose() {
    // _menuController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var screenWidth = MediaQuery.of(context).size.width;
    var screenHeight = MediaQuery.of(context).size.height;
    return Column(
      children: <Widget>[
        CustomAppBar(
          safeHeight: screenHeight,
        ),
        Expanded(child: _child)
      ],
    );
  }
}
