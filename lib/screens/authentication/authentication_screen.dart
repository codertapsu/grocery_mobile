import 'dart:math';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:grocery_mobile/controllers/controllers.dart';
import 'package:grocery_mobile/helpers/color_helper.dart';
import 'package:grocery_mobile/helpers/matrix_helper.dart';
import 'package:grocery_mobile/helpers/validators.dart';
import 'package:grocery_mobile/models/login_user_type.dart';
import 'package:grocery_mobile/models/user_form_field.dart';
import 'package:grocery_mobile/screens/authentication/widgets/confirm_recover_card.dart';
import 'package:grocery_mobile/styles/sign_in_theme.dart';

import 'widgets/additional_sign_up_card.dart';
import 'widgets/animated_card_builder.dart';
import 'widgets/confirm_sign_up_card.dart';
import 'widgets/recover_card.dart';
import 'widgets/sign_in_card.dart';

class AuthenticationScreen extends StatefulWidget {
  AuthenticationScreen({Key? key}) : super(key: key);

  @override
  State<AuthenticationScreen> createState() => _AuthenticationScreenState();
}

class _AuthenticationScreenState extends State<AuthenticationScreen>
    with TickerProviderStateMixin {
  static const int _signInPageIndex = 0;
  static const int _recoveryIndex = 1;
  static const int _additionalSignUpIndex = 2;
  static const int _confirmSignUpIndex = 3;
  static const int _confirmRecoverIndex = 4;

  final _loadingDuration = const Duration(milliseconds: 400);
  final _loginUserType = LoginUserType.phone;

  late AnimationController _loadingController;
  late Animation<double> _flipAnimation;

  int _pageIndex = _signInPageIndex;

  ThemeData _mergeTheme(
      {required ThemeData theme, required SignInTheme signInTheme}) {
    final blackOrWhite =
        theme.brightness == Brightness.light ? Colors.black54 : Colors.white;
    final primaryOrWhite = theme.brightness == Brightness.light
        ? theme.primaryColor
        : Colors.white;
    final originalPrimaryColor = signInTheme.primaryColor ?? theme.primaryColor;
    final primaryDarkShades = getDarkShades(originalPrimaryColor);
    final primaryColor = primaryDarkShades.length == 1
        ? lighten(primaryDarkShades.first!)
        : primaryDarkShades.first;
    final primaryColorDark = primaryDarkShades.length >= 3
        ? primaryDarkShades[2]
        : primaryDarkShades.last;
    final accentColor = signInTheme.accentColor ?? theme.colorScheme.secondary;
    final errorColor = signInTheme.errorColor ?? theme.errorColor;
    // the background is a dark gradient, force to use white text if detect default black text color
    final isDefaultBlackText = theme.textTheme.headline3!.color ==
        Typography.blackMountainView.headline3!.color;
    final titleStyle = theme.textTheme.headline3!
        .copyWith(
          color: signInTheme.accentColor ??
              (isDefaultBlackText
                  ? Colors.white
                  : theme.textTheme.headline3!.color),
          fontSize: signInTheme.beforeHeroFontSize,
          fontWeight: FontWeight.w300,
        )
        .merge(signInTheme.titleStyle);
    final footerStyle = theme.textTheme.bodyText1!
        .copyWith(
          color: signInTheme.accentColor ??
              (isDefaultBlackText
                  ? Colors.white
                  : theme.textTheme.headline3!.color),
        )
        .merge(signInTheme.footerTextStyle);
    final textStyle = theme.textTheme.bodyText2!
        .copyWith(color: blackOrWhite)
        .merge(signInTheme.bodyStyle);
    final textFieldStyle = theme.textTheme.subtitle1!
        .copyWith(color: blackOrWhite, fontSize: 14)
        .merge(signInTheme.textFieldStyle);
    final buttonStyle = theme.textTheme.button!
        .copyWith(color: Colors.white)
        .merge(signInTheme.buttonStyle);
    final cardTheme = signInTheme.cardTheme;
    final inputTheme = signInTheme.inputTheme;
    final buttonTheme = signInTheme.buttonTheme;
    final roundBorderRadius = BorderRadius.circular(100);

    // signInThemeHelper.loginTextStyle = titleStyle;

    TextStyle labelStyle;

    if (signInTheme.primaryColorAsInputLabel) {
      labelStyle = TextStyle(color: primaryColor);
    } else {
      labelStyle = TextStyle(color: blackOrWhite);
    }

    return theme.copyWith(
      primaryColor: primaryColor,
      primaryColorDark: primaryColorDark,
      errorColor: errorColor,
      cardTheme: theme.cardTheme.copyWith(
        clipBehavior: cardTheme.clipBehavior,
        color: cardTheme.color ?? theme.cardColor,
        elevation: cardTheme.elevation ?? 12.0,
        margin: cardTheme.margin ?? const EdgeInsets.all(4.0),
        shape: cardTheme.shape ??
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      ),
      inputDecorationTheme: theme.inputDecorationTheme.copyWith(
        filled: inputTheme.filled,
        fillColor: inputTheme.fillColor ??
            Color.alphaBlend(
              primaryOrWhite.withOpacity(.07),
              Colors.grey.withOpacity(.04),
            ),
        contentPadding: inputTheme.contentPadding ??
            const EdgeInsets.symmetric(vertical: 4.0),
        errorStyle: inputTheme.errorStyle ?? TextStyle(color: errorColor),
        labelStyle: inputTheme.labelStyle ?? labelStyle,
        enabledBorder: inputTheme.enabledBorder ??
            inputTheme.border ??
            OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.transparent),
              borderRadius: roundBorderRadius,
            ),
        focusedBorder: inputTheme.focusedBorder ??
            inputTheme.border ??
            OutlineInputBorder(
              borderSide: BorderSide(color: primaryColor!, width: 1.5),
              borderRadius: roundBorderRadius,
            ),
        errorBorder: inputTheme.errorBorder ??
            inputTheme.border ??
            OutlineInputBorder(
              borderSide: BorderSide(color: errorColor),
              borderRadius: roundBorderRadius,
            ),
        focusedErrorBorder: inputTheme.focusedErrorBorder ??
            inputTheme.border ??
            OutlineInputBorder(
              borderSide: BorderSide(color: errorColor, width: 1.5),
              borderRadius: roundBorderRadius,
            ),
        disabledBorder: inputTheme.disabledBorder ??
            inputTheme.border ??
            OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.transparent),
              borderRadius: roundBorderRadius,
            ),
      ),
      floatingActionButtonTheme: theme.floatingActionButtonTheme.copyWith(
        backgroundColor: buttonTheme.backgroundColor ?? primaryColor,
        splashColor: buttonTheme.splashColor ?? theme.colorScheme.secondary,
        elevation: buttonTheme.elevation ?? 4.0,
        highlightElevation: buttonTheme.highlightElevation ?? 2.0,
        shape: buttonTheme.shape ?? const StadiumBorder(),
      ),
      // put it here because floatingActionButtonTheme doesnt have highlightColor property
      highlightColor:
          signInTheme.buttonTheme.highlightColor ?? theme.highlightColor,
      textTheme: theme.textTheme.copyWith(
        headline3: titleStyle,
        bodyText2: textStyle,
        subtitle1: textFieldStyle,
        subtitle2: footerStyle,
        button: buttonStyle,
      ),
      colorScheme:
          Theme.of(context).colorScheme.copyWith(secondary: accentColor),
    );
  }

  void _changeCard(int newCardIndex) {
    setState(() {
      _pageIndex = newCardIndex;
    });
  }

  @override
  void initState() {
    _loadingController = AnimationController(
      vsync: this,
      duration: _loadingDuration,
    );

    _flipAnimation = Tween<double>(begin: pi / 2, end: 0).animate(
      CurvedAnimation(
        parent: _loadingController,
        curve: Curves.easeOutBack,
        reverseCurve: Curves.easeIn,
      ),
    );

    super.initState();

    WidgetsBinding.instance!.addPostFrameCallback((_) {
      _loadingController.forward();
    });
  }

  @override
  void dispose() {
    _loadingController.dispose();
    Get.find<AuthController>().closeAuthForm();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;
    var screenWidth = MediaQuery.of(context).size.width;
    final signInTheme = SignInTheme();
    final theme = _mergeTheme(
      theme: Theme.of(context),
      signInTheme: signInTheme,
    );
    return Theme(
      data: theme,
      child: GestureDetector(
        onTap: () {
          FocusManager.instance.primaryFocus!.unfocus();
        },
        child: Container(
          color: Colors.transparent,
          width: screenWidth,
          height: screenHeight,
          child: Align(
            alignment: Alignment.center,
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  AnimatedBuilder(
                    animation: _flipAnimation,
                    builder: (_, child) {
                      return Transform(
                        transform: MatrixHelper.perspective()
                          ..rotateX(_flipAnimation.value),
                        alignment: Alignment.center,
                        child: child,
                      );
                    },
                    child: Container(
                      height: min(screenHeight * 0.7, 500),
                      child: AnimatedCardBuilder(
                        pageIndex: _pageIndex,
                        children: _buildAuthCards(),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  List<Widget> _buildAuthCards() {
    return [
      SignInCard(
        userType: _loginUserType,
        onSwitchRecoveryPassword: () => _changeCard(_recoveryIndex),
      ),
      RecoverCard(
        userType: _loginUserType,
        onBack: () => _changeCard(_signInPageIndex),
      ),
      ConfirmRecoverCard(
        passwordValidator: Validators.defaultPasswordValidator,
        onBack: () => _changeCard(_signInPageIndex),
        onSubmitCompleted: () => _changeCard(_signInPageIndex),
      ),
      AdditionalSignUpCard(
        formFields: [
          UserFormField(
              keyName: 'Username', icon: Icon(FontAwesomeIcons.userAlt)),
          UserFormField(keyName: 'Name'),
          UserFormField(keyName: 'Surname'),
          UserFormField(
            keyName: 'phone_number',
            displayName: 'Phone Number',
            userType: LoginUserType.phone,
            fieldValidator: (value) {
              var phoneRegExp = RegExp(
                  '^(\\+\\d{1,2}\\s)?\\(?\\d{3}\\)?[\\s.-]?\\d{3}[\\s.-]?\\d{4}\$');
              if (value != null &&
                  value.length < 7 &&
                  !phoneRegExp.hasMatch(value)) {
                return "This isn't a valid phone number";
              }
              return null;
            },
          ),
        ],
        onBack: () => _changeCard(_signInPageIndex),
        onSubmitCompleted: () {
          // if (auth.onConfirmSignup != null) {
          //   _changeCard(_confirmSignup);
          // } else if (widget.loginAfterSignUp) {
          //   _forwardChangeRouteAnimation(_additionalSignUpCardKey)
          //       .then((_) {
          //     widget.onSubmitCompleted!();
          //   });
          // } else {
          //   _changeCard(_loginPageIndex);
          // }
        },
      ),
      ConfirmSignUpCard(
        onBack: () {
          _changeCard(_additionalSignUpIndex);
        },
        onSubmitCompleted: () {
          // if (widget.loginAfterSignUp) {
          //   _forwardChangeRouteAnimation(_confirmSignUpCardKey).then((_) {
          //     widget.onSubmitCompleted!();
          //   });
          // } else {
          //   _changeCard(_loginPageIndex);
          // }
        },
      ),
    ];
  }
}
