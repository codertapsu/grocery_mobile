import 'dart:collection';
import 'dart:math';

import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:grocery_mobile/helpers/text_field_helper.dart';
import 'package:grocery_mobile/models/user_form_field.dart';
import 'package:grocery_mobile/styles/app_theme.dart';
import 'package:grocery_mobile/widgets/animated_button.dart';
import 'package:grocery_mobile/widgets/animated_text_form_field.dart';
import 'package:grocery_mobile/widgets/fade_in.dart';

class AdditionalSignUpCard extends StatefulWidget {
  final List<UserFormField> formFields;
  final VoidCallback onBack;
  final VoidCallback onSubmitCompleted;

  AdditionalSignUpCard({
    Key? key,
    required this.formFields,
    required this.onBack,
    required this.onSubmitCompleted,
  }) : super(key: key) {
    if (formFields.isEmpty) {
      throw RangeError('The formFields array must not be empty');
    } else if (formFields.length > 6) {
      throw RangeError(
          'More than 6 formFields are not displayable, you provided ${formFields.length}');
    }
  }

  @override
  AdditionalSignUpCardState createState() => AdditionalSignUpCardState();
}

class AdditionalSignUpCardState extends State<AdditionalSignUpCard>
    with TickerProviderStateMixin {
  final _formKey = GlobalKey<FormState>();

  late AnimationController _loadingController;
  late AnimationController _submitController;
  late Interval _textButtonLoadingAnimationInterval;
  late Animation<double> _buttonScaleAnimation;
  late List<AnimationController> _fieldAnimationControllers = [];
  late List<Interval> animatedTextFormFieldIntervals;
  late HashMap<String, TextEditingController> _nameControllers;

  bool _isSubmitting = false;

  void _initAnimatedTextFormFieldIntervals() {
    var delta = 0.85;
    animatedTextFormFieldIntervals = [Interval(0, delta)];
    for (int index = 1; index < 10; index++) {
      var startAt = animatedTextFormFieldIntervals[index - 1].begin + 0.15;
      animatedTextFormFieldIntervals.add(Interval(startAt, startAt + delta));
    }
  }

  @override
  void initState() {
    _loadingController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 1150),
      reverseDuration: const Duration(milliseconds: 300),
    );
    _nameControllers =
        HashMap<String, TextEditingController>.fromIterable(widget.formFields,
            key: (formFields) => formFields.keyName,
            value: (formFields) => TextEditingController(
                  text: formFields.defaultValue,
                ));

    if (_nameControllers.length != widget.formFields.length) {
      throw ArgumentError(
          'Some of the formFields have duplicated names, and this is not allowed.');
    }
    _fieldAnimationControllers = widget.formFields
        .map((e) => AnimationController(
            vsync: this, duration: const Duration(milliseconds: 1000)))
        .toList();
    // List<double> intervalBegin = List<double>.generate(widget.formFields.length, (i) => 0.15 / i);
    //
    // for (int i = 0; i < widget.formFields.length; i++) {
    //   _fieldAnimationIntervals.add(Interval(intervalBegin[i], (0.85 + intervalBegin[i])));
    // }
    _submitController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 1000),
    );
    _textButtonLoadingAnimationInterval = const Interval(
      .6,
      1.0,
      curve: Curves.easeOut,
    );
    _buttonScaleAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(
        parent: _loadingController,
        curve: const Interval(.4, 1.0, curve: Curves.easeOutBack),
      ),
    );

    _initAnimatedTextFormFieldIntervals();

    super.initState();

    Future.delayed(const Duration(seconds: 1), () {
      if (mounted) {
        _loadingController.forward();
      }
    });
  }

  @override
  void dispose() {
    for (var element in _fieldAnimationControllers) {
      element.dispose();
    }
    _loadingController.dispose();
    _submitController.dispose();
    super.dispose();
  }

  Future<bool> _submit() async {
    FocusScope.of(context).requestFocus(FocusNode());

    return true;
    // final messages = Provider.of<LoginMessages>(context, listen: false);

    // if (!_formCompleteSignUpKey.currentState!.validate()) {
    //   return false;
    // }

    // _formCompleteSignUpKey.currentState!.save();
    // await _submitController.forward();
    // setState(() => _isSubmitting = true);
    // final auth = Provider.of<Auth>(context, listen: false);
    // String? error;

    // We have to convert the Map<String, TextEditingController> to a Map<String, String>
    // and pass it to the function given by the user
    // auth.additionalSignupData =
    //     _nameControllers.map((key, value) => MapEntry(key, value.text));

    // switch (auth.authType) {
    //   case AuthType.provider:
    //     error = await auth.onSignup!(SignupData.fromProvider(
    //       additionalSignupData: auth.additionalSignupData,
    //     ));
    //     break;
    //   case AuthType.userPassword:
    //     error = await auth.onSignup!(SignupData.fromSignupForm(
    //         name: auth.email,
    //         password: auth.password,
    //         additionalSignupData: auth.additionalSignupData,
    //         termsOfService: auth.getTermsOfServiceResults()));
    //     break;
    // }

    // await _submitController.reverse();
    // if (!DartHelper.isNullOrEmpty(error)) {
    //   showErrorToast(context, messages.flushbarTitleError, error!);
    //   setState(() => _isSubmitting = false);
    //   return false;
    // } else {
    //   showSuccessToast(context, messages.flushbarTitleSuccess,
    //       messages.signUpSuccess, const Duration(seconds: 4));
    //   setState(() => _isSubmitting = false);
    //   // await _loadingController.reverse();
    //   widget.onSubmitCompleted.call();
    //   return true;
    // }
  }

  Widget _buildFields(double width) {
    return Column(
      children: widget.formFields.mapIndexed((index, formField) {
        return Column(
          children: [
            const SizedBox(
              height: 10,
            ),
            AnimatedTextFormField(
              controller: _nameControllers[formField.keyName],
              interval: animatedTextFormFieldIntervals[index],
              loadingController: _loadingController,
              width: width,
              labelText: formField.displayName,
              prefixIcon: formField.icon ??
                  const Icon(FontAwesomeIcons.solidUserCircle),
              keyboardType: TextFieldUtils.getKeyboardType(formField.userType),
              autofillHints: [
                TextFieldUtils.getAutofillHints(formField.userType)
              ],
              textInputAction:
                  formField.keyName == widget.formFields.last.keyName
                      ? TextInputAction.done
                      : TextInputAction.next,
              validator: formField.fieldValidator,
            ),
            const SizedBox(
              height: 5,
            )
          ],
        );
      }).toList(),
    );
  }

  Widget _buildSubmitButton() {
    return ScaleTransition(
      scale: _buttonScaleAnimation,
      child: AnimatedButton(
        controller: _submitController,
        text: 'SUBMIT',
        onPressed: !_isSubmitting ? _submit : null,
      ),
    );
  }

  Widget _buildBackButton() {
    return FadeIn(
      controller: _loadingController,
      offset: .5,
      curve: _textButtonLoadingAnimationInterval,
      fadeDirection: FadeDirection.topToBottom,
      child: TextButton(
        onPressed: () {
          if (!_isSubmitting) {
            _formKey.currentState!.save();
            widget.onBack();
          }
        },
        style: AppTheme.textButtonDefaultStyle,
        child: Text('BACK'),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    final cardWidth = min(deviceSize.width * 0.75, 360.0);
    const cardPadding = 16.0;
    final textFieldWidth = cardWidth - cardPadding * 2;

    return FittedBox(
      child: Card(
        child: Container(
          padding: const EdgeInsets.only(
            left: cardPadding,
            top: cardPadding + 10.0,
            right: cardPadding,
            bottom: cardPadding,
          ),
          width: cardWidth,
          alignment: Alignment.center,
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                ScaleTransition(
                  scale: _buttonScaleAnimation,
                  child: Text(
                    'Please fill in this form to complete the sign up',
                    key: Key('RECOVER_PASSWORD_INTRO'),
                    textAlign: TextAlign.center,
                  ),
                ),
                _buildFields(textFieldWidth),
                const SizedBox(height: 5),
                _buildSubmitButton(),
                _buildBackButton(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
