import 'dart:collection';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:grocery_mobile/helpers/matrix_helper.dart';

class AnimatedCardBuilder extends StatefulWidget {
  final List<Widget> children;
  final int pageIndex;

  AnimatedCardBuilder({
    Key? key,
    required this.children,
    this.pageIndex = 0,
  }) : super(key: key);

  @override
  State<AnimatedCardBuilder> createState() => _AnimatedCardBuilderState();
}

class _AnimatedCardBuilderState extends State<AnimatedCardBuilder>
    with SingleTickerProviderStateMixin {
  final _loadingDuration = const Duration(milliseconds: 4000);
  final _animationDuration = const Duration(milliseconds: 350);
  final _animationCurve = Curves.ease;

  late final PageController _pageController;

  int _currentPageIndex = 0;

  void _changePage(int pageIndex) {
    setState(() {
      _currentPageIndex = pageIndex;
    });
  }

  int _determinePageIndex(int pageIndex) {
    return pageIndex % widget.children.length;
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    print('didChangeDependencies');
  }

  @override
  void didUpdateWidget(AnimatedCardBuilder oldWidget) {
    super.didUpdateWidget(oldWidget);
    print('didUpdateWidget');
    _pageController.animateToPage(
      widget.pageIndex,
      duration: _animationDuration,
      curve: _animationCurve,
    );
  }

  @override
  void initState() {
    _pageController = PageController(
      viewportFraction: 1.0,
      initialPage: widget.pageIndex,
    );
    super.initState();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return PageView.builder(
      // physics: const NeverScrollableScrollPhysics(),
      controller: _pageController,
      onPageChanged: _changePage,
      itemBuilder: (_, index) {
        var scale = index == _currentPageIndex ? 1.0 : 0.8;
        var realPageIndex = _determinePageIndex(index);
        var isContainedChild =
            widget.children.asMap().containsKey(realPageIndex);
        var child = isContainedChild
            ? widget.children[_determinePageIndex(index)]
            : widget.children[0];
        return TweenAnimationBuilder(
          duration: _animationDuration,
          tween: Tween(begin: scale, end: scale),
          curve: _animationCurve,
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 25),
            child: child,
          ),
          builder: (_, value, child) {
            return Transform.scale(
              scale: value as double,
              child: child,
            );
          },
        );
      },
    );
  }
}
