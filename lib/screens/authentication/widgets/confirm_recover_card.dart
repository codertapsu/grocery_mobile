import 'dart:math';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:grocery_mobile/styles/app_theme.dart';
import 'package:grocery_mobile/widgets/animated_button.dart';
import 'package:grocery_mobile/widgets/animated_text_form_field.dart';
import 'package:grocery_mobile/widgets/fade_in.dart';

class ConfirmRecoverCard extends StatefulWidget {
  const ConfirmRecoverCard({
    Key? key,
    required this.passwordValidator,
    required this.onBack,
    required this.onSubmitCompleted,
  }) : super(key: key);

  final FormFieldValidator<String> passwordValidator;
  final VoidCallback onBack;
  final VoidCallback onSubmitCompleted;

  @override
  ConfirmRecoverCardState createState() => ConfirmRecoverCardState();
}

class ConfirmRecoverCardState extends State<ConfirmRecoverCard>
    with TickerProviderStateMixin {
  final _formKey = GlobalKey<FormState>();
  final _passwordFocusNode = FocusNode();
  final _confirmPasswordFocusNode = FocusNode();
  final _passwordController = TextEditingController();

  late AnimationController _loadingController;
  late AnimationController _submitController;
  late Interval _textButtonLoadingAnimationInterval;
  late Animation<double> _buttonScaleAnimation;
  late List<Interval> animatedTextFormFieldIntervals;

  bool _isSubmitting = false;
  String _code = '';

  void _initAnimatedTextFormFieldIntervals() {
    animatedTextFormFieldIntervals = [];
    List<double> intervalBegin = List<double>.generate(10, (i) => 0.15 / i);

    for (int i = 0; i < intervalBegin.length; i++) {
      animatedTextFormFieldIntervals.add(
        Interval(
          min(0.85, max(0.0, intervalBegin[i])),
          min((0.85 + intervalBegin[i]), 1.0),
        ),
      );
    }
  }

  @override
  void initState() {
    _loadingController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 1150),
      reverseDuration: const Duration(milliseconds: 300),
    );

    _submitController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 1000),
    );
    _textButtonLoadingAnimationInterval = const Interval(
      .6,
      1.0,
      curve: Curves.easeOut,
    );
    _buttonScaleAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(
        parent: _loadingController,
        curve: const Interval(.4, 1.0, curve: Curves.easeOutBack),
      ),
    );
    _initAnimatedTextFormFieldIntervals();

    super.initState();

    Future.delayed(const Duration(seconds: 1), () {
      if (mounted) {
        _loadingController.forward();
      }
    });
  }

  @override
  void dispose() {
    _loadingController.dispose();
    _submitController.dispose();
    super.dispose();
  }

  Future<bool> _submit() async {
    FocusScope.of(context).requestFocus(FocusNode()); // close keyboard

    if (!_formKey.currentState!.validate()) {
      return false;
    }
    // final auth = Provider.of<Auth>(context, listen: false);
    // final messages = Provider.of<LoginMessages>(context, listen: false);

    _formKey.currentState!.save();
    await _submitController.forward();
    setState(() => _isSubmitting = true);
    // final error = await auth.onConfirmRecover!(
    //   _code,
    //   LoginData(
    //     name: auth.email,
    //     password: auth.password,
    //   ),
    // );

    // if (error != null) {
    //   showErrorToast(context, messages.flushbarTitleError, error);
    //   setState(() => _isSubmitting = false);
    //   await _submitController.reverse();
    //   return false;
    // } else {
    //   showSuccessToast(context, messages.flushbarTitleSuccess,
    //       messages.confirmRecoverSuccess);
    //   setState(() => _isSubmitting = false);
    //   widget.onSubmitCompleted();
    //   return true;
    // }
    return true;
  }

  Widget _buildVerificationCodeField(double width) {
    return AnimatedTextFormField(
      loadingController: _loadingController,
      interval: animatedTextFormFieldIntervals[0],
      width: width,
      labelText: 'Recovery Code',
      prefixIcon: const Icon(FontAwesomeIcons.solidCheckCircle),
      textInputAction: TextInputAction.next,
      onFieldSubmitted: (value) {
        FocusScope.of(context).requestFocus(_passwordFocusNode);
      },
      validator: (value) {
        if (value!.isEmpty) {
          return 'Recovery code is empty';
        }
        return null;
      },
      onSaved: (value) => _code = value!,
    );
  }

  Widget _buildPasswordField(double width) {
    return AnimatedPasswordTextFormField(
      loadingController: _loadingController,
      interval: animatedTextFormFieldIntervals[1],
      animatedWidth: width,
      labelText: 'Password',
      controller: _passwordController,
      textInputAction: TextInputAction.next,
      focusNode: _passwordFocusNode,
      onFieldSubmitted: (value) {
        FocusScope.of(context).requestFocus(_confirmPasswordFocusNode);
      },
      validator: widget.passwordValidator,
      onSaved: (value) {
        // final auth = Provider.of<Auth>(context, listen: false);
        // auth.password = value!;
      },
    );
  }

  Widget _buildConfirmPasswordField(double width) {
    return AnimatedPasswordTextFormField(
      loadingController: _loadingController,
      interval: animatedTextFormFieldIntervals[2],
      animatedWidth: width,
      labelText: 'Confirm Password',
      textInputAction: TextInputAction.done,
      focusNode: _confirmPasswordFocusNode,
      onFieldSubmitted: (value) => _submit(),
      validator: (value) {
        if (value != _passwordController.text) {
          return 'Password do not match!';
        }
        return null;
      },
    );
  }

  Widget _buildSubmitButton() {
    return ScaleTransition(
      scale: _buttonScaleAnimation,
      child: AnimatedButton(
        controller: _submitController,
        text: 'SET PASSWORD',
        onPressed: !_isSubmitting ? _submit : null,
      ),
    );
  }

  Widget _buildBackButton() {
    return FadeIn(
      controller: _loadingController,
      offset: .5,
      curve: _textButtonLoadingAnimationInterval,
      fadeDirection: FadeDirection.topToBottom,
      child: TextButton(
        onPressed: () {
          if (!_isSubmitting) {
            _formKey.currentState!.save();
            widget.onBack();
          }
        },
        style: AppTheme.textButtonDefaultStyle,
        child: Text('BACK'),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    final cardWidth = min(deviceSize.width * 0.75, 360.0);
    const cardPadding = 16.0;
    final textFieldWidth = cardWidth - cardPadding * 2;

    return FittedBox(
      child: Card(
        child: Container(
          padding: const EdgeInsets.only(
            left: cardPadding,
            top: cardPadding + 10.0,
            right: cardPadding,
            bottom: cardPadding,
          ),
          width: cardWidth,
          alignment: Alignment.center,
          child: Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                ScaleTransition(
                  scale: _buttonScaleAnimation,
                  child: Text(
                    'The recovery code to set a new password was sent to your email.',
                    textAlign: TextAlign.center,
                  ),
                ),
                const SizedBox(height: 20),
                _buildVerificationCodeField(textFieldWidth),
                const SizedBox(height: 15),
                _buildPasswordField(textFieldWidth),
                const SizedBox(height: 15),
                _buildConfirmPasswordField(textFieldWidth),
                const SizedBox(height: 20),
                _buildSubmitButton(),
                _buildBackButton(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
