import 'dart:math';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:grocery_mobile/styles/app_theme.dart';
import 'package:grocery_mobile/widgets/animated_button.dart';
import 'package:grocery_mobile/widgets/animated_text_form_field.dart';
import 'package:grocery_mobile/widgets/fade_in.dart';

class ConfirmSignUpCard extends StatefulWidget {
  final VoidCallback onBack;
  final VoidCallback onSubmitCompleted;

  const ConfirmSignUpCard({
    Key? key,
    required this.onBack,
    required this.onSubmitCompleted,
  }) : super(key: key);

  @override
  ConfirmSignUpCardState createState() => ConfirmSignUpCardState();
}

class ConfirmSignUpCardState extends State<ConfirmSignUpCard>
    with TickerProviderStateMixin {
  final _formKey = GlobalKey<FormState>();

  late AnimationController _loadingController;
  late AnimationController _submitController;
  late Interval _textButtonLoadingAnimationInterval;
  late Animation<double> _buttonScaleAnimation;

  bool _isSubmitting = false;
  String _code = '';

  @override
  void initState() {
    _loadingController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 1150),
      reverseDuration: const Duration(milliseconds: 300),
    );
    _submitController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 1000),
    );
    _textButtonLoadingAnimationInterval =
        const Interval(.6, 1.0, curve: Curves.easeOut);
    _buttonScaleAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(
        parent: _loadingController,
        curve: const Interval(.4, 1.0, curve: Curves.easeOutBack),
      ),
    );

    super.initState();

    Future.delayed(const Duration(seconds: 1), () {
      if (mounted) {
        _loadingController.forward();
      }
    });
  }

  @override
  void dispose() {
    _submitController.dispose();
    _loadingController.dispose();
    super.dispose();
  }

  Future<bool> _submit() async {
    FocusScope.of(context).requestFocus(FocusNode());

    // if (!_formRecoverKey.currentState!.validate()) {
    //   return false;
    // }
    // final auth = Provider.of<Auth>(context, listen: false);
    // final messages = Provider.of<LoginMessages>(context, listen: false);

    // _formRecoverKey.currentState!.save();
    // await _fieldSubmitController.forward();
    // setState(() => _isSubmitting = true);
    // final error = await auth.onConfirmSignup!(
    //     _code,
    //     LoginData(
    //       name: auth.email,
    //       password: auth.password,
    //     ));

    // if (error != null) {
    //   showErrorToast(context, messages.flushbarTitleError, error);
    //   setState(() => _isSubmitting = false);
    //   await _fieldSubmitController.reverse();
    //   return false;
    // }

    // showSuccessToast(
    //     context, messages.flushbarTitleSuccess, messages.confirmSignupSuccess);
    // setState(() => _isSubmitting = false);
    // await _fieldSubmitController.reverse();

    // if (!widget.loginAfterSignUp) {
    //   auth.mode = AuthMode.login;
    //   widget.onBack();
    //   return false;
    // }

    // widget.onSubmitCompleted();
    return true;
  }

  Future<bool> _resendCode() async {
    FocusScope.of(context).requestFocus(FocusNode());

    // final auth = Provider.of<Auth>(context, listen: false);
    // final messages = Provider.of<LoginMessages>(context, listen: false);

    // await _fieldSubmitController.forward();
    // setState(() => _isSubmitting = true);
    // final error = await auth.onResendCode!(SignupData.fromSignupForm(
    //     name: auth.email,
    //     password: auth.password,
    //     termsOfService: auth.getTermsOfServiceResults()));

    // if (error != null) {
    //   showErrorToast(context, messages.flushbarTitleError, error);
    //   setState(() => _isSubmitting = false);
    //   await _fieldSubmitController.reverse();
    //   return false;
    // }

    // showSuccessToast(
    //     context, messages.flushbarTitleSuccess, messages.resendCodeSuccess);
    // setState(() => _isSubmitting = false);
    // await _fieldSubmitController.reverse();
    return true;
  }

  Widget _buildConfirmationCodeField(double width) {
    return AnimatedTextFormField(
      loadingController: _loadingController,
      width: width,
      labelText: 'Confirmation Code',
      prefixIcon: const Icon(FontAwesomeIcons.solidCheckCircle),
      textInputAction: TextInputAction.done,
      onFieldSubmitted: (value) => _submit(),
      validator: (value) {
        if (value!.isEmpty) {
          return 'Confirmation code is empty';
        }
        return null;
      },
      onSaved: (value) => _code = value!,
    );
  }

  Widget _buildResendCode() {
    var theme = Theme.of(context);
    return FadeIn(
      controller: _loadingController,
      fadeDirection: FadeDirection.bottomToTop,
      offset: .5,
      curve: _textButtonLoadingAnimationInterval,
      child: TextButton(
        onPressed: !_isSubmitting ? _resendCode : null,
        child: Text(
          'Resend Code',
          style: theme.textTheme.bodyText2,
        ),
      ),
    );
  }

  Widget _buildSubmitButton() {
    return ScaleTransition(
      scale: _buttonScaleAnimation,
      child: AnimatedButton(
        controller: _submitController,
        text: 'CONFIRM',
        onPressed: !_isSubmitting ? _submit : null,
      ),
    );
  }

  Widget _buildBackButton() {
    return FadeIn(
      controller: _loadingController,
      offset: .5,
      curve: _textButtonLoadingAnimationInterval,
      fadeDirection: FadeDirection.topToBottom,
      child: TextButton(
        onPressed: () {
          if (!_isSubmitting) {
            _formKey.currentState!.save();
            widget.onBack();
          }
        },
        style: AppTheme.textButtonDefaultStyle,
        child: Text('BACK'),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    final cardWidth = min(deviceSize.width * 0.75, 360.0);
    const cardPadding = 16.0;
    final textFieldWidth = cardWidth - cardPadding * 2;

    return FittedBox(
      child: Card(
        child: Container(
          padding: const EdgeInsets.only(
            left: cardPadding,
            top: cardPadding + 10.0,
            right: cardPadding,
            bottom: cardPadding,
          ),
          width: cardWidth,
          alignment: Alignment.center,
          child: Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                ScaleTransition(
                  scale: _buttonScaleAnimation,
                  child: Text(
                    'A confirmation code was sent to your email. '
                    'Please enter the code to confirm your account.',
                    textAlign: TextAlign.center,
                  ),
                ),
                const SizedBox(height: 20),
                _buildConfirmationCodeField(textFieldWidth),
                const SizedBox(height: 10),
                _buildResendCode(),
                _buildSubmitButton(),
                _buildBackButton(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
