import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:grocery_mobile/constants/constants.dart';
import 'package:grocery_mobile/helpers/text_field_helper.dart';
import 'package:grocery_mobile/models/login_user_type.dart';
import 'package:grocery_mobile/styles/app_theme.dart';
import 'package:grocery_mobile/widgets/animated_button.dart';
import 'package:grocery_mobile/widgets/animated_text_form_field.dart';
import 'package:grocery_mobile/widgets/fade_in.dart';

class RecoverCard extends StatefulWidget {
  final VoidCallback onBack;
  final LoginUserType userType;

  RecoverCard({
    Key? key,
    required this.onBack,
    required this.userType,
  }) : super(key: key);

  @override
  State<RecoverCard> createState() => _RecoverCardState();
}

class _RecoverCardState extends State<RecoverCard>
    with TickerProviderStateMixin {
  final GlobalKey<FormState> _formKey = GlobalKey();

  late AnimationController _loadingController;
  late AnimationController _submitController;
  late TextEditingController _nameController;
  late Interval _textButtonLoadingAnimationInterval;
  late Animation<double> _buttonScaleAnimation;

  bool _isSubmitting = false;

  void _submit() {
    FocusManager.instance.primaryFocus?.unfocus();
    if (!_formKey.currentState!.validate()) {
      return;
    }

    _formKey.currentState!.save();
    _submitController.forward();
    setState(() => _isSubmitting = true);
    // final error = await auth.onRecoverPassword!(auth.email);

    // if (error != null) {
    //   showErrorToast(context, messages.flushbarTitleError, error);
    //   setState(() => _isSubmitting = false);
    //   await _submitController.reverse();
    //   return false;
    // } else {
    //   showSuccessToast(context, messages.flushbarTitleSuccess,
    //       messages.recoverPasswordSuccess);
    //   setState(() => _isSubmitting = false);
    //   widget.onSubmitCompleted();
    //   return true;
    // }

    Future.delayed(const Duration(seconds: 2), () {
      _submitController.reverse();
      setState(() {
        _isSubmitting = false;
      });
    });
  }

  @override
  void initState() {
    _loadingController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 1150),
      reverseDuration: const Duration(milliseconds: 300),
    );
    _nameController = TextEditingController(text: '');
    _submitController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
    );
    _textButtonLoadingAnimationInterval = const Interval(
      .6,
      1.0,
      curve: Curves.easeOut,
    );
    _buttonScaleAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(
        parent: _loadingController,
        curve: const Interval(.4, 1.0, curve: Curves.easeOutBack),
      ),
    );

    super.initState();

    Future.delayed(const Duration(seconds: 1), () {
      if (mounted) {
        _loadingController.forward();
      }
    });
  }

  @override
  void dispose() {
    _loadingController.dispose();
    _submitController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final cardWidth = math.min(MediaQuery.of(context).size.width * 0.75, 400.0);
    const cardPadding = 16.0;
    final textFieldWidth = cardWidth - cardPadding * 2;
    final recoverForm = Form(
      key: _formKey,
      child: Column(
        children: [
          ScaleTransition(
            scale: _buttonScaleAnimation,
            child: Text(
              'Reset your password here',
              textAlign: TextAlign.center,
            ),
          ),
          const SizedBox(height: 20),
          _buildRecoverNameField(textFieldWidth),
          const SizedBox(height: 20),
          ScaleTransition(
            scale: _buttonScaleAnimation,
            child: Text(
              true
                  ? 'We will send a password recovery code to your email.'
                  : 'We will send your plain-text password to this email account.',
              key: kRecoverPasswordDescriptionKey,
              textAlign: TextAlign.center,
            ),
          ),
          const SizedBox(height: 26),
          _buildSubmitButton(),
          _buildBackButton(),
        ],
      ),
    );
    return FittedBox(
      child: Card(
        elevation: 0,
        child: Container(
          padding: const EdgeInsets.only(
            left: cardPadding,
            right: cardPadding,
            top: cardPadding + 10,
          ),
          width: cardWidth,
          child: recoverForm,
        ),
      ),
    );
  }

  Widget _buildRecoverNameField(double width) {
    return AnimatedTextFormField(
      controller: _nameController,
      loadingController: _loadingController,
      width: width,
      labelText: 'Email',
      prefixIcon: const Icon(FontAwesomeIcons.solidUserCircle),
      keyboardType: TextFieldUtils.getKeyboardType(widget.userType),
      autofillHints: [TextFieldUtils.getAutofillHints(widget.userType)],
      textInputAction: TextInputAction.done,
      onFieldSubmitted: (value) => _submit(),
      onSaved: (value) {},
    );
  }

  Widget _buildSubmitButton() {
    return ScaleTransition(
      scale: _buttonScaleAnimation,
      child: AnimatedButton(
        controller: _submitController,
        text: 'RECOVER',
        onPressed: !_isSubmitting ? _submit : null,
      ),
    );
  }

  Widget _buildBackButton() {
    return FadeIn(
      controller: _loadingController,
      offset: .5,
      curve: _textButtonLoadingAnimationInterval,
      fadeDirection: FadeDirection.topToBottom,
      child: TextButton(
        onPressed: () {
          if (!_isSubmitting) {
            _formKey.currentState!.save();
            widget.onBack();
          }
        },
        style: AppTheme.textButtonDefaultStyle,
        child: Text('BACK'),
      ),
    );
  }
}
