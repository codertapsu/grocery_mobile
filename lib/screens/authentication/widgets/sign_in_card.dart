import 'dart:math';

import 'package:flutter/material.dart';
import 'package:grocery_mobile/helpers/text_field_helper.dart';
import 'package:grocery_mobile/models/login_user_type.dart';
import 'package:grocery_mobile/models/term_of_service.dart';
import 'package:grocery_mobile/styles/app_theme.dart';
import 'package:grocery_mobile/widgets/animated_button.dart';
import 'package:grocery_mobile/widgets/animated_text.dart';
import 'package:grocery_mobile/widgets/animated_text_form_field.dart';
import 'package:grocery_mobile/widgets/fade_in.dart';
import 'package:grocery_mobile/widgets/hidable.dart';
import 'package:grocery_mobile/widgets/term_of_service_checkbox.dart';

var termsOfService = [
  TermOfService(
    id: 'newsletter',
    mandatory: false,
    text: 'Newsletter subscription',
  ),
  TermOfService(
    id: 'general-term',
    mandatory: true,
    text: 'Term of services',
    linkUrl: 'https://github.com/NearHuscarl/flutter_login',
  ),
];

class SignInCard extends StatefulWidget {
  final LoginUserType userType;
  final bool hideForgotPasswordButton;
  final bool hideSignUpButton;
  final VoidCallback onSwitchRecoveryPassword;

  SignInCard({
    Key? key,
    required this.userType,
    required this.onSwitchRecoveryPassword,
    this.hideForgotPasswordButton = false,
    this.hideSignUpButton = false,
  }) : super(key: key);

  @override
  State<SignInCard> createState() => _SignInCardState();
}

class _SignInCardState extends State<SignInCard> with TickerProviderStateMixin {
  final _formKey = GlobalKey<FormState>();
  final _passwordFocusNode = FocusNode();
  final _confirmPasswordFocusNode = FocusNode();

  late AnimationController _loadingController;
  late TextEditingController _nameController;
  late TextEditingController _passController;
  late TextEditingController _confirmPassController;

  var _isLoading = false;
  var _isSubmitting = false;
  var _isLogin = true;

  /// switch between login and signup
  late AnimationController _switchAuthController;
  late AnimationController _postSwitchAuthController;
  late AnimationController _submitController;

  List<AnimationController> _providerControllerList = <AnimationController>[];

  Interval? _nameTextFieldLoadingAnimationInterval;
  Interval? _passTextFieldLoadingAnimationInterval;
  Interval? _textButtonLoadingAnimationInterval;
  late Animation<double> _buttonScaleAnimation;

  bool get buttonEnabled => !_isLoading && !_isSubmitting;

  void handleLoadingAnimationStatus(AnimationStatus status) {
    if (status == AnimationStatus.forward) {
      setState(() => _isLoading = true);
    }
    if (status == AnimationStatus.completed) {
      setState(() => _isLoading = false);
    }
  }

  void _switchAuthMode() {
    setState(() {
      _isLogin = !_isLogin;
    });
    if (_switchAuthController.status == AnimationStatus.dismissed) {
      _switchAuthController.forward();
    } else {
      _switchAuthController.reverse();
    }
  }

  @override
  void initState() {
    _loadingController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 1150),
      reverseDuration: const Duration(milliseconds: 300),
    );
    _switchAuthController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 800),
    );
    _postSwitchAuthController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 150),
    );
    _submitController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 1000),
    );
    _nameTextFieldLoadingAnimationInterval = const Interval(0, .85);
    _passTextFieldLoadingAnimationInterval = const Interval(.15, 1.0);
    _textButtonLoadingAnimationInterval =
        const Interval(.6, 1.0, curve: Curves.easeOut);
    _buttonScaleAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(
        parent: _loadingController,
        curve: const Interval(.4, 1.0, curve: Curves.easeOutBack),
      ),
    );
    _nameController = TextEditingController(text: '');
    _passController = TextEditingController(text: '');
    _confirmPassController = TextEditingController(text: '');

    super.initState();

    Future.delayed(const Duration(seconds: 1), () {
      if (mounted) {
        _loadingController.forward();
      }
    });
  }

  @override
  void dispose() {
    _loadingController.dispose();
    _confirmPasswordFocusNode.dispose();
    _passwordFocusNode.dispose();
    _postSwitchAuthController.dispose();
    _submitController.dispose();
    _switchAuthController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // var screenWidth = MediaQuery.of(context).size.width;
    // var screenHeight = MediaQuery.of(context).size.height;
    final theme = Theme.of(context);
    final cardWidth = min(MediaQuery.of(context).size.width * 0.75, 400.0);
    const cardPadding = 16.0;
    final textFieldWidth = cardWidth - cardPadding * 2;
    final authForm = Form(
      key: _formKey,
      child: Column(
        children: [
          Container(
            padding: const EdgeInsets.only(
              left: cardPadding,
              right: cardPadding,
              top: cardPadding + 10,
            ),
            width: cardWidth,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                _buildUserField(textFieldWidth),
                const SizedBox(height: 20),
                _buildPasswordField(textFieldWidth),
                const SizedBox(height: 10),
              ],
            ),
          ),
          Hidable(
            backgroundColor: _switchAuthController.isCompleted
                ? null
                : theme.colorScheme.secondary,
            controller: _switchAuthController,
            initialState:
                _isLogin ? HidableState.shrunk : HidableState.expanded,
            alignment: Alignment.topLeft,
            color: theme.cardTheme.color,
            width: cardWidth,
            padding: const EdgeInsets.symmetric(horizontal: cardPadding),
            onExpandCompleted: () => _postSwitchAuthController.forward(),
            child: Column(children: [
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10.0),
                child: _buildConfirmPasswordField(textFieldWidth),
              ),
              for (var e in termsOfService)
                TermCheckbox(
                  termOfService: e,
                  validation: true,
                ),
            ]),
          ),
          Container(
            padding: EdgeInsets.only(
              right: cardPadding,
              bottom: cardPadding,
              left: cardPadding,
            ),
            width: cardWidth,
            child: Column(
              children: <Widget>[
                !widget.hideForgotPasswordButton
                    ? _buildForgotPassword(theme)
                    : SizedBox.fromSize(
                        size: const Size.fromHeight(16),
                      ),
                _buildSubmitButton(theme),
                !widget.hideSignUpButton
                    ? _buildSwitchAuthButton(theme)
                    : SizedBox.fromSize(
                        size: const Size.fromHeight(10),
                      ),
              ],
            ),
          ),
        ],
      ),
    );
    return FittedBox(
      child: Card(
        elevation: 0,
        child: authForm,
      ),
    );
  }

  Widget _buildUserField(
    double width,
  ) {
    return AnimatedTextFormField(
      controller: _nameController,
      width: width,
      loadingController: _loadingController,
      interval: _nameTextFieldLoadingAnimationInterval,
      labelText: TextFieldUtils.getLabelText(widget.userType),
      autofillHints: _isSubmitting
          ? null
          : [TextFieldUtils.getAutofillHints(widget.userType)],
      prefixIcon: TextFieldUtils.getPrefixIcon(widget.userType),
      keyboardType: TextFieldUtils.getKeyboardType(widget.userType),
      textInputAction: TextInputAction.next,
      onFieldSubmitted: (value) {
        FocusScope.of(context).requestFocus(_passwordFocusNode);
      },
      onSaved: (value) {},
      enabled: !_isSubmitting,
    );
  }

  Widget _buildPasswordField(double width) {
    return AnimatedPasswordTextFormField(
      animatedWidth: width,
      loadingController: _loadingController,
      interval: _passTextFieldLoadingAnimationInterval,
      labelText: 'Password',
      autofillHints: _isSubmitting
          ? null
          : (_isLogin ? [AutofillHints.password] : [AutofillHints.newPassword]),
      controller: _passController,
      textInputAction: _isLogin ? TextInputAction.done : TextInputAction.next,
      focusNode: _passwordFocusNode,
      onFieldSubmitted: (value) {
        if (_isLogin) {
          // _submit();
        } else {
          // SignUp
          FocusScope.of(context).requestFocus(_confirmPasswordFocusNode);
        }
      },
      onSaved: (value) {},
      enabled: !_isSubmitting,
    );
  }

  Widget _buildConfirmPasswordField(double width) {
    return AnimatedPasswordTextFormField(
      animatedWidth: width,
      enabled: true,
      loadingController: _loadingController,
      inertiaController: _postSwitchAuthController,
      inertiaDirection: TextFieldInertiaDirection.right,
      labelText: 'Confirm Password',
      controller: _confirmPassController,
      textInputAction: TextInputAction.done,
      focusNode: _confirmPasswordFocusNode,
      onFieldSubmitted: (value) {
        // _submit();
      },
      onSaved: (value) {},
    );
  }

  Widget _buildForgotPassword(ThemeData theme) {
    return FadeIn(
      controller: _loadingController,
      fadeDirection: FadeDirection.bottomToTop,
      offset: .5,
      curve: _textButtonLoadingAnimationInterval,
      child: TextButton(
        onPressed: buttonEnabled
            ? () {
                // save state to populate email field on recovery card
                _formKey.currentState!.save();
                widget.onSwitchRecoveryPassword();
              }
            : null,
        child: Text(
          'Forgot Password?',
          style: theme.textTheme.bodyText2,
        ),
      ),
    );
  }

  Widget _buildSubmitButton(ThemeData theme) {
    return ScaleTransition(
      scale: _buttonScaleAnimation,
      child: AnimatedButton(
        controller: _submitController,
        text: _isLogin ? 'LOGIN' : 'SIGNUP',
        onPressed: () {},
      ),
    );
  }

  Widget _buildSwitchAuthButton(ThemeData theme) {
    return FadeIn(
      controller: _loadingController,
      offset: .5,
      curve: _textButtonLoadingAnimationInterval,
      fadeDirection: FadeDirection.topToBottom,
      child: TextButton(
        onPressed: () {
          _switchAuthMode();
        },
        style: AppTheme.textButtonDefaultStyle,
        child: AnimatedText(
          text: (!_isLogin ? 'LOGIN' : 'SIGNUP').toUpperCase(),
          textRotation: AnimatedTextRotation.down,
        ),
      ),
    );
  }
}
