import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';
import 'package:grocery_mobile/controllers/controllers.dart';
import 'package:grocery_mobile/models/product_model.dart';
import 'package:grocery_mobile/screens/details/details_screen.dart';
import 'package:grocery_mobile/widgets/product_card.dart';

import 'widgets/filter_expansion.dart';

class CategoryScreen extends StatefulWidget {
  final categoryId;

  CategoryScreen({
    Key? key,
    required this.categoryId,
  }) : super(key: key);

  @override
  State<CategoryScreen> createState() => _CategoryScreenState();
}

class _CategoryScreenState extends State<CategoryScreen> {
  final _productController = Get.find<ProductController>();
  double height = 200;

  void onItemClicked(BuildContext context, Product product) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => DetailsScreen(product: product),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(height),
        child: AnimatedContainer(
          duration: Duration(seconds: 1),
          height: height,
          child: LayoutBuilder(
            builder: (_, constraint) {
              final width = constraint.maxWidth * 8;
              return ClipRect(
                child: Stack(
                  children: <Widget>[
                    OverflowBox(
                      maxHeight: double.infinity,
                      maxWidth: double.infinity,
                      child: Container(
                        color: Colors.amber,
                        width: width,
                        height: width,
                        child: Padding(
                          padding:
                              EdgeInsets.only(bottom: width / 2 - height / 3),
                          child: const DecoratedBox(
                            decoration: BoxDecoration(
                              color: Colors.indigo,
                              shape: BoxShape.circle,
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.black54, blurRadius: 10.0)
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    Center(
                      child: Text(
                        '123456789',
                        style: const TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                          shadows: [
                            Shadow(color: Colors.black54, blurRadius: 10.0)
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              );
              // return AppBar(
              //   backgroundColor: Colors.transparent,
              //   elevation: 0,
              //   centerTitle: true,
              //   automaticallyImplyLeading: false,
              //   leading: GestureDetector(
              //     onTap: () {
              //       Navigator.pop(context);
              //     },
              //     child: Container(
              //       padding: EdgeInsets.only(left: 25),
              //       child: Icon(
              //         Icons.arrow_back_ios,
              //         color: Colors.black,
              //       ),
              //     ),
              //   ),
              //   actions: [
              //     GestureDetector(
              //       onTap: () {
              //         Navigator.push(
              //           context,
              //           MaterialPageRoute(
              //               builder: (context) => FilterExpansion()),
              //         );
              //       },
              //       child: Container(
              //         padding: EdgeInsets.only(right: 25),
              //         child: Icon(
              //           Icons.sort,
              //           color: Colors.black,
              //         ),
              //       ),
              //     ),
              //   ],
              //   title: Container(
              //     padding: EdgeInsets.symmetric(
              //       horizontal: 25,
              //     ),
              //     child: Text(
              //       "Beverages",
              //       style: TextStyle(
              //         fontWeight: FontWeight.bold,
              //         fontSize: 20,
              //       ),
              //     ),
              //   ),
              // );
            },
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: StaggeredGrid.count(
          crossAxisCount: 2,
          mainAxisSpacing: 3.0,
          crossAxisSpacing: 0.0,
          children: (_productController.state ?? []).map((product) {
            return GestureDetector(
              onTap: () {
                // onItemClicked(context, product);
                setState(() {
                  if (height == 200) {
                    height = 400;
                  } else {
                    height = 200;
                  }
                });
              },
              child: Container(
                padding: EdgeInsets.all(10),
                child: ProductCard(
                  key: Key(product.id),
                  product: product,
                  width: 175,
                  height: 250,
                ),
              ),
            );
          }).toList(),
        ),
      ),
    );
  }
}
