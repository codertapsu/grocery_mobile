import 'dart:math';

import 'package:after_layout/after_layout.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:grocery_mobile/styles/app_theme.dart';

class CheckoutScreen extends StatefulWidget {
  const CheckoutScreen({Key? key}) : super(key: key);

  @override
  State<CheckoutScreen> createState() => _CheckoutScreenState();
}

class _CheckoutScreenState extends State<CheckoutScreen>
    with AfterLayoutMixin<CheckoutScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(),
    );
  }

  @override
  void afterFirstLayout(BuildContext context) {
    Future.delayed(
      const Duration(milliseconds: 100),
      () => awesomeDialog(context),
    );
  }

  int _randomGeneratedCode() {
    Random random = new Random();
    return random.nextInt(10000000);
  }

  AwesomeDialog awesomeDialog(BuildContext context) {
    return AwesomeDialog(
      btnOkColor: Theme.of(context).primaryColor,
      context: context,
      animType: AnimType.BOTTOMSLIDE,
      headerAnimationLoop: false,
      dialogType: DialogType.SUCCES,
      autoHide: Duration(minutes: 10),
      body: Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'Your order has been placed',
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                child: Text('Check your E-mail for confirmation.'),
              ),
              SizedBox(height: 30),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 5, horizontal: 20),
                child: Text(
                  "Your Order Number is \n#" +
                      _randomGeneratedCode().toString(),
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 20),
                ),
              ),
              SizedBox(height: 20),
              Center(
                child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: SvgPicture.asset(
                    AppTheme.checkingoutSVG,
                    width: MediaQuery.of(context).size.height * 0.3 / 2,
                    height: MediaQuery.of(context).size.height * 0.3 / 2,
                  ),
                  // child: IllustrationContainer(
                  //   path: AppTheme.checkingoutSVG,
                  //   reduceSizeByHalf: true,
                  // ),
                ),
              ),
            ],
          ),
        ),
      ),
      // btnOk: _buildFancyButtonOk,
      onDissmissCallback: (type) {
        // Provider.of<ProductsOperationsController>(context, listen: false)
        //     .clearCart();
        Navigator.pop(context);
      },
    )..show();
  }
}
