import 'dart:math';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:grocery_mobile/constants/app_colors.dart';
import 'package:grocery_mobile/controllers/product_controller.dart';
import 'package:grocery_mobile/mock/products.dart';
import 'package:grocery_mobile/models/product_model.dart';
import 'package:grocery_mobile/screens/dashboard/widgets/ads_banner.dart';
import 'package:grocery_mobile/screens/details/details_screen.dart';
import 'package:grocery_mobile/widgets/product_card.dart';

class DashBoardScreen extends StatelessWidget {
  final _productController = Get.find<ProductController>();

  DashBoardScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var viewSize = MediaQuery.of(context).size;
    return Container(
      padding: const EdgeInsets.only(
        top: 5,
        bottom: 5,
      ),
      child: _productController.obx((state) {
        var products = state ?? [];
        return SingleChildScrollView(
          child: Column(
            children: [
              const AdsBanner(),
              const SizedBox(height: 10),
              _buildSubTitle('Exclusive Offers', () {}),
              _buildHorizontalItemSlider(
                viewSize,
                context,
                products,
              ),
              const SizedBox(height: 10),
              _buildSubTitle('Best Sellings', () {}),
              _buildHorizontalItemSlider(
                viewSize,
                context,
                generateDemoProduct(),
              ),
              // const SizedBox(height: 10),
              // _buildSubTitle('Groceries', () {}),
              // _buildHorizontalItemSliderFeatured(
              //   viewSize,
              //   context,
              //   generateDemoProduct(),
              // ),
              const SizedBox(height: 10),
              _buildSubTitle('Meats', () {}),
              _buildHorizontalItemSlider(
                viewSize,
                context,
                generateDemoProduct(),
              ),
            ],
          ),
        );
      }),
    );
  }

  Widget _buildSubTitle(String title, [GestureTapCallback? onTap]) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            title,
            style: const TextStyle(
              fontSize: 24,
              fontWeight: FontWeight.bold,
            ),
          ),
          InkWell(
            onTap: onTap ?? () {},
            child: const Text(
              'See all',
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
                color: AppColors.primaryColor,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildHorizontalItemSlider(
    Size viewSize,
    BuildContext context,
    List<Product> products,
  ) {
    List<Widget> list = [const SizedBox(width: 15)];
    for (int index = 0; index < products.length; index++) {
      var product = products[index];
      list.add(
        GestureDetector(
          onTap: () {
            Navigator.push(
              context,
              PageRouteBuilder(
                transitionDuration: const Duration(milliseconds: 500),
                reverseTransitionDuration: const Duration(milliseconds: 500),
                pageBuilder: (context, animation, secondaryAnimation) =>
                    FadeTransition(
                  opacity: animation,
                  child: DetailsScreen(
                    product: products[index],
                  ),
                ),
              ),
            );
          },
          child: ProductCard(
            key: Key(product.id),
            product: product,
            width: 175,
            height: 250,
          ),
        ),
      );
      list.add(const SizedBox(width: 15));
    }
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        children: list,
      ),
    );
  }

  Widget _buildHorizontalItemSliderFeatured(
    Size viewSize,
    BuildContext context,
    List<Product> products,
  ) {
    return SizedBox(
      height: viewSize.height * 0.13,
      child: ListView.separated(
        padding: const EdgeInsets.symmetric(horizontal: 15),
        scrollDirection: Axis.horizontal,
        itemCount: products.length,
        itemBuilder: (_, i) => Container(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          width: viewSize.width * 0.6,
          decoration: BoxDecoration(
            color: Colors.primaries[Random().nextInt(Colors.primaries.length)],
            borderRadius: BorderRadius.circular(15),
          ),
          child: Row(
            children: [
              Image.asset(products[i].images![0]),
              const SizedBox(width: 10),
              Text(
                products[i].title,
                style: AppColors.kTitleStyle,
              ),
            ],
          ),
        ),
        separatorBuilder: (_, __) => const SizedBox(width: 10),
      ),
    );
  }
}
