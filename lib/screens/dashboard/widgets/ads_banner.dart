import 'dart:async';
import 'dart:math';
import 'package:async/async.dart';

import 'package:flutter/material.dart';
import 'package:grocery_mobile/constants/app_sizes.dart';
import 'package:grocery_mobile/widgets/page_indicator.dart';

class AdsBanner extends StatefulWidget {
  const AdsBanner({Key? key}) : super(key: key);

  @override
  State<AdsBanner> createState() => _AdsBannerState();
}

class _AdsBannerState extends State<AdsBanner>
    with SingleTickerProviderStateMixin {
  late PageController _pageController;
  late ScrollController _scrollController;
  late TabController _tabController;
  late RestartableTimer _timer;

  final List<Widget> _banners = List.generate(
    3,
    (index) => Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15),
      child: Image.asset(
        'assets/images/vegs_banner.png',
        fit: BoxFit.fill,
      ),
    ),
  );

  void nextPage() {
    _pageController.animateToPage(
      _pageController.page!.toInt() + 1,
      duration: Duration(milliseconds: 400),
      curve: Curves.easeIn,
    );
  }

  void previousPage() {
    _pageController.animateToPage(
      _pageController.page!.toInt() - 1,
      duration: Duration(milliseconds: 400),
      curve: Curves.easeIn,
    );
  }

  @override
  void initState() {
    _tabController = TabController(vsync: this, length: _banners.length);
    _pageController = PageController(initialPage: 0, viewportFraction: 0.8);
    _scrollController = ScrollController();
    super.initState();
    var i = 0;
    // WidgetsBinding.instance!.addPostFrameCallback((_) {
    //   print('addPostFrameCallback');
    //   _timer = RestartableTimer(
    //     Duration(seconds: 2),
    //     () {
    //       print('RestartableTimer ${i++}');
    //       _timer.reset();
    //     },
    //   );
    // });
    _pageController.addListener(() {
      // print('_pageController.page: ${_pageController.page}');
    });
    _scrollController.addListener(() {
      // print('${_scrollController.position.pixels}');
    });
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
    _pageController.dispose();
    // _timer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    // return _buildBanner();
    return _buildAdsBanner();
  }

  Widget _buildAdsBanner() {
    var screenHeight = MediaQuery.of(context).size.height;
    return SizedBox(
      height: max(screenHeight * 0.2, AppSizes.adsBannerHeight),
      child: Stack(
        fit: StackFit.expand,
        children: [
          TabBarView(
            children: _banners,
            controller: _tabController,
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              margin: const EdgeInsets.only(bottom: 10),
              width: 30,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: List.generate(_banners.length, (index) {
                  return PageIndicator(
                    index: index,
                    controller: _tabController,
                  );
                }),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildAdsSlide(BuildContext context) {
    var screenWidth = MediaQuery.of(context).size.width;
    List<Widget> _banners = [];
    for (var i = 0; i < 3; i++) {
      var banner = Padding(
        padding: EdgeInsets.all(10.0),
        child: Container(
          child: Stack(
            fit: StackFit.expand,
            children: [
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(20.0)),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black38.withOpacity(0.5),
                      spreadRadius: 1.0,
                      blurRadius: 5.0,
                      offset: Offset(4.0, 4.0),
                    ),
                  ],
                ),
              ),
              ClipRRect(
                borderRadius: BorderRadius.all(
                  Radius.circular(20.0),
                ),
                child: Image.asset(
                  'assets/images/vegs_banner.png',
                  fit: BoxFit.cover,
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(20.0)),
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      Colors.transparent,
                      Colors.black,
                    ],
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(10.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Vegetables',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 25.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      'Buy fresh vegetables at your doorstep',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 15.0,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      );
      _banners.add(banner);
    }
    return Container(
      width: screenWidth,
      height: screenWidth * 9 / 16,
      child: PageView.builder(
        controller: _pageController,
        // itemCount: 100,
        itemBuilder: (_, __) {
          return Padding(
            padding: EdgeInsets.all(10.0),
            child: Container(
              child: Stack(
                fit: StackFit.expand,
                children: [
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(20.0)),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black38.withOpacity(0.5),
                          spreadRadius: 1.0,
                          blurRadius: 5.0,
                          offset: Offset(4.0, 4.0),
                        ),
                      ],
                    ),
                  ),
                  ClipRRect(
                    borderRadius: BorderRadius.all(
                      Radius.circular(20.0),
                    ),
                    child: Image.asset(
                      'assets/images/vegs_banner.png',
                      fit: BoxFit.cover,
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(20.0)),
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          Colors.transparent,
                          Colors.black,
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Vegetables',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 25.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Text(
                          'Buy fresh vegetables at your doorstep',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 15.0,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
      // child: PageView(
      //   controller: _pageController,
      //   scrollDirection: Axis.horizontal,
      //   children: banners,
      // ),
    );
  }

  Widget _buildBanner() {
    var screenWidth = MediaQuery.of(context).size.width;
    return Container(
      width: screenWidth,
      height: screenWidth * 9 / 16,
      child: ListView.builder(
        controller: _pageController,
        scrollDirection: Axis.horizontal,
        padding: EdgeInsets.only(
          left: 15,
          right: 0,
        ),
        itemBuilder: (_, __) {
          return Container(
            margin: EdgeInsets.only(right: 15),
            width: screenWidth - 45,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(28),
              color: Colors.pinkAccent,
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(28),
              child: Stack(
                children: [
                  Positioned(
                    top: -25,
                    left: -30,
                    child: Container(
                      width: 110,
                      height: 110,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Color(0xFFe0f2f1),
                      ),
                    ),
                  ),
                  Positioned(
                    bottom: -50,
                    right: -80,
                    child: Container(
                      width: 200,
                      height: 200,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Color(0xFFe0f2f1),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
