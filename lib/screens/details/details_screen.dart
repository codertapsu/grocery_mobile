import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:grocery_mobile/constants/app_colors.dart';
import 'package:grocery_mobile/controllers/cart_controller.dart';
import 'package:grocery_mobile/controllers/layout_controller.dart';
import 'package:grocery_mobile/controllers/product_controller.dart';
import 'package:grocery_mobile/helpers/color_helper.dart';
import 'package:grocery_mobile/models/product_model.dart';
import 'package:grocery_mobile/styles/app_theme.dart';
import 'package:grocery_mobile/widgets/buttons/loading_indicator_button.dart';
import 'package:grocery_mobile/widgets/buttons/primary_button.dart';
import 'package:grocery_mobile/widgets/favorite_toggle.dart';
import 'package:grocery_mobile/widgets/rating_bar.dart';

import 'widgets/cart_counter.dart';
import 'widgets/expandable.dart';
import 'widgets/header.dart';

class DetailsScreen extends StatefulWidget {
  final Product product;

  const DetailsScreen({
    Key? key,
    required this.product,
  }) : super(key: key);

  @override
  _DetailsScreenState createState() => _DetailsScreenState();
}

class _DetailsScreenState extends State<DetailsScreen> {
  final _productController = Get.find<ProductController>();
  final _cartController = Get.find<CartController>();

  late Product _product;
  late TextEditingController _textEditingController;
  late int _quantity;
  late double _rating;
  late List<Product> _recommendedProducts;
  late Timer _timerGenerateSimilarProducts;

  String _cartTag = "";
  LoadingButtonState _buttonState = LoadingButtonState.init;

  List<Product> _generateSimilarProducts() {
    final Map<String, Product> similarProductsMap = {};
    final products = _productController.state ?? [];
    final _random = Random();
    while (similarProductsMap.length != 3) {
      final product = products[_random.nextInt(products.length)];
      if (product.id == _product.id ||
          similarProductsMap.containsKey(product.id)) continue;
      similarProductsMap[product.id] = product;
    }

    return similarProductsMap.values.toList();
  }

  void _changeProduct(Product product) {
    setState(() {
      _product = product;
    });
  }

  void _backToHome() {
    // Get.back();
    Navigator.pop(context);
  }

  void _onChangeButtonState() async {
    setState(() {
      _buttonState = LoadingButtonState.loading;
    });
    await Future.delayed(Duration(seconds: 3));
    setState(() {
      _buttonState = LoadingButtonState.done;
      _cartTag = additionalTag;
    });
    await Future.delayed(Duration(milliseconds: 500));
    setState(() {
      _buttonState = LoadingButtonState.init;
    });
    _backToHome();
  }

  void _onQuantityChange(int quantity) {
    setState(() {
      _quantity = quantity;
    });
  }

  void _onAddProduct() {
    setState(() {
      _cartTag = additionalTag;
    });
    _cartController.addToCart(_product, _quantity);
    Future.delayed(Duration.zero, () {
      Navigator.pop(context);
    });
  }

  void _openReviewDialog() {
    Get.dialog(
      AlertDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20.0)),
        ),
        contentPadding: EdgeInsets.only(top: 10.0),
        // title: Text("Review"),
        content: Container(
          width: 300.0,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      "Rate",
                      style: TextStyle(fontSize: 24.0),
                    ),
                    RatingBar(
                      rating: _rating,
                      onChanged: (rating) {
                        setState(() {
                          _rating = rating;
                        });
                      },
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 5.0,
              ),
              Divider(
                color: Colors.grey,
                height: 4.0,
              ),
              Padding(
                padding: EdgeInsets.only(left: 20.0, right: 20.0),
                child: TextField(
                  decoration: InputDecoration(
                    hintText: "Add Review",
                    border: InputBorder.none,
                  ),
                  controller: _textEditingController,
                  autofocus: true,
                  onChanged: (text) {
                    TextSelection previousSelection =
                        _textEditingController.selection;
                    _textEditingController.text = text;
                    _textEditingController.selection = previousSelection;
                  },
                  maxLines: 8,
                ),
              ),
              InkWell(
                onTap: () {
                  _productController.addReview(
                    _product,
                    _textEditingController.text,
                    _rating,
                  );
                  _textEditingController.clear();
                  Get.back();
                },
                child: Container(
                  padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
                  decoration: BoxDecoration(
                    color: AppColors.primaryColor,
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(20.0),
                      bottomRight: Radius.circular(20.0),
                    ),
                  ),
                  child: Text(
                    "Rate Product",
                    style: TextStyle(color: Colors.white),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ],
          ),
        ),
        // content: Text("Please write your review"),
        // actions: [
        //   TextButton(
        //     child: Text("Cancel"),
        //     onPressed: () {
        //       Get.back();
        //     },
        //   ),
        //   TextButton(
        //     child: Text("Submit"),
        //     onPressed: () {
        //       Get.back();
        //     },
        //   ),
        // ],
      ),
      transitionDuration: Duration(milliseconds: 800),
      transitionCurve: Curves.easeInOut,
    );
  }

  @override
  void initState() {
    _product = widget.product;
    var cartItem = (_cartController.state ?? [])
        .firstWhereOrNull((item) => item.productId == _product.id);
    _quantity = cartItem != null ? cartItem.quantity : 1;
    _rating = _product.rating;
    _textEditingController = new TextEditingController();
    _textEditingController.text = 'My Initial Text';
    _recommendedProducts = _generateSimilarProducts();
    _timerGenerateSimilarProducts = Timer.periodic(Duration(seconds: 5), (_) {
      print('generate similar products');
      setState(() {
        _recommendedProducts = _generateSimilarProducts();
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    _timerGenerateSimilarProducts.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      // appBar: buildAppBar(),
      body: _buildBody(context),
      bottomNavigationBar: _buildBottomBar(),
    );
  }

  Widget _buildBottomBar() {
    return Container(
      height: 60,
      color: Colors.white,
      padding: EdgeInsets.symmetric(
        horizontal: defaultPadding,
        vertical: 5,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          FavoriteToggle(
            isFavorite: _product.isFavorite,
            onChanged: (isFavorite) {
              if (isFavorite) {
                _productController.markFavorite(_product);
              } else {
                _productController.markUnFavorite(_product);
              }
            },
          ),
          const SizedBox(width: 20),
          Expanded(
            child: PrimaryButton(
              label: "Add to cart",
              fontWeight: FontWeight.w600,
              padding: EdgeInsets.symmetric(vertical: 2),
              // trailingWidget: getButtonPriceWidget(),
              onPressed: _onAddProduct,
            ),
            // child: Center(
            //   child: LoadingIndicatorButton(
            //     label: 'Add to cart',
            //     state: _buttonState,
            //     onPressed: _onChangeButtonState,
            //   ),
            // ),
          ),
        ],
      ),
    );
  }

  Widget _buildBody(BuildContext context) {
    return SafeArea(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Header(
              product: _product,
              cartTag: _cartTag,
            ),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 15.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        _product.title,
                        style: AppColors.kTitleStyle.copyWith(fontSize: 18),
                      ),
                      IconButton(
                        onPressed: () {
                          _openReviewDialog();
                        },
                        icon: Icon(
                          Icons.rate_review_rounded,
                          color: Colors.green[300],
                        ),
                      ),
                      // RatingBar(
                      //   rating: _product.rating,
                      //   onChanged: (rating) {
                      //     _productController.updateRating(
                      //         _product, rating);
                      //   },
                      // ),
                    ],
                  ),
                  SizedBox(height: 5),
                  Text(
                    _product.description,
                    style: AppColors.kDescriptionStyle,
                  ),
                  SizedBox(height: 20),
                  Row(
                    children: [
                      CartCounter(
                        initNumber: _quantity,
                        onChange: _onQuantityChange,
                      ),
                      Spacer(),
                      Text(
                        '\$${_product.price * _quantity}',
                        style: AppColors.kTitleStyle.copyWith(fontSize: 18),
                      )
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(height: 15),
            Divider(color: AppColors.kBorderColor),
            Expandable(
              title: 'Product Details',
              child: Text('lorem'),
            ),
            Divider(color: AppColors.kBorderColor, indent: 15, endIndent: 15),
            Expandable(
              title: 'Nutrition',
              trailing: Container(
                padding: const EdgeInsets.all(4),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: AppColors.kSecondaryColor,
                ),
                child: Text('100gr'),
              ),
              child: Text('100gr'),
            ),
            Divider(color: AppColors.kBorderColor, indent: 15, endIndent: 15),
            Expandable(
              title: 'Reviews',
              trailing: RatingBar(
                rating: _product.rating,
                readonly: true,
              ),
              child: Text('100gr'),
            ),
            SizedBox(height: 20),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: Text(
                'Similar This',
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
            SizedBox(height: 10),
            _buildRecommendProduct(),
            SizedBox(height: 5),
          ],
        ),
      ),
    );
  }

  Widget _buildRecommendProduct() {
    List<Widget> list = [const SizedBox(width: 15)];
    for (int index = 0; index < _recommendedProducts.length; index++) {
      var product = _recommendedProducts[index];
      list.add(
        GestureDetector(
          onTap: () {
            _changeProduct(product);
          },
          child: Container(
            width: 110,
            height: 110,
            decoration: BoxDecoration(
              border: Border.all(
                color: ExtendedColor.fromHex('#E2E2E2'),
              ),
              borderRadius: BorderRadius.circular(20),
            ),
            child: Center(
              child: Image(
                height: 70,
                image: AssetImage(product.images![0]),
              ),
            ),
          ),
        ),
      );
      list.add(const SizedBox(width: 15));
    }
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        children: list,
      ),
    );
  }
}
