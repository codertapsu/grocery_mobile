import 'package:flutter/material.dart';
import 'package:grocery_mobile/constants/constants.dart';
import 'package:grocery_mobile/screens/details/widgets/rounded_icon_btn.dart';

class CartCounter extends StatefulWidget {
  final int? initNumber;
  final Function(int)? onChange;
  final Function? onIncrease;
  final Function? onDecrease;
  final int? minNumber;

  const CartCounter({
    Key? key,
    this.initNumber,
    this.onChange,
    this.onIncrease,
    this.onDecrease,
    this.minNumber = 0,
  }) : super(key: key);

  @override
  State<CartCounter> createState() => _CartCounterState();
}

class _CartCounterState extends State<CartCounter> {
  late int _currentCount;
  late Function _counterCallback;
  late Function _increaseCallback;
  late Function _decreaseCallback;
  late int _minNumber;

  void _increment() {
    setState(() {
      _currentCount++;
      _counterCallback(_currentCount);
      _increaseCallback();
    });
  }

  void _decrement() {
    setState(() {
      if (_currentCount > _minNumber) {
        _currentCount--;
        _counterCallback(_currentCount);
        _decreaseCallback();
      }
    });
  }

  @override
  void initState() {
    _currentCount = widget.initNumber ?? 1;
    _counterCallback = widget.onChange ?? (int number) {};
    _increaseCallback = widget.onIncrease ?? () {};
    _decreaseCallback = widget.onDecrease ?? () {};
    _minNumber = widget.minNumber ?? 0;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: Color.fromARGB(255, 192, 7, 7),
        borderRadius: BorderRadius.all(Radius.circular(40)),
      ),
      child: Row(
        children: [
          RoundIconBtn(
            iconData: Icons.remove,
            color: Colors.black38,
            press: () => _decrement(),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: defaultPadding / 4),
            child: Text(
              _currentCount.toString(),
              style: const TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w800,
              ),
            ),
          ),
          RoundIconBtn(
            iconData: Icons.add,
            press: () => _increment(),
          ),
        ],
      ),
    );
  }
}
