import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:grocery_mobile/constants/app_colors.dart';
import 'package:grocery_mobile/models/product_model.dart';

import 'cart_counter.dart';

class Details extends StatelessWidget {
  final Product product;

  const Details({
    Key? key,
    required this.product,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      // width: double.infinity,
      padding: const EdgeInsets.symmetric(horizontal: 15.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                product.title,
                style: AppColors.kTitleStyle.copyWith(fontSize: 18),
              ),
              SvgPicture.asset(
                'assets/icons/favorite.svg',
                color: AppColors.kBlackColor.withOpacity(0.7),
              ),
            ],
          ),
          SizedBox(height: 10),
          Text(
            product.description,
            style: AppColors.kDescriptionStyle,
          ),
          SizedBox(height: 20),
          Row(
            children: [
              CartCounter(),
              Spacer(),
              Text('\$${product.price}',
                  style: AppColors.kTitleStyle.copyWith(fontSize: 18))
            ],
          ),
        ],
      ),
    );
  }
}
