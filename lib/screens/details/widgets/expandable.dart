import 'package:flutter/material.dart';
import 'package:grocery_mobile/constants/app_colors.dart';

class Expandable extends StatefulWidget {
  final String title;
  final Widget child;
  final Widget? trailing;

  const Expandable({
    Key? key,
    required this.title,
    required this.child,
    this.trailing,
  }) : super(key: key);

  @override
  _ExpandableState createState() => _ExpandableState();
}

class _ExpandableState extends State<Expandable>
    with SingleTickerProviderStateMixin {
  bool _expanded = false;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15.0),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(widget.title, style: AppColors.kTitleStyle),
              Spacer(),
              if (widget.trailing != null) widget.trailing!,
              InkWell(
                child: RotatedBox(
                  quarterTurns: _expanded ? 3 : 0,
                  child: Icon(Icons.chevron_right),
                ),
                onTap: () => setState(() => _expanded = !_expanded),
              ),
            ],
          ),
          AnimatedSize(
            duration: Duration(milliseconds: 200),
            child: ConstrainedBox(
              constraints:
                  _expanded ? BoxConstraints() : BoxConstraints(maxHeight: 0),
              child: widget.child,
              // child: Text(widget.description),
            ),
          ),
        ],
      ),
    );
  }
}
