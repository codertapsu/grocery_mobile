import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:grocery_mobile/constants/app_colors.dart';
import 'package:grocery_mobile/models/product_model.dart';
import 'package:grocery_mobile/widgets/page_indicator.dart';

class Header extends StatefulWidget {
  final Product product;
  final String cartTag;

  const Header({
    Key? key,
    required this.product,
    required this.cartTag,
  }) : super(key: key);

  @override
  State<Header> createState() => _HeaderState();
}

class _HeaderState extends State<Header> with SingleTickerProviderStateMixin {
  late TabController _tabController;
  int _focusingProductImageIndex = 0;

  List<String> get _images => widget.product.images ?? [];

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: _images.length);
    _tabController.addListener(() {
      if (!_tabController.indexIsChanging) {
        setState(() {
          _focusingProductImageIndex = _tabController.index;
        });
      }
    });
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 1.1,
      child: ClipPath(
        clipper: MyClipper(),
        child: Container(
          padding:
              const EdgeInsets.only(left: 10, right: 15, top: 20, bottom: 20),
          decoration: BoxDecoration(
            color: AppColors.kSecondaryColor,
          ),
          child: LayoutBuilder(
            builder: (_, constraints) {
              return Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      InkWell(
                        onTap: () => Navigator.of(context).pop(),
                        child: Icon(
                          Icons.chevron_left,
                          color: AppColors.kBlackColor,
                          size: 30,
                        ),
                      ),
                      SvgPicture.asset('assets/icons/share.svg'),
                    ],
                  ),
                  SizedBox(
                    width: constraints.maxWidth,
                    height: constraints.maxHeight * 0.7,
                    child: TabBarView(
                      children: _buildImages(),
                      controller: _tabController,
                    ),
                  ),
                  const SizedBox(height: 15),
                  Container(
                    margin: const EdgeInsets.only(bottom: 0),
                    width: _images.length * 10,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: List.generate(_images.length, (index) {
                        return PageIndicator(
                          index: index,
                          controller: _tabController,
                        );
                      }),
                    ),
                  ),
                ],
              );
            },
          ),
        ),
      ),
    );
  }

  List<Widget> _buildImages() {
    return List.generate(
      _images.length,
      (index) {
        var additonal = index == _focusingProductImageIndex ? '' : '_$index';
        var heroTag = widget.product.id + additonal + widget.cartTag;
        return Hero(
          tag: heroTag,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: Image.asset(
              _images[index],
              fit: BoxFit.contain,
            ),
          ),
        );
      },
    );
  }
}

class MyClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path();

    path.lineTo(0, size.height - 60);
    path.quadraticBezierTo(
        size.width / 2, size.height, size.width, size.height - 60);
    path.lineTo(size.width, 0);

    path.close();
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) => false;
}
