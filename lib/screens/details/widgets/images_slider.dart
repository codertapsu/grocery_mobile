import 'dart:math';

import 'package:flutter/material.dart';
import 'package:grocery_mobile/widgets/page_indicator.dart';

class ImagesSlider extends StatefulWidget {
  final List<String> images;

  const ImagesSlider({Key? key, required this.images}) : super(key: key);

  @override
  State<ImagesSlider> createState() => _ImagesSliderState();
}

class _ImagesSliderState extends State<ImagesSlider>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;
  late List<Widget> _imageWidgets;

  // final List<Widget> _banners = List.generate(
  //   3,
  //   (index) => Padding(
  //     padding: const EdgeInsets.symmetric(horizontal: 15),
  //     child: Image.asset(
  //       'assets/images/vegs_banner.png',
  //       fit: BoxFit.fill,
  //     ),
  //   ),
  // );

  @override
  void initState() {
    super.initState();
    _imageWidgets = List.generate(
      widget.images.length,
      (index) => Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15),
        child: Image.asset(
          widget.images[index],
          fit: BoxFit.fill,
        ),
      ),
    );
    _tabController = TabController(vsync: this, length: _imageWidgets.length);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;
    var screenWidth = MediaQuery.of(context).size.width;

    return SizedBox(
      width: screenWidth,
      height: max(screenHeight * 0.5, 400),
      child: Stack(
        fit: StackFit.expand,
        children: [
          TabBarView(
            children: _imageWidgets,
            controller: _tabController,
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              margin: const EdgeInsets.only(bottom: 10),
              width: 30,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: List.generate(_imageWidgets.length, (index) {
                  return PageIndicator(
                    index: index,
                    controller: _tabController,
                  );
                }),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
