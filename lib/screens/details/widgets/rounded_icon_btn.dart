import 'package:flutter/material.dart';
import 'package:grocery_mobile/constants/constants.dart';

class RoundIconBtn extends StatelessWidget {
  final IconData iconData;
  final Color color;
  final VoidCallback press;
  final VoidCallback? longPress;

  const RoundIconBtn({
    Key? key,
    required this.iconData,
    required this.press,
    this.color = primaryColor,
    this.longPress,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      padding: EdgeInsets.zero,
      shape: const CircleBorder(),
      elevation: 0,
      color: Colors.white,
      height: 36,
      minWidth: 36,
      onPressed: press,
      onLongPress: longPress ?? () {},
      child: Icon(
        iconData,
        color: color,
      ),
    );
  }
}
