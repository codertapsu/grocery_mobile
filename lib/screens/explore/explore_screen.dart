import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:grocery_mobile/controllers/controllers.dart';
import 'package:grocery_mobile/models/category_model.dart';
import 'package:grocery_mobile/screens/category/category_screen.dart';

import 'widgets/category_card.dart';

List<Color> gridColors = const [
  Color(0xff53B175),
  Color(0xffF8A44C),
  Color(0xffF7A593),
  Color(0xffD3B0E0),
  Color(0xffFDE598),
  Color(0xffB7DFF5),
  Color(0xff836AF6),
  Color(0xffD73B77),
];

class ExploreScreen extends StatelessWidget {
  final _categoryController = Get.find<CategoryController>();

  ExploreScreen({
    Key? key,
  }) : super(key: key);

  void _onCategoryItemClicked(BuildContext context, Category category) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (BuildContext context) {
          return CategoryScreen(
            categoryId: 1,
          );
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            getHeader(),
            Expanded(
              child: Container(
                padding: EdgeInsets.all(15),
                child: getStaggeredGridView(context),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget getHeader() {
    return Column(
      children: [
        SizedBox(
          height: 20,
        ),
        Center(
          child: Text(
            "Find Products",
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        // SizedBox(
        //   height: 20,
        // ),
        // Padding(
        //   padding: EdgeInsets.symmetric(horizontal: 10),
        //   child: SearchBarWidget(),
        // ),
      ],
    );
  }

  Widget getStaggeredGridView(BuildContext context) {
    return _categoryController.obx((state) {
      var categories = state ?? [];
      var filteredCategories = categories;
      // var filteredCategories = categories.where((category) {
      //   return category.name.toLowerCase().contains("grocery");
      // });
      return GridView.count(
        crossAxisCount: 2,
        mainAxisSpacing: 10,
        crossAxisSpacing: 10,
        childAspectRatio: 0.85,
        children: filteredCategories.mapIndexed((index, item) {
          return GestureDetector(
            onTap: () {
              _onCategoryItemClicked(context, item);
            },
            child: CategoryCard(
              category: item,
              color: gridColors[index % gridColors.length],
            ),
          );
        }).toList(),
      );
    });
  }
}
