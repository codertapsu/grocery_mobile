import 'package:flutter/material.dart';
import 'package:grocery_mobile/constants/app_colors.dart';
import 'package:grocery_mobile/models/category_model.dart';

class CategoryCard extends StatelessWidget {
  final Category category;
  final Color color;

  const CategoryCard({
    Key? key,
    required this.category,
    this.color = Colors.blueAccent,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: color.withOpacity(0.1),
        borderRadius: BorderRadius.circular(18),
        border: Border.all(
          color: color.withOpacity(0.7),
          width: 2,
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            child: imageWidget(),
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Text(
                category.name,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget imageWidget() {
    return Image.asset(
      category.image,
      fit: BoxFit.contain,
      height: 120,
    );
  }
}
