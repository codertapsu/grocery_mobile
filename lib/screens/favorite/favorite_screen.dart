import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';
import 'package:grocery_mobile/controllers/controllers.dart';
import 'package:grocery_mobile/models/product_model.dart';
import 'package:grocery_mobile/screens/details/details_screen.dart';
import 'package:grocery_mobile/widgets/product_card.dart';

class FavoriteScreen extends StatelessWidget {
  final _productController = Get.find<ProductController>();
  FavoriteScreen({Key? key}) : super(key: key);

  void navigateToProductDetails(BuildContext context, Product product) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => DetailsScreen(product: product),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   backgroundColor: Colors.transparent,
      //   elevation: 0,
      //   centerTitle: true,
      //   automaticallyImplyLeading: false,
      //   leading: GestureDetector(
      //     onTap: () {
      //       Navigator.pop(context);
      //     },
      //     child: Container(
      //       padding: EdgeInsets.only(left: 25),
      //       child: Icon(
      //         Icons.arrow_back_ios,
      //         color: Colors.black,
      //       ),
      //     ),
      //   ),
      //   actions: [],
      //   title: Container(
      //     padding: EdgeInsets.symmetric(
      //       horizontal: 25,
      //     ),
      //     child: Text(
      //       "Beverages",
      //       style: TextStyle(
      //         fontWeight: FontWeight.bold,
      //         fontSize: 20,
      //         color: Colors.black87,
      //       ),
      //     ),
      //   ),
      // ),
      body: SingleChildScrollView(
        child: StaggeredGrid.count(
          crossAxisCount: 2,
          mainAxisSpacing: 3.0,
          crossAxisSpacing: 0.0,
          children: (_productController.state ?? []).map((product) {
            return GestureDetector(
              onTap: () {
                navigateToProductDetails(context, product);
              },
              child: Container(
                padding: EdgeInsets.all(10),
                child: ProductCard(
                  key: Key(product.id),
                  product: product,
                  width: 175,
                  height: 250,
                ),
              ),
            );
          }).toList(),
        ),
      ),
    );
  }
}
