import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:grocery_mobile/controllers/controllers.dart';
import 'package:grocery_mobile/models/navigation_page_model.dart';
import 'package:grocery_mobile/widgets/user_avatar.dart';

class DrawerWidget extends StatefulWidget {
  final Map<int, NavigationPage> navigationPagesMap;

  const DrawerWidget({
    Key? key,
    required this.navigationPagesMap,
  }) : super(key: key);

  @override
  State<DrawerWidget> createState() => _DrawerWidgetState();
}

class _DrawerWidgetState extends State<DrawerWidget>
    with SingleTickerProviderStateMixin {
  final _layoutController = Get.find<LayoutController>();
  final _authController = Get.find<AuthController>();

  @override
  Widget build(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;

    return SingleChildScrollView(
      child: Column(
        children: [
          _buildButton(context),
          const UserAvatar(),
          SizedBox(
            height: screenHeight * 0.02,
          ),
          _buildText(context),
          SizedBox(
            height: screenHeight * 0.02,
          ),
          buildDrawerItem(context),
          SizedBox(
            height: screenHeight * 0.02,
          ),
          ElevatedButton.icon(
            onPressed: _authController.openAuthForm,
            icon: Icon(FontAwesomeIcons.signInAlt),
            label: Text('Sign In'),
          ),
        ],
      ),
    );
  }

  Widget buildDrawerItem(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Column(
        children: widget.navigationPagesMap.entries.map(
          (entry) {
            return ListTile(
              contentPadding: const EdgeInsets.symmetric(
                horizontal: 20,
                vertical: 1,
              ),
              leading: Icon(
                entry.value.icon,
                color: Colors.white.withOpacity(0.2),
              ),
              title: Text(
                entry.value.title,
                style: const TextStyle(
                  color: Colors.white,
                ),
              ),
              onTap: () {
                _layoutController.selectMenu(entry.key);
              },
            );
          },
        ).toList(),
      ),
    );
  }

  Widget _buildButton(BuildContext context) {
    var we = MediaQuery.of(context).size.width;
    var he = MediaQuery.of(context).size.height;

    return Container(
      margin: EdgeInsets.only(top: he * 0.09, left: we * 0.15),
      width: 50,
      height: 50,
      alignment: Alignment.center,
      decoration: const BoxDecoration(
        color: Colors.grey,
        shape: BoxShape.circle,
      ),
      child: Container(
        width: 47,
        height: 47,
        alignment: Alignment.center,
        decoration: const BoxDecoration(
          shape: BoxShape.circle,
          color: Color(0xFF04123F),
        ),
        child: IconButton(
          onPressed: _layoutController.closeMenu,
          icon: const Icon(
            Icons.arrow_back_ios_outlined,
            color: Colors.white,
            size: 20,
          ),
        ),
      ),
    );
  }

  Widget _buildText(context) {
    var we = MediaQuery.of(context).size.width;
    var he = MediaQuery.of(context).size.height;

    return Container(
      margin: EdgeInsets.only(right: we * 0.4),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Joy",
            style: GoogleFonts.lato(
              fontSize: 40,
              letterSpacing: 2,
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
          Text(
            "Mitchell",
            style: GoogleFonts.lato(
              fontSize: 40,
              letterSpacing: 2,
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }
}
