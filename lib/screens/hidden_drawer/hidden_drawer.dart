import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:grocery_mobile/controllers/controllers.dart';
import 'package:grocery_mobile/models/navigation_page_model.dart';
import 'package:grocery_mobile/screens/authentication/authentication_screen.dart';
import 'package:grocery_mobile/screens/dashboard/dashboard_screen.dart';
import 'package:grocery_mobile/screens/explore/explore_screen.dart';
import 'package:grocery_mobile/screens/favorite/favorite_screen.dart';
import 'package:grocery_mobile/screens/home_page/home_page.dart';
import 'package:grocery_mobile/screens/profile/profile_screen.dart';

import 'drawer_widget.dart';

Color myColor = Color(0xff00bfa5);

class HiddenDrawer extends StatefulWidget {
  const HiddenDrawer({Key? key}) : super(key: key);

  @override
  State<HiddenDrawer> createState() => _HiddenDrawerState();
}

class _HiddenDrawerState extends State<HiddenDrawer> {
  final _layoutController = Get.find<LayoutController>();
  final _authController = Get.find<AuthController>();
  final Map<int, NavigationPage> _navigationPagesMap = {
    0: NavigationPage(
      title: 'Home',
      icon: FontAwesomeIcons.store,
      page: DashBoardScreen(),
      isSelected: true,
    ),
    1: NavigationPage(
      title: 'Explore',
      icon: FontAwesomeIcons.search,
      page: ExploreScreen(),
    ),
    2: NavigationPage(
      title: 'Favorites',
      icon: FontAwesomeIcons.solidHeart,
      page: FavoriteScreen(),
    ),
    3: NavigationPage(
      title: 'Profile',
      icon: FontAwesomeIcons.userAlt,
      page: ProfileScreen(),
    ),
  };

  late double _xOffset;
  late double _yOffset;
  late double _scaleFactor;

  Future<bool> _showExitConfirmation() async {
    bool? _result = await showGeneralDialog(
      context: context,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      barrierDismissible: false,
      barrierColor: Colors.black.withOpacity(0.5),
      transitionDuration: Duration(milliseconds: 300),
      pageBuilder: (_, __, ___) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(32.0)),
          ),
          title: Text('Exit App'),
          content: Text('Do you want to exit an App?'),
          actions: [
            ElevatedButton(
              onPressed: () => Navigator.of(context).pop(false),
              //return false when click on "NO"
              child: Text('No'),
            ),
            ElevatedButton(
              onPressed: () => Navigator.of(context).pop(true),
              //return true when click on "Yes"
              child: Text('Yes'),
            ),
          ],
        );
      },
      transitionBuilder: buildNewTransition,
    );
    return _result ?? false;
    // return await showNewDialog(
    //       context: context,
    //       builder: (BuildContext context) {
    //         return AlertDialog(
    //           title: Text('Exit App'),
    //           content: Text('Do you want to exit an App?'),
    //           actions: [
    //             ElevatedButton(
    //               onPressed: () => Navigator.of(context).pop(false),
    //               //return false when click on "NO"
    //               child: Text('No'),
    //             ),
    //             ElevatedButton(
    //               onPressed: () => Navigator.of(context).pop(true),
    //               //return true when click on "Yes"
    //               child: Text('Yes'),
    //             ),
    //           ],
    //         );
    //       },
    //     ) ??
    //     false;
  }

  void _showAuthFormDialog(BuildContext context) {
    showGeneralDialog(
      context: context,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      barrierDismissible: false,
      barrierColor: Colors.black.withOpacity(0.5),
      transitionDuration: Duration(milliseconds: 300),
      pageBuilder: (_, __, ___) {
        return Scaffold(
          backgroundColor: Colors.transparent,
          body: AuthenticationScreen(),
          floatingActionButton: ElevatedButton(
            onPressed: () {
              _authController.closeAuthForm();
              Navigator.pop(context, false);
            },
            child: Icon(Icons.close, color: Colors.white),
            style: ElevatedButton.styleFrom(
              shape: CircleBorder(),
              padding: EdgeInsets.all(20),
              primary: Colors.blue, // <-- Button color
              onPrimary: Colors.red, // <-- Splash color
            ),
          ),
        );
      },
      transitionBuilder: buildNewTransition,
    ).then((value) {
      print(value);
    });
  }

  @override
  void initState() {
    super.initState();
    _layoutController.isOpenMenu.listen((isOpen) {
      if (isOpen) {
        setState(() {
          _xOffset = 300;
          _yOffset = 70;
          _scaleFactor = 0.85;
        });
      } else {
        setState(() {
          _xOffset = 0;
          _yOffset = 0;
          _scaleFactor = 1;
        });
      }
    });
    _layoutController.closeMenu();
    _authController.isOpenAuthForm.listen((isOpen) {
      if (isOpen) {
        _showAuthFormDialog(context);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    Future<bool> showExitPopup() async {
      return await showDialog(
            //show confirm dialogue
            //the return value will be from "Yes" or "No" options
            context: context,
            builder: (context) => AlertDialog(
              title: Text('Exit App'),
              content: Text('Do you want to exit an App?'),
              actions: [
                ElevatedButton(
                  onPressed: () => Navigator.of(context).pop(false),
                  //return false when click on "NO"
                  child: Text('No'),
                ),
                ElevatedButton(
                  onPressed: () => Navigator.of(context).pop(true),
                  //return true when click on "Yes"
                  child: Text('Yes'),
                ),
              ],
            ),
          ) ??
          false; //if showDialouge had returned null, then return false
    }

    return WillPopScope(
      onWillPop: _showExitConfirmation,
      child: Scaffold(
        backgroundColor: const Color(0xFF04123F),
        body: Stack(
          children: [
            DrawerWidget(navigationPagesMap: _navigationPagesMap),
            _buildPage(),
          ],
        ),
      ),
    );
  }

  Widget _buildPage() {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 250),
      transform: Matrix4.translationValues(_xOffset, _yOffset, 0)
        ..scale(_scaleFactor),
      child: ClipRRect(
        borderRadius:
            BorderRadius.circular(_layoutController.isOpenMenu.value ? 30 : 0),
        child: HomePage(navigationPagesMap: _navigationPagesMap),
      ),
    );
  }
}

Widget buildNewTransition(
  BuildContext context,
  Animation<double> animation,
  Animation<double> secondaryAnimation,
  Widget child,
) {
  return ScaleTransition(
    scale: animation,
    // scale: CurvedAnimation(
    //   parent: animation,
    //   curve: Curves.bounceIn,
    //   reverseCurve: Curves.bounceIn,
    // ),
    child: child,
  );
}
