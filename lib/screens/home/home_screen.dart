import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:grocery_mobile/constants/constants.dart';
import 'package:grocery_mobile/controllers/cart_controller.dart';
import 'package:grocery_mobile/controllers/product_controller.dart';
import 'package:grocery_mobile/widgets/product_card.dart';

import 'widgets/cart_details_view.dart';
import 'widgets/cart_short_view.dart';
import 'widgets/home_header.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin {
  final _productController = Get.find<ProductController>();
  final _cartController = Get.find<CartController>();
  bool _isCartExpanded = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    // controller.dispose();
    super.dispose();
  }

  void _onVerticalGesture(DragUpdateDetails details) {
    if (details.primaryDelta! < -0.7) {
      setState(() {
        _isCartExpanded = false;
      });
    } else if (details.primaryDelta! > 12) {
      setState(() {
        _isCartExpanded = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        bottom: false,
        child: Container(
          color: Color(0xFFEAEAEA),
          child: AnimatedBuilder(
              animation: _cartController,
              builder: (context, _) {
                return LayoutBuilder(
                  builder: (context, BoxConstraints constraints) {
                    return Stack(
                      children: [
                        // Header
                        AnimatedPositioned(
                          duration: panelTransition,
                          top: _isCartExpanded ? 0 : -headerHeight,
                          right: 0,
                          left: 0,
                          height: headerHeight,
                          child: const HomeHeader(),
                        ),
                        AnimatedPositioned(
                          duration: panelTransition,
                          top: _isCartExpanded
                              ? headerHeight
                              : -(constraints.maxHeight -
                                  cartBarHeight * 2 -
                                  headerHeight),
                          left: 0,
                          right: 0,
                          height: constraints.maxHeight -
                              headerHeight -
                              cartBarHeight,
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                horizontal: defaultPadding),
                            decoration: const BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.only(
                                bottomLeft:
                                    Radius.circular(defaultPadding * 1.5),
                                bottomRight:
                                    Radius.circular(defaultPadding * 1.5),
                              ),
                            ),
                            child: _buildGridProducts(),
                          ),
                        ),
                        // Card Panel
                        AnimatedPositioned(
                          duration: panelTransition,
                          bottom: 0,
                          left: 0,
                          right: 0,
                          height: _isCartExpanded
                              ? cartBarHeight
                              : (constraints.maxHeight - cartBarHeight),
                          child: GestureDetector(
                            onVerticalDragUpdate: _onVerticalGesture,
                            child: Container(
                              padding: const EdgeInsets.all(defaultPadding),
                              color: const Color(0xFFEAEAEA),
                              alignment: Alignment.topLeft,
                              child: AnimatedSwitcher(
                                duration: panelTransition,
                                child: _isCartExpanded
                                    ? CardShortView()
                                    : CartDetailsView(
                                        safeHeight: screenHeight - 30,
                                      ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    );
                  },
                );
              }),
        ),
      ),
    );
  }

  Widget _buildGridProducts() {
    return _productController.obx((state) {
      var products = state ?? [];

      return GridView.builder(
        itemCount: products.length,
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          childAspectRatio: 0.75,
          mainAxisSpacing: defaultPadding,
          crossAxisSpacing: defaultPadding,
        ),
        itemBuilder: (context, index) => ProductCard(
          product: products[index],
          height: 250,
          width: 150,
          // press: () {
          //   Navigator.push(
          //     context,
          //     PageRouteBuilder(
          //       transitionDuration: const Duration(milliseconds: 500),
          //       reverseTransitionDuration: const Duration(milliseconds: 500),
          //       pageBuilder: (context, animation, secondaryAnimation) =>
          //           FadeTransition(
          //         opacity: animation,
          //         child: DetailsScreen(
          //           product: products[index],
          //         ),
          //       ),
          //     ),
          //   );
          // },
        ),
      );
    });
  }
}
