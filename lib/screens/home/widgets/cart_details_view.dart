import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:grocery_mobile/controllers/cart_controller.dart';
import 'package:grocery_mobile/controllers/product_controller.dart';
import 'package:grocery_mobile/models/cart_item_model.dart';
import 'package:grocery_mobile/models/product_model.dart';
import 'package:grocery_mobile/screens/checkout/checkout_screen.dart';
import 'package:grocery_mobile/screens/home/widgets/extra_actions_slidable.dart';
import 'package:grocery_mobile/widgets/buttons/primary_button.dart';
import 'package:grocery_mobile/widgets/checkout_item.dart';
import 'package:grocery_mobile/widgets/delivery_card.dart';

import 'cart_details_view_card.dart';

class CartDetailsView extends StatelessWidget {
  final _cartController = Get.find<CartController>();
  final _productController = Get.find<ProductController>();

  final double safeHeight;

  CartDetailsView({
    Key? key,
    required this.safeHeight,
  }) : super(key: key);

  void showSnackBar(String message) {
    Get.snackbar(
      'Success',
      message,
      icon: Icon(
        Icons.person,
        color: Colors.white,
      ),
      snackPosition: SnackPosition.BOTTOM,
      backgroundColor: Colors.green,
      borderRadius: 20,
      margin: EdgeInsets.all(15),
      colorText: Colors.white,
      duration: Duration(milliseconds: 2500),
      isDismissible: true,
      dismissDirection: DismissDirection.horizontal,
      forwardAnimationCurve: Curves.easeOutBack,
    );
  }

  void dismissSlidableItem(
    ExtraAction action,
    CartItem cartItem,
    Product product,
  ) {
    switch (action) {
      case ExtraAction.favorite:
        {
          _productController.markFavorite(product);
          showSnackBar('Chat has been added to favorites');
          break;
        }
      case ExtraAction.share:
        {
          showSnackBar('Chat has been shared');
          break;
        }
      case ExtraAction.delete:
        {
          _cartController.removeItem(cartItem.productId);
          showSnackBar('Chat has been deleted');
          break;
        }
    }
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: const NeverScrollableScrollPhysics(),
      child: Container(
        height: safeHeight,
        decoration: BoxDecoration(
          color: Colors.black54,
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(30),
            bottomRight: Radius.circular(30),
          ),
        ),
        child: Column(
          children: [
            const SizedBox(height: 25),
            const Text(
              'My Cart',
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(height: 10),
            Expanded(
              child: SingleChildScrollView(
                child: _buildCartItems(context),
              ),
            ),
            getCheckoutButton(context),
          ],
        ),
      ),
    );
  }

  Widget _buildCartItems(BuildContext context) {
    var products = _productController.state ?? [];
    return _cartController.obx((state) {
      var cart = state ?? [];
      var children = <Widget>[];
      for (var index = 0, length = cart.length; index < length; index++) {
        var cartItem = cart[index];
        var product = products.firstWhereOrNull(
              (item) => item.id == cartItem.productId,
            ) ??
            {} as Product;
        children.add(
          ExtraActionsSlidable(
            key: UniqueKey(),
            child: CheckoutItem(
              cartItem: cartItem,
              product: product,
            ),
            onDismissed: (action) {
              dismissSlidableItem(action, cartItem, product);
            },
          ),
        );
        if (index < length - 1) {
          children.add(
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 25),
              child: Divider(
                thickness: 1,
                color: Colors.white,
              ),
            ),
          );
        }
      }
      return Column(
        children: children,
      );
    });
  }

  Widget getButtonPriceWidget() {
    return Container(
      padding: EdgeInsets.all(2),
      decoration: BoxDecoration(
        color: Color(0xff489E67),
        borderRadius: BorderRadius.circular(4),
      ),
      child: Text(
        "\$12.96",
        style: TextStyle(fontWeight: FontWeight.w600),
      ),
    );
  }

  void showBottomSheet(context) {
    // showModalBottomSheet(
    //     context: context,
    //     isScrollControlled: true,
    //     backgroundColor: Colors.transparent,
    //     builder: (BuildContext bc) {
    //       return CheckoutBottomSheet();
    //     });
  }

  Widget getCheckoutButton(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
      child: PrimaryButton(
        label: "Go To Check Out",
        fontWeight: FontWeight.w600,
        padding: EdgeInsets.symmetric(vertical: 20),
        trailingWidget: getButtonPriceWidget(),
        onPressed: () {
          showBottomSheet(context);
        },
      ),
    );
  }

  Widget _buildDismissableCheckoutItem(CartItem cartItem, Product product) {
    return Dismissible(
      key: UniqueKey(),
      direction: DismissDirection.endToStart,
      onDismissed: (_) {
        _cartController.removeItem(cartItem.productId);
      },
      background: Container(
        color: Colors.redAccent,
        child: Align(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              IconButton(
                onPressed: () {
                  _cartController.removeItem(cartItem.productId);
                },
                icon: const Icon(
                  Icons.delete,
                  color: Colors.white,
                ),
              ),
              const Text(
                " Remove",
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w700,
                ),
                textAlign: TextAlign.right,
              ),
              const SizedBox(width: 20),
            ],
          ),
          alignment: Alignment.centerRight,
        ),
      ),
      child: CheckoutItem(
        cartItem: cartItem,
        product: product,
      ),
    );
  }
}
