import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:grocery_mobile/constants/constants.dart';
import 'package:grocery_mobile/controllers/cart_controller.dart';
import 'package:grocery_mobile/controllers/product_controller.dart';
import 'package:grocery_mobile/models/cart_item_model.dart';
import 'package:grocery_mobile/screens/details/details_screen.dart';
import 'package:grocery_mobile/widgets/price_widget.dart';

class CartDetailsViewCard extends StatelessWidget {
  final _productController = Get.find<ProductController>();
  // final cartController = Get.find<CartController>();

  final CartItem cartItem;

  CartDetailsViewCard({
    Key? key,
    required this.cartItem,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dismissible(
      key: Key(cartItem.hashCode.toString()),
      direction: DismissDirection.endToStart,
      onDismissed: (direction) {
        // cartController.removeItem(cartItem.product.id);
      },
      background: Container(
        color: Colors.red,
        child: Align(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: const [
              Icon(
                Icons.delete,
                color: Colors.white,
              ),
              Text(
                " Remove",
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w700,
                ),
                textAlign: TextAlign.right,
              ),
              SizedBox(width: 10),
            ],
          ),
          alignment: Alignment.centerRight,
        ),
      ),
      child: _productController.obx((state) {
        var product =
            (state ?? []).firstWhere((item) => item.id == cartItem.productId);
        return ListTile(
          contentPadding:
              const EdgeInsets.symmetric(vertical: defaultPadding / 2),
          leading: InkWell(
            child: CircleAvatar(
              radius: 25,
              backgroundColor: Colors.white,
              backgroundImage: AssetImage(product.images![0]),
            ),
            onTap: () {
              Navigator.push(
                context,
                PageRouteBuilder(
                  transitionDuration: const Duration(milliseconds: 500),
                  reverseTransitionDuration: const Duration(milliseconds: 500),
                  pageBuilder: (context, animation, secondaryAnimation) =>
                      FadeTransition(
                    opacity: animation,
                    child: DetailsScreen(product: product),
                  ),
                ),
              );
            },
          ),
          title: Text(
            product.title,
            style: Theme.of(context)
                .textTheme
                .subtitle1!
                .copyWith(fontWeight: FontWeight.bold),
          ),
          trailing: FittedBox(
            child: Row(
              children: [
                Price(amount: "20"),
                Text(
                  "  x ${cartItem.quantity}",
                  style: Theme.of(context)
                      .textTheme
                      .subtitle1!
                      .copyWith(fontWeight: FontWeight.bold),
                )
              ],
            ),
          ),
        );
      }),
    );
  }
}
