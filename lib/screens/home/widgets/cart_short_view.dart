import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:grocery_mobile/constants/constants.dart';
import 'package:grocery_mobile/controllers/cart_controller.dart';
import 'package:grocery_mobile/controllers/layout_controller.dart';
import 'package:grocery_mobile/controllers/product_controller.dart';

class CardShortView extends StatelessWidget {
  final _productController = Get.find<ProductController>();
  final _cartController = Get.find<CartController>();

  CardShortView({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(
          "Cart",
          style: Theme.of(context).textTheme.headline6,
        ),
        const SizedBox(width: defaultPadding),
        Expanded(
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: _productController.obx((state) {
              var products = state ?? [];
              return Row(
                children: List.generate(
                  _cartController.state!.length,
                  (index) {
                    var product = products.firstWhere((item) =>
                        item.id == _cartController.state![index].productId);
                    return Padding(
                      padding: const EdgeInsets.only(right: defaultPadding / 2),
                      child: Stack(
                        children: [
                          Hero(
                            tag: product.id + additionalTag,
                            child: CircleAvatar(
                              backgroundColor: Colors.white,
                              backgroundImage: AssetImage(product.images![0]),
                            ),
                          ),
                          Positioned(
                            right: 0,
                            top: 0,
                            child: CircleAvatar(
                              backgroundColor: Colors.orangeAccent,
                              radius: 8,
                              child: Text(
                                _cartController.state![index].quantity
                                    .toString(),
                                style: const TextStyle(
                                  fontSize: 6,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    );
                  },
                ),
              );
            }),
          ),
        ),
        CircleAvatar(
          backgroundColor: Colors.white,
          child: Text(
            _cartController.totalCartItems.toString(),
            style: const TextStyle(
                fontWeight: FontWeight.bold, color: primaryColor),
          ),
        )
      ],
    );
  }
}
