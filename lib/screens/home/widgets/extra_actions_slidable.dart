import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

enum ExtraAction { favorite, share, delete }

class ExtraActionsSlidable<T> extends StatelessWidget {
  final Widget child;
  final Function(ExtraAction action)? onDismissed;

  const ExtraActionsSlidable({
    Key? key,
    required this.child,
    this.onDismissed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Slidable(
        key: ValueKey(0),
        child: child,
        startActionPane: ActionPane(
          motion: const ScrollMotion(),
          children: [
            SlidableAction(
              onPressed: (_) {
                if (onDismissed != null) {
                  onDismissed!(ExtraAction.favorite);
                }
              },
              backgroundColor: Color(0xFF0392CF),
              foregroundColor: Colors.white,
              icon: Icons.favorite,
              label: 'Favorite',
            ),
            SlidableAction(
              onPressed: (_) {
                if (onDismissed != null) {
                  onDismissed!(ExtraAction.share);
                }
              },
              backgroundColor: Color(0xFF21B7CA),
              foregroundColor: Colors.white,
              icon: Icons.share,
              label: 'Share',
            ),
          ],
        ),
        endActionPane: ActionPane(
          motion: const ScrollMotion(),
          dismissible: DismissiblePane(onDismissed: () {
            if (onDismissed != null) onDismissed!(ExtraAction.delete);
          }),
          children: [
            SlidableAction(
              onPressed: (_) {
                if (onDismissed != null) {
                  onDismissed!(ExtraAction.delete);
                }
              },
              backgroundColor: Color(0xFFFE4A49),
              foregroundColor: Colors.white,
              icon: Icons.delete,
              label: 'Delete',
            ),
          ],
        ),
      );
}
