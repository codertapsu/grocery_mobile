import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:grocery_mobile/controllers/layout_controller.dart';
import 'package:grocery_mobile/models/navigation_page_model.dart';
import 'package:grocery_mobile/screens/dashboard/dashboard_screen.dart';
import 'package:grocery_mobile/widgets/custom_home_appBar.dart';
import 'package:grocery_mobile/widgets/swipe_gesture_recognizer.dart';

class HomePage extends StatefulWidget {
  final Map<int, NavigationPage> navigationPagesMap;

  const HomePage({
    Key? key,
    required this.navigationPagesMap,
  }) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final _layoutController = Get.find<LayoutController>();

  late Widget _child;
  late int _pageIndex;

  void _handleNavigationChange(int index) {
    if (widget.navigationPagesMap.containsKey(index)) {
      setState(() {
        _pageIndex = index;
        _child = AnimatedSwitcher(
          switchInCurve: Curves.easeOut,
          switchOutCurve: Curves.easeIn,
          duration: const Duration(milliseconds: 500),
          child: widget.navigationPagesMap[index]!.page,
        );
      });
    }
  }

  @override
  void initState() {
    _pageIndex = 0;
    _child = DashBoardScreen();
    _layoutController.currentMenuIndex.listen((index) {
      _handleNavigationChange(index);
    });

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        top: false,
        bottom: false,
        child: LayoutBuilder(builder: (_, constraints) {
          return Column(
            children: [
              if (_pageIndex == 0)
                CustomAppBar(
                  safeHeight: constraints.maxHeight - 20,
                ),
              Expanded(
                child: SwipeGestureRecognizer(
                  onSwipeRight: _layoutController.openMenu,
                  onSwipeLeft: _layoutController.closeMenu,
                  onTap: _layoutController.closeMenu,
                  child: _child,
                ),
              ),
            ],
          );
        }),
      ),
    );
  }
}
