import 'dart:math' as math;
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:grocery_mobile/constants/sidebar_menu.dart';
import 'package:grocery_mobile/controllers/layout_controller.dart';
import 'package:grocery_mobile/models/sidebar_menu_item_model.dart';
import 'package:grocery_mobile/screens/allocation/allocation.dart';
import 'package:grocery_mobile/widgets/sidebar_menu.dart';
import 'package:grocery_mobile/widgets/sing_out_bottom.dart';
import 'package:grocery_mobile/widgets/user_short_info.dart';
import 'package:response/response.dart';

double degToRad(double angle) {
  return -angle * (math.pi / 180);
}

var response = ResponseUI.instance;

class MasterScreen extends StatefulWidget {
  const MasterScreen({Key? key}) : super(key: key);
  // static _MasterScreenState? of(BuildContext context) =>
  //     context.findAncestorStateOfType<_MasterScreenState>();
  @override
  _MasterScreenState createState() => _MasterScreenState();
}

class _MasterScreenState extends State<MasterScreen>
    with SingleTickerProviderStateMixin {
  final _menuController = Get.find<LayoutController>();

  late AnimationController _layoutController;
  late Animation<double> _animation;
  late Animation<Color?> _appBarColorTransAnimation;
  static double maxSlide = window.physicalSize.width * 0.60;
  double minDragStartEdge = 60;
  double maxDragStartEdge = maxSlide - 30;
  bool _canBeDragged = false;

  void _onMenuChange(SidebarMenuItem sidebarMenuItem, int index) {
    _menuController.selectMenu(index);
  }

  void _close() => _layoutController.reverse();
  void _open() => _layoutController.forward();
  void _toggleDrawer() => _layoutController.isCompleted ? _close() : _open();

  void _onDragStart(DragStartDetails details) {
    bool isDragOpenFromLeft = _layoutController.isDismissed &&
        details.globalPosition.dx < minDragStartEdge;
    bool isDragCloseFromRight = _layoutController.isCompleted &&
        details.globalPosition.dx > maxDragStartEdge;

    _canBeDragged = isDragOpenFromLeft || isDragCloseFromRight;
  }

  void _onDragUpdate(DragUpdateDetails details) {
    if (_canBeDragged) {
      double delta = details.primaryDelta! / maxSlide;
      _layoutController.value += delta;
    }
  }

  void _onDragEnd(DragEndDetails details) {
    //I have no idea what it means, copied from Drawer
    double _kMinFlingVelocity = 365.0;

    if (_layoutController.isDismissed || _layoutController.isCompleted) {
      return;
    }
    if (details.velocity.pixelsPerSecond.dx.abs() >= _kMinFlingVelocity) {
      double visualVelocity = details.velocity.pixelsPerSecond.dx /
          MediaQuery.of(context).size.width;

      _layoutController.fling(velocity: visualVelocity);
    } else if (_layoutController.value < 0.5) {
      _close();
    } else {
      _open();
    }
  }

  @override
  void initState() {
    super.initState();
    _layoutController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 200));
    _animation = Tween<double>(begin: 0, end: 1).animate(
      CurvedAnimation(
        parent: _layoutController,
        curve: Curves.easeOut,
      ),
    );
    _appBarColorTransAnimation =
        ColorTween(begin: Colors.grey[50], end: Color(0xff416C6E))
            .animate(_layoutController);
  }

  @override
  void dispose() {
    _layoutController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;
    var screenWidth = MediaQuery.of(context).size.width;
    return WillPopScope(
      onWillPop: () async {
        if (_layoutController.isCompleted) {
          _close();
          return false;
        }
        return true;
      },
      child: Scaffold(
        appBar: PreferredSize(
          child: AnimatedBuilder(
            animation: _appBarColorTransAnimation,
            // child: child,
            builder: (BuildContext context, Widget? child) {
              return AppBar(
                backgroundColor: _appBarColorTransAnimation.value,
                brightness: _appBarColorTransAnimation.isCompleted
                    ? Brightness.dark
                    : Brightness.light,
                elevation: 0,
              );
            },
          ),
          preferredSize: Size.fromHeight(0),
        ),
        body: SafeArea(
          bottom: false,
          child: AnimatedBuilder(
            animation: _animation,
            builder: (BuildContext context, Widget? child) {
              final double animationValue = _animation.value;
              final double initialLeftPosition = 25;
              final double contentScale = 1.0 - (0.3 * animationValue);
              final double navigationTranslation = (1 - animationValue) * -200;
              final double userAvatarTranslation = (1 - animationValue) * -350;
              return Stack(
                overflow: Overflow.visible,
                children: <Widget>[
                  // background
                  Container(
                    height: response.screenHeight,
                    width: response.screenWidth,
                    color: Color(0xff416C6E),
                  ),
                  // allocate Screen
                  Transform(
                    alignment: Alignment.centerLeft,
                    transform: Matrix4.identity()
                      ..translate(screenWidth * 0.60 * animationValue)
                      ..scale(contentScale, contentScale),
                    child: GestureDetector(
                      onHorizontalDragStart: _onDragStart,
                      onHorizontalDragUpdate: _onDragUpdate,
                      onHorizontalDragEnd: _onDragEnd,
                      onTap: () {
                        if (_layoutController.isCompleted) {
                          _close();
                        }
                      },
                      child: Container(
                        height: screenHeight,
                        width: screenWidth,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius:
                                BorderRadius.circular(30 * animationValue),
                            boxShadow: [
                              BoxShadow(
                                color: animationValue < 0.5
                                    ? Colors.transparent
                                    : Colors.black12,
                                blurRadius: 20,
                                spreadRadius: 10,
                              )
                            ]),
                        child: IgnorePointer(
                          ignoring: _layoutController.isCompleted,
                          child: AllocationPage(
                            toggleMenu: () {
                              _toggleDrawer();
                            },
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    top: screenHeight * 0.25,
                    left: initialLeftPosition,
                    child: Transform(
                      transform: Matrix4.identity()
                        ..translate(navigationTranslation),
                      child: SidebarMenu(
                        sideBarMenuItems: sideBarMenuItems,
                        onChange: _onMenuChange,
                      ),
                    ),
                  ),
                  Positioned(
                    top: 40,
                    left: initialLeftPosition,
                    child: Transform(
                      transform: Matrix4.identity()
                        ..translate(userAvatarTranslation),
                      child: UserShortInfo(),
                    ),
                  ),
                  Positioned(
                    left: initialLeftPosition,
                    bottom: 30,
                    child: Transform(
                      transform: Matrix4.identity()
                        ..translate(0.0, 100.0 * (1 - animationValue)),
                      child: const SignOutBottom(),
                    ),
                  ),
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}
