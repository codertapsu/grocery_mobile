import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:grocery_mobile/constants/app_colors.dart';
import 'package:grocery_mobile/models/profile_item.dart';
import 'package:grocery_mobile/screens/address/address_screen.dart';

var profileItems = [
  'Orders',
  'Orders',
  'Orders',
  'Orders',
  'Orders',
  'Orders',
  'Orders',
  'Orders',
];

List<Color> gridColors = const [
  Color(0xff53B175),
  Color(0xffF8A44C),
  Color(0xffF7A593),
  Color(0xffD3B0E0),
  Color(0xffFDE598),
  Color(0xffB7DFF5),
  Color(0xff836AF6),
  Color(0xffD73B77),
];

class ProfileScreen extends StatelessWidget {
  final List<ProfileItem> _menus = [
    ProfileItem(
      title: 'Orders',
      description: 'View your orders',
      imagePath: 'assets/icons/pasta.png',
      onTap: () {},
    ),
    ProfileItem(
      title: 'Addresses',
      description: 'View your addresses',
      imagePath: 'assets/icons/pasta.png',
      onTap: () {},
    ),
    ProfileItem(
      title: 'Payment',
      description: 'View your payment methods',
      imagePath: 'assets/icons/pasta.png',
      onTap: () {},
    ),
    ProfileItem(
      title: 'Settings',
      description: 'View your settings',
      imagePath: 'assets/icons/pasta.png',
      onTap: () {},
    ),
    ProfileItem(
      title: 'Logout',
      description: 'Logout from Grocery',
      imagePath: 'assets/icons/pasta.png',
      onTap: () {},
    ),
    ProfileItem(
      title: 'About',
      description: 'About Grocery',
      imagePath: 'assets/icons/pasta.png',
      onTap: () {},
    )
  ];

  ProfileScreen({Key? key}) : super(key: key);

  void _navigateToAddScreen(BuildContext context) {
    Navigator.push(
      context,
      PageRouteBuilder(
        transitionDuration: const Duration(milliseconds: 500),
        reverseTransitionDuration: const Duration(milliseconds: 500),
        pageBuilder: (context, animation, secondaryAnimation) {
          return FadeTransition(
            opacity: animation,
            child: AddressScreen(),
          );
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          child: Column(
            children: [
              Container(
                child: Text('User'),
              ),
              SizedBox(height: 20),
              Expanded(
                child: SingleChildScrollView(
                  child: _buildListCard(),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  List<Widget> getChildrenWithSeperator({
    required List<Widget> widgets,
    required Widget seperator,
    bool addToLastChild = true,
  }) {
    List<Widget> children = [];
    if (widgets.length > 0) {
      children.add(seperator);

      for (int i = 0; i < widgets.length; i++) {
        children.add(widgets[i]);

        if (widgets.length - i == 1) {
          if (addToLastChild) {
            children.add(seperator);
          }
        } else {
          children.add(seperator);
        }
      }
    }
    return children;
  }

  Widget logoutButton() {
    return Container(
      width: double.maxFinite,
      margin: EdgeInsets.symmetric(horizontal: 25),
      child: RaisedButton(
        visualDensity: VisualDensity.compact,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(18.0),
        ),
        color: Color(0xffF2F3F2),
        textColor: Colors.white,
        elevation: 0.0,
        padding: EdgeInsets.symmetric(vertical: 24, horizontal: 25),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SizedBox(
              width: 20,
              height: 20,
              child: Icon(FontAwesomeIcons.userCircle),
            ),
            Text(
              "Log Out",
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  color: AppColors.primaryColor),
            ),
            Container()
          ],
        ),
        onPressed: () {},
      ),
    );
  }

  Widget getImageHeader() {
    String imagePath = "assets/images/account_image.jpg";
    return CircleAvatar(
      radius: 5.0,
      backgroundImage: AssetImage(imagePath),
      backgroundColor: AppColors.primaryColor.withOpacity(0.7),
    );
  }

  Widget getAccountItemWidget(String profileItem) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 15),
      padding: EdgeInsets.symmetric(horizontal: 25),
      child: Row(
        children: [
          SizedBox(
            width: 20,
            height: 20,
            child: Icon(FontAwesomeIcons.userCircle),
          ),
          SizedBox(
            width: 20,
          ),
          Text(
            profileItem,
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
          ),
          Spacer(),
          Icon(Icons.arrow_forward_ios)
        ],
      ),
    );
  }

  Widget _buildListCard() {
    return Container(
      child: StaggeredGrid.count(
        crossAxisCount: 2,
        mainAxisSpacing: 2,
        crossAxisSpacing: 2,
        children: _menus.mapIndexed(_buildCardItem).toList(),
      ),
    );
  }

  Widget _buildCardItem(int index, ProfileItem item) {
    Color cardColor = gridColors[index % gridColors.length];
    return Card(
      elevation: 4.0,
      color: cardColor,
      shape: RoundedRectangleBorder(
        side: BorderSide(
          color: Colors.transparent,
          width: 0,
        ),
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: InkWell(
        borderRadius: BorderRadius.circular(20.0),
        splashColor: AppColors.primaryColor.withOpacity(0.1),
        onTap: () {},
        onLongPress: () {},
        child: Container(
          padding: EdgeInsets.all(15),
          child: Column(
            children: [
              Container(
                child: Image.asset(
                  item.imagePath,
                  height: 60,
                  fit: BoxFit.contain,
                ),
              ),
              SizedBox(height: 20),
              Text(
                item.title.toUpperCase(),
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                ),
              ),
              Text(
                item.description,
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 10),
              Icon(
                Icons.arrow_circle_right_outlined,
                color: Colors.black87,
              )
            ],
          ),
        ),
      ),
    );
  }
}
