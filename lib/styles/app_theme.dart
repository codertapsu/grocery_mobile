import 'package:flutter/material.dart';
// import 'package:google_fonts/google_fonts.dart';

class AppTheme {
  static const primaryColor = Color(0xff53B175);
  static const successColor = Color(0xff53B175);
  static const errorColor = Color(0xFFFF2020);
  static const darkGrey = Color(0xff7C7C7C);

  static Color mainScaffoldBackgroundColor = Color(0xffF7F4EF);
  static Color secondaryScaffoldColor = Colors.white;
  static Color mainOrangeColor = Color(0xffFFBC44);
  static Color mainCartBackgroundColor = Color(0xff010001);
  static Color secondaryCartBackgroundColor = Color(0xff181816);
  static String emptyCartSVG = "assets/icons/undraw_empty_cart.svg";
  static String emptyCartSVG2 = "assets/icons/undraw_heartbroken.svg";
  static String checkingoutSVG = "assets/icons/undraw_shopping_app.svg";

  static ButtonStyle textButtonDefaultStyle = TextButton.styleFrom(
    minimumSize: const Size(70, 30),
    padding: const EdgeInsets.symmetric(horizontal: 30.0, vertical: 8.0),
    alignment: Alignment.center,
  );

  static InputBorder defaultInputBorder = OutlineInputBorder(
    borderRadius: BorderRadius.circular(50.0),
    borderSide: const BorderSide(
      width: 1,
      style: BorderStyle.solid,
      color: darkGrey,
    ),
  );
}

const defaultPadding = 20.0;
const cartBarHeight = 100.0;
const headerHeight = 85.0;

const bgColor = Color(0xFFF6F5F2);
const primaryColor = Color(0xFF40A944);

var AppLightTheme = ThemeData(
  brightness: Brightness.light,
  // textTheme: GoogleFonts.poppinsTextTheme(Theme.of(context).textTheme),
  elevatedButtonTheme: ElevatedButtonThemeData(
    style: TextButton.styleFrom(
      padding: EdgeInsets.all(defaultPadding * 0.75),
      shape: StadiumBorder(),
      backgroundColor: primaryColor,
    ),
  ),
  textButtonTheme: TextButtonThemeData(
    style: TextButton.styleFrom(
      minimumSize: Size(
        70,
        30,
      ),
      padding: EdgeInsets.symmetric(
        horizontal: 30.0,
        vertical: 8.0,
      ),
      alignment: Alignment.center,
      backgroundColor: Colors.transparent,
    ),
  ),
);
