import 'package:flutter/material.dart';

class AnimatedAppBar extends StatelessWidget {
  String title;
  double height;

  AnimatedAppBar({
    Key? key,
    this.title = '',
    this.height = 200,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PreferredSize(
      preferredSize: Size(height, height),
      child: AnimatedContainer(
        duration: const Duration(seconds: 1),
        height: height,
        child: LayoutBuilder(builder: (context, constraint) {
          final width = constraint.maxWidth * 8;
          return ClipRect(
            child: Stack(
              children: <Widget>[
                OverflowBox(
                  maxHeight: double.infinity,
                  maxWidth: double.infinity,
                  child: SizedBox(
                    width: width,
                    height: width,
                    child: Padding(
                      padding: EdgeInsets.only(bottom: width / 2 - height / 3),
                      child: const DecoratedBox(
                        decoration: BoxDecoration(
                          color: Colors.indigo,
                          shape: BoxShape.circle,
                          boxShadow: [
                            BoxShadow(color: Colors.black54, blurRadius: 10.0)
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Center(
                  child: Text(
                    title,
                    style: const TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                      shadows: [
                        Shadow(color: Colors.black54, blurRadius: 10.0)
                      ],
                    ),
                  ),
                ),
              ],
            ),
          );
        }),
      ),
    );
  }
}
