import 'package:flutter/material.dart';
import 'package:grocery_mobile/constants/app_colors.dart';

enum LoadingButtonState {
  init,
  loading,
  done,
  error,
}

class LoadingIndicatorButton extends StatefulWidget {
  final String label;
  final double height;
  final FontWeight fontWeight;
  final LoadingButtonState? state;
  final Function? onPressed;

  LoadingIndicatorButton({
    Key? key,
    required this.label,
    this.height = 50,
    this.fontWeight = FontWeight.normal,
    this.state,
    this.onPressed,
  }) : super(key: key);

  @override
  State<LoadingIndicatorButton> createState() => _LoadingIndicatorButtonState();
}

class _LoadingIndicatorButtonState extends State<LoadingIndicatorButton> {
  bool _isAnimating = true;
  late LoadingButtonState _state;

  @override
  void initState() {
    super.initState();
    _state = widget.state ?? LoadingButtonState.init;
  }

  @override
  void didUpdateWidget(old) {
    super.didUpdateWidget(old);
    // print('didUpdateWidget: ${widget.state} - ${old.state}');
    setState(() {
      _state = widget.state ?? LoadingButtonState.init;
    });
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = widget.height;
    final isDone = _state == LoadingButtonState.done;
    final isStretched = _isAnimating || _state == LoadingButtonState.init;

    return AnimatedContainer(
      duration: const Duration(milliseconds: 400),
      curve: Curves.easeIn,
      width: _state == LoadingButtonState.init ? width : height,
      onEnd: () {
        setState(() {
          _isAnimating = !_isAnimating;
        });
      },
      height: height,
      child: isStretched
          ? _buildButton(height)
          : _buildSmallButton(isDone, height),
    );
  }

  Widget _buildButton(double height) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        elevation: 0.0,
        textStyle: TextStyle(
          color: Colors.white,
          fontSize: 18,
          fontWeight: widget.fontWeight,
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(height / 2),
        ),
        visualDensity: VisualDensity.compact,
        minimumSize: Size.fromHeight(height),
        primary: AppColors.primaryColor,
      ),
      onPressed: () {
        if (widget.onPressed != null &&
            widget.state == LoadingButtonState.init) {
          widget.onPressed!();
        }
      },
      child: FittedBox(
        child: Text(
          widget.label,
          textAlign: TextAlign.center,
        ),
      ),
    );
  }

  Widget _buildSmallButton(bool isDone, double height) {
    var color = AppColors.primaryColor;
    var icon = Icon(
      Icons.done,
      size: (height / 2).roundToDouble(),
      color: Colors.white,
    );
    if (_state == LoadingButtonState.error) {
      color = AppColors.errorColor;
      icon = Icon(
        Icons.error,
        size: (height / 2).roundToDouble(),
        color: Colors.white,
      );
    }
    return Container(
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: color,
      ),
      child: Center(
        child: AnimatedSwitcher(
          duration: const Duration(milliseconds: 200),
          transitionBuilder: (child, animation) => ScaleTransition(
            scale: animation,
            child: child,
          ),
          child: isDone
              ? icon
              : const CircularProgressIndicator(
                  color: Colors.white,
                  strokeWidth: 3,
                ),
        ),
      ),
    );
  }
}
