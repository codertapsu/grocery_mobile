import 'package:flutter/material.dart';
import 'package:grocery_mobile/constants/app_colors.dart';

class PrimaryButton extends StatelessWidget {
  final String label;
  final double roundness;
  final FontWeight fontWeight;
  final EdgeInsets padding;
  final Widget? trailingWidget;
  final Function? onPressed;

  const PrimaryButton({
    Key? key,
    required this.label,
    this.roundness = 18,
    this.fontWeight = FontWeight.bold,
    this.padding = const EdgeInsets.symmetric(vertical: 24),
    this.trailingWidget,
    this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _buildButton();
  }

  _buildButton() {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        elevation: 0.0,
        textStyle: TextStyle(
          color: Colors.white,
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(roundness),
        ),
        visualDensity: VisualDensity.compact,
        padding: padding,
        primary: AppColors.primaryColor,
      ),
      onPressed: () {
        if (onPressed != null) {
          onPressed!();
        }
      },
      child: Stack(
        fit: StackFit.passthrough,
        children: <Widget>[
          Center(
            child: Text(
              label,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 18,
                fontWeight: fontWeight,
              ),
            ),
          ),
          if (trailingWidget != null)
            Positioned(
              top: 0,
              right: 25,
              child: trailingWidget!,
            )
        ],
      ),
    );
  }
}
