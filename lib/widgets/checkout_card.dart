import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:grocery_mobile/controllers/product_controller.dart';
import 'package:grocery_mobile/models/cart_item_model.dart';
import 'dart:collection';

import 'package:grocery_mobile/models/product_model.dart';

class Checkout extends StatelessWidget {
  final UnmodifiableListView<CartItem> cart;
  final int index;

  final _productController = Get.find<ProductController>();

  Checkout({
    Key? key,
    required this.cart,
    required this.index,
  }) : super(key: key);

  String _cost(Product product) {
    double totalCost =
        double.parse(product.price.toString().replaceAll('\$', '')) *
            cart[index].quantity;
    return totalCost.toStringAsFixed(2);
  }

  @override
  Widget build(BuildContext context) {
    var screenWidth = MediaQuery.of(context).size.width;
    var screenHeight = MediaQuery.of(context).size.height;
    return Container(
      padding: EdgeInsets.only(bottom: 20),
      child: Center(
        child: _productController.obx((state) {
          var product = (state ?? [])
              .firstWhere((item) => item.id == cart[index].productId);
          return Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              CircleAvatar(
                backgroundColor: Colors.white,
                radius: 22,
                child: Image.asset(
                  product.images![0],
                  scale: 7,
                ),
              ),
              SizedBox(width: 15),
              Text(
                cart[index].quantity.toString() + '  x   ',
                textAlign: TextAlign.start,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 12,
                ),
              ),
              Container(
                width: screenWidth * 0.5,
                // color: Colors.red,
                child: Text(
                  product.title.toString(),
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                  ),
                ),
              ),
              Spacer(),
              Text(
                "\$" + _cost(product),
                textAlign: TextAlign.start,
                style: TextStyle(
                  color: Colors.white70,
                  fontSize: 15,
                ),
              ),
            ],
          );
        }),
      ),
    );
  }
}
