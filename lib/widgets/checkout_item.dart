import 'package:flutter/material.dart';
import 'package:grocery_mobile/constants/app_colors.dart';
import 'package:grocery_mobile/models/cart_item_model.dart';
import 'package:grocery_mobile/models/product_model.dart';

import 'item_counter.dart';

class CheckoutItem extends StatefulWidget {
  final CartItem cartItem;
  final Product product;

  CheckoutItem({
    Key? key,
    required this.cartItem,
    required this.product,
  }) : super(key: key);

  @override
  State<CheckoutItem> createState() => _CheckoutItemState();
}

class _CheckoutItemState extends State<CheckoutItem> {
  late int quantity;

  @override
  void initState() {
    quantity = widget.cartItem.quantity;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: 20,
        horizontal: 10,
      ),
      child: IntrinsicHeight(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Image.asset(
              widget.product.images![0],
              width: 90,
            ),
            SizedBox(width: 5),
            Flexible(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    widget.product.title,
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    widget.product.description,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.bold,
                      color: Colors.white60,
                    ),
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  Spacer(),
                  ItemCounter(
                    onAmountChanged: (newQuantity) {
                      setState(() {
                        quantity = newQuantity;
                      });
                    },
                  )
                ],
              ),
            ),
            Container(
              width: 70,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Icon(
                    Icons.close,
                    color: Colors.white,
                    size: 25,
                  ),
                  Spacer(flex: 5),
                  Text(
                    "\$${getPrice().toStringAsFixed(2)}",
                    textAlign: TextAlign.right,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Spacer(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  double getPrice() {
    return widget.product.price * quantity;
  }
}
