import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:grocery_mobile/constants/app_sizes.dart';
import 'package:grocery_mobile/controllers/cart_controller.dart';
import 'package:grocery_mobile/controllers/layout_controller.dart';
import 'package:grocery_mobile/controllers/product_controller.dart';
import 'package:grocery_mobile/screens/home/widgets/cart_details_view.dart';
import 'package:grocery_mobile/widgets/swipe_gesture_recognizer.dart';

class CustomAppBar extends StatefulWidget {
  final double safeHeight;

  CustomAppBar({
    Key? key,
    required this.safeHeight,
  }) : super(key: key);

  @override
  State<CustomAppBar> createState() => _CustomAppBarState();
}

class _CustomAppBarState extends State<CustomAppBar>
    with SingleTickerProviderStateMixin {
  final _layoutController = Get.find<LayoutController>();
  final _productController = Get.find<ProductController>();
  final _cartController = Get.find<CartController>();
  final _panelTransition = const Duration(milliseconds: 500);

  late AnimationController _menuIconController;
  late CartViewState _cartViewState;

  @override
  void initState() {
    _menuIconController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 300),
    );
    _cartViewState = _layoutController.cartViewState.value;
    _layoutController.isOpenMenu.listen((isOpen) {
      if (mounted) {
        if (isOpen) {
          _menuIconController.forward();
        } else {
          _menuIconController.reverse();
        }
      }
    });
    _layoutController.cartViewState.listen((state) {
      setState(() {
        _cartViewState = state;
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    _menuIconController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var safePadding = MediaQuery.of(context).padding.top;
    return AnimatedBuilder(
      animation: _layoutController,
      builder: (_, __) {
        return AnimatedContainer(
          height: _cartViewState == CartViewState.normal
              ? AppSizes.appBarHeight + safePadding
              : widget.safeHeight,
          curve: Curves.linear,
          duration: _panelTransition,
          child: SwipeGestureRecognizer(
            onSwipeDown: () {
              if (_cartController.state!.isNotEmpty) {
                _layoutController.changeHomeState(CartViewState.cart);
              }
            },
            onSwipeUp: () {
              _layoutController.changeHomeState(CartViewState.normal);
            },
            onSwipeLeft: _layoutController.closeMenu,
            onSwipeRight: _layoutController.openMenu,
            onTap: _layoutController.closeMenu,
            child: _layoutController.cartViewState == CartViewState.normal
                ? _buildCartShortView(safePadding)
                : CartDetailsView(safeHeight: widget.safeHeight),
          ),
        );
      },
    );
  }

  Widget _buildCartShortView(double safePadding) {
    return Container(
      padding: EdgeInsets.only(left: 20, right: 20, top: safePadding),
      decoration: BoxDecoration(
        color: Colors.red,
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          IconButton(
            padding: const EdgeInsets.all(0),
            onPressed: () {
              if (_layoutController.isOpenMenu.value) {
                _layoutController.closeMenu();
              } else {
                _layoutController.openMenu();
              }
            },
            icon: AnimatedIcon(
              icon: AnimatedIcons.menu_arrow,
              color: Colors.white,
              progress: _menuIconController,
            ),
            alignment: Alignment.centerLeft,
          ),
          Expanded(
            child: _buildPreviewCart(),
          ),
        ],
      ),
    );
  }

  Widget _buildPreviewCart() {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: _cartController.obx((state) {
        var cart = state ?? [];
        var cartLength = cart.length;
        var products = _productController.state ?? [];

        final List<Widget> list = [];
        for (int index = 0; index < cartLength; index++) {
          var product = products
              .firstWhereOrNull((item) => item.id == cart[index].productId);
          if (product != null) {
            list.add(
              Padding(
                padding: const EdgeInsets.only(right: 10),
                child: Stack(
                  children: [
                    Hero(
                      tag: '${product.id}$additionalTag',
                      child: CircleAvatar(
                        backgroundColor: Colors.white,
                        backgroundImage: AssetImage(product.images![0]),
                      ),
                    ),
                    Positioned(
                      right: 0,
                      top: 0,
                      child: CircleAvatar(
                        backgroundColor: Colors.orangeAccent,
                        radius: 8,
                        child: Text(
                          cart[index].quantity.toString(),
                          style: const TextStyle(
                            fontSize: 6,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          }
        }
        return Row(
          children: list,
        );
      }),
    );
  }
}
