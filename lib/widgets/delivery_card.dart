import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:grocery_mobile/styles/app_theme.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';

class DeliveryCard extends StatelessWidget {
  final double totalPrice;
  const DeliveryCard({Key? key, required this.totalPrice}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        CircleAvatar(
          radius: 25,
          backgroundColor: Colors.white,
          child: FaIcon(
            FontAwesomeIcons.truckMoving,
            color: AppTheme.mainOrangeColor,
            size: 20,
          ),
        ),
        SizedBox(width: 15),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            // SizedBox(height: 13),
            const Text(
              "Delivary",
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: 15,
              ),
            ),
            const SizedBox(height: 15),
            Text(
              "All orders of \$40 or more\nqualify for FREE deleivary.",
              style: TextStyle(
                color: Colors.white70,
                fontSize: 11,
              ),
            ),
            SizedBox(height: 15),
            StepProgressIndicator(
              fallbackLength: 140,
              totalSteps: 40,
              currentStep: totalPrice <= 40 ? totalPrice.round() : 40,
              size: 4,
              padding: 0,
              selectedColor: Colors.yellow,
              unselectedColor: Colors.grey,
            )
          ],
        ),
        Spacer(),
        Text(
          "\$$totalPrice",
          style: TextStyle(
            color: Colors.white70,
            fontSize: 15,
          ),
        ),
      ],
    );
  }
}
