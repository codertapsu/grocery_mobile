import 'package:flutter/material.dart';

class ExpandablePageView extends StatefulWidget {
  final List<Widget> children;

  const ExpandablePageView({
    Key? key,
    required this.children,
  }) : super(key: key);

  @override
  _ExpandablePageViewState createState() => _ExpandablePageViewState();
}

class _ExpandablePageViewState extends State<ExpandablePageView>
    with TickerProviderStateMixin {
  final _animationDuration = const Duration(milliseconds: 350);
  final _animationCurve = Curves.ease;

  late final PageController _pageController;
  late List<double> _heights;

  int _currentPage = 0;

  double get _currentHeight => _heights[_currentPage];

  void _changePage(int page) {
    print('page: ${page % widget.children.length}');
    // setState(() {
    //   _currentPage = page % widget.children.length;
    // });
  }

  @override
  void initState() {
    _heights = widget.children.map((e) => 0.0).toList();
    super.initState();
    _pageController = PageController();
    //   ..addListener(() {
    //     final _newPage = _pageController.page!.round();
    //     if (_currentPage != _newPage) {
    //       setState(() => _currentPage = _newPage);
    //     }
    //   });
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return TweenAnimationBuilder<double>(
      curve: Curves.linear,
      duration: const Duration(milliseconds: 100),
      tween: Tween<double>(begin: _heights[0], end: _currentHeight),
      builder: (context, value, child) => SizedBox(height: value, child: child),
      // child: PageView(
      //   controller: _pageController,
      //   children: _sizeReportingChildren
      //       .asMap()
      //       .map((index, child) => MapEntry(index, child))
      //       .values
      //       .toList(),
      // ),
      child: PageView.builder(
        onPageChanged: _changePage,
        itemBuilder: (_, index) {
          var scale = index == _currentPage ? 1.0 : 0.8;
          return TweenAnimationBuilder(
            duration: _animationDuration,
            tween: Tween(begin: scale, end: scale),
            curve: _animationCurve,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 15),
              color: Colors.blue,
              child: OverflowBox(
                //needed, so that parent won't impose its constraints on the children, thus skewing the measurement results.
                minHeight: 0,
                maxHeight: double.infinity,
                alignment: Alignment.topCenter,
                child: SizeReporting(
                  onSizeChange: (size) {
                    setState(() {
                      _heights[index % widget.children.length] = size.height;
                    });
                  },
                  child: Align(
                    child: widget.children[0],
                  ),
                ),
              ),
            ),
            builder: (_, value, child) {
              return Transform.scale(
                scale: value as double,
                child: child,
              );
            },
          );
        },
      ),
    );
  }

  List<Widget> get _sizeReportingChildren => widget.children
      .asMap()
      .map(
        (index, child) => MapEntry(
          index,
          OverflowBox(
            //needed, so that parent won't impose its constraints on the children, thus skewing the measurement results.
            minHeight: 0,
            maxHeight: double.infinity,
            alignment: Alignment.topCenter,
            child: SizeReporting(
              onSizeChange: (size) =>
                  setState(() => _heights[index] = size.height),
              child: Align(child: child),
            ),
          ),
        ),
      )
      .values
      .toList();
}

class SizeReporting extends StatefulWidget {
  final Widget child;
  final ValueChanged<Size> onSizeChange;

  const SizeReporting({
    Key? key,
    required this.child,
    required this.onSizeChange,
  }) : super(key: key);

  @override
  _SizeReportingState createState() => _SizeReportingState();
}

class _SizeReportingState extends State<SizeReporting> {
  Size _oldSize = Size.zero;

  void _notifySize() {
    if (mounted) {
      final size = context.size!;
      if (_oldSize != size) {
        print('old height: ${_oldSize.height} - new height: ${size.height}');
        print('old width: ${_oldSize.width} - new width: ${size.width}');
      }
      _oldSize = size;
      widget.onSizeChange(size);
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance!.addPostFrameCallback((_) => _notifySize());
    return widget.child;
  }
}
