import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class FavoriteBtn extends StatefulWidget {
  final double radius;
  final bool isFavorite;
  final Function(bool)? onChange;

  const FavoriteBtn({
    Key? key,
    this.onChange,
    this.isFavorite = false,
    this.radius = 12,
  }) : super(key: key);

  @override
  State<FavoriteBtn> createState() => _FavoriteBtnState();
}

class _FavoriteBtnState extends State<FavoriteBtn>
    with SingleTickerProviderStateMixin {
  late AnimationController _animationController;
  late Animation<Color?> _colorAnimation;
  late bool _isFavorite;
  late Function(bool) _onChange;

  @override
  void initState() {
    super.initState();
    _isFavorite = widget.isFavorite;
    _onChange = widget.onChange ?? (bool value) {};

    _animationController = AnimationController(
      duration: const Duration(milliseconds: 200),
      vsync: this,
    );

    _colorAnimation = ColorTween(
      begin: const Color(0xFFE3E2E3),
      end: const Color(0xFFC40000),
    ).animate(_animationController);

    _animationController.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        setState(() {
          _isFavorite = true;
          _onChange(_isFavorite);
        });
      }
      if (status == AnimationStatus.dismissed) {
        setState(() {
          _isFavorite = false;
          _onChange(_isFavorite);
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
        animation: _animationController,
        builder: (BuildContext context, _) {
          return InkWell(
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
            child: CircleAvatar(
              radius: widget.radius,
              backgroundColor: _colorAnimation.value,
              child: SvgPicture.asset("assets/icons/heart.svg"),
            ),
            onTap: _onTap,
          );
        });
  }

  void _onTap() {
    _isFavorite
        ? _animationController.reverse()
        : _animationController.forward();
  }
}
