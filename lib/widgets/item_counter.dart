import 'package:flutter/material.dart';
import 'package:grocery_mobile/constants/app_colors.dart';
import 'package:grocery_mobile/screens/details/widgets/rounded_icon_btn.dart';

class ItemCounter extends StatefulWidget {
  final Function onAmountChanged;

  const ItemCounter({
    Key? key,
    required this.onAmountChanged,
  }) : super(key: key);

  @override
  _ItemCounterState createState() => _ItemCounterState();
}

class _ItemCounterState extends State<ItemCounter> {
  int amount = 1;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        iconWidget(
          Icons.remove,
          iconColor: Colors.white,
          onPressed: decrementAmount,
        ),
        SizedBox(width: 5),
        Container(
          width: 30,
          child: Center(
            child: Text(
              amount.toString(),
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
                color: Colors.white,
              ),
            ),
          ),
        ),
        SizedBox(width: 5),
        iconWidget(
          Icons.add,
          iconColor: AppColors.primaryColor,
          onPressed: incrementAmount,
        )
      ],
    );
  }

  void incrementAmount() {
    setState(() {
      amount = amount + 1;
      updateParent();
    });
  }

  void decrementAmount() {
    if (amount <= 0) return;
    setState(() {
      amount = amount - 1;
      updateParent();
    });
  }

  void updateParent() {
    if (widget.onAmountChanged != null) {
      widget.onAmountChanged(amount);
    }
  }

  Widget iconWidget(
    IconData iconData, {
    Color? iconColor,
    onPressed,
  }) {
    return GestureDetector(
      onTap: () {
        if (onPressed != null) {
          onPressed();
        }
      },
      child: Container(
        height: 45,
        width: 45,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(17),
          border: Border.all(
            color: Color(0xffE2E2E2),
          ),
        ),
        child: Center(
          child: Icon(
            iconData,
            color: iconColor ?? Colors.black,
            size: 25,
          ),
        ),
      ),
    );
  }
}
