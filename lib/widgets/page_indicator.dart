import 'package:flutter/material.dart';

class PageIndicator extends StatefulWidget {
  final int index;
  final TabController controller;

  const PageIndicator({
    Key? key,
    required this.index,
    required this.controller,
  }) : super(key: key);

  @override
  State<PageIndicator> createState() => _PageIndicatorState();
}

class _PageIndicatorState extends State<PageIndicator> {
  bool _expanded = false;

  @override
  void initState() {
    super.initState();
    _expanded = widget.index == widget.controller.index;

    // add listener to tabcontroller to update page indicator size
    widget.controller.addListener(() {
      setState(() {
        _expanded = widget.index == widget.controller.index;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: const Duration(
        milliseconds: 300,
      ),
      width: _expanded ? 15 : 5,
      height: 5,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        color: _expanded ? Color(0xff53B175) : Colors.grey,
      ),
    );
  }
}
