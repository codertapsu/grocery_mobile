import 'package:flutter/material.dart';
import 'package:grocery_mobile/constants/app_colors.dart';
import 'package:grocery_mobile/constants/constants.dart';
import 'package:grocery_mobile/helpers/color_helper.dart';
import 'package:grocery_mobile/models/product_model.dart';

class ProductCard extends StatelessWidget {
  final Product product;
  final double width;
  final double height;

  ProductCard({
    Key? key,
    required this.product,
    required this.width,
    required this.height,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      padding: const EdgeInsets.symmetric(
        horizontal: defaultPadding,
        vertical: 10,
      ),
      decoration: BoxDecoration(
        color: Colors.transparent,
        border: Border.all(
          color: ExtendedColor.fromHex('#E2E2E2'),
        ),
        borderRadius: BorderRadius.circular(
          18,
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Hero(
            tag: product.id,
            child: _buildImageWidget(),
          ),
          const SizedBox(height: 5),
          Text(product.title, style: AppColors.kTitleStyle),
          Expanded(
            child: SingleChildScrollView(
              child: Text(
                product.description,
                style: AppColors.kDescriptionStyle,
              ),
            ),
          ),
          const SizedBox(height: 5),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                '\$${product.price}',
                style:
                    AppColors.kTitleStyle.copyWith(fontWeight: FontWeight.w700),
              ),
              _buildAddButton(),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildImageWidget() {
    return Image.asset(
      product.images![0],
      height: height / 2 - 20,
    );
  }

  Widget _buildAddButton() {
    return Container(
      padding: const EdgeInsets.all(7),
      decoration: BoxDecoration(
        color: AppColors.kPrimaryColor,
        borderRadius: BorderRadius.circular(12),
      ),
      child: const Icon(
        Icons.add,
        size: 20,
        color: Colors.white,
      ),
    );
    // return ElevatedButton(
    //   onPressed: () {},
    //   child: Icon(
    //     Icons.add,
    //     size: 20,
    //     color: Colors.white,
    //   ),
    //   style: ElevatedButton.styleFrom(
    //     shape: CircleBorder(),
    //     padding: EdgeInsets.zero,
    //     primary: AppColors.kPrimaryColor, // <-- Button color
    //     onPrimary: Colors.red, // <-- Splash color
    //   ),
    // );
  }
}
