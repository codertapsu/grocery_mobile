import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:grocery_mobile/controllers/product_controller.dart';
import 'package:grocery_mobile/models/cart_item_model.dart';
import 'dart:collection';

import 'package:grocery_mobile/models/product_model.dart';
import 'package:grocery_mobile/screens/checkout/checkout_screen.dart';
import 'package:grocery_mobile/styles/app_theme.dart';
import 'package:grocery_mobile/widgets/IllustraionContainer.dart';
import 'package:grocery_mobile/widgets/checkout_card.dart';
import 'package:grocery_mobile/widgets/delivery_card.dart';

class ProductsCheckout extends StatelessWidget {
  final double cartCheckoutTransitionValue;
  final UnmodifiableListView<CartItem> cart;
  final double totalPrice;

  final _productController = Get.find<ProductController>();

  ProductsCheckout({
    Key? key,
    required this.cartCheckoutTransitionValue,
    required this.cart,
    required this.totalPrice,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var finalTotalCost =
        totalPrice == 0 ? 0 : (totalPrice > 40 ? totalPrice : totalPrice + 5);
    return Stack(
      overflow: Overflow.visible,
      fit: StackFit.loose,
      children: <Widget>[
        Positioned(
          top: -cartCheckoutTransitionValue,
          left: 0,
          child: Text(
            "Cart",
            style: TextStyle(
              fontSize: 35,
              color: Colors.white,
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
        Positioned(
          left: 0,
          right: 0,
          top: 80 - cartCheckoutTransitionValue,
          child: Container(
            height: 220 + cartCheckoutTransitionValue,
            width: double.infinity,
            // color: Colors.teal,
            child: cart.length < 1
                ? Center(
                    child: Padding(
                      padding: EdgeInsets.all(15.0),
                      child: Text(
                        "You don't have any items in your Cart.\nStart shopping now, and checkout.",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 15,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  )
                : _buildListView(),
          ),
        ),
        Positioned(
          left: 0,
          bottom: cart.isNotEmpty ? 40 : 200,
          right: 0,
          child: Container(
            // color: AppTheme.mainCartBackgroundColor,
            child: Visibility(
              visible: cart.isNotEmpty,
              replacement: IllustrationContainer(
                path: AppTheme.emptyCartSVG2,
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  DeliveryCard(totalPrice: totalPrice),
                  SizedBox(height: 40),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Text(
                        "Total:",
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w300,
                          fontSize: 25,
                        ),
                      ),
                      Spacer(),
                      Text(
                        "\$${finalTotalCost..toStringAsExponential(3)}",
                        style: TextStyle(
                          color: Colors.white.withAlpha(240),
                          fontWeight: FontWeight.bold,
                          fontSize: 35,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 50),
                  _buildNextButton(context),
                  SizedBox(height: 40),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget _buildNextButton(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.push(
          context, MaterialPageRoute(builder: (context) => CheckoutScreen())),
      child: Container(
        height: 55,
        decoration: BoxDecoration(
            color: AppTheme.mainOrangeColor,
            borderRadius: BorderRadius.circular(50)),
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Place Order",
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w700,
                  color: Colors.black87,
                ),
              ),
              SizedBox(
                width: 20,
              ),
              Icon(
                Icons.arrow_forward_ios,
                color: Colors.black,
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildListView() {
    return _productController.obx((state) {
      var products = state ?? [];

      return ListView.builder(
        itemCount: cart.length,
        itemBuilder: (BuildContext context, int index) {
          var product = products
              .firstWhere((element) => element.id == cart[index].productId);
          return Dismissible(
            key: Key(product.images![0]),
            direction: DismissDirection.endToStart,
            onDismissed: (direction) {
              // Provider.of<ProductsOperationsController>(context, listen: false)
              //     .deleteFromCart(index);
              // Provider.of<ProductsOperationsController>(context, listen: false)
              //     .returnTotalCost();
            },
            background: Container(
              color: Colors.red,
              child: Align(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Icon(
                      Icons.delete,
                      color: Colors.white,
                    ),
                    Text(
                      " Delete",
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w700,
                      ),
                      textAlign: TextAlign.right,
                    ),
                    SizedBox(
                      width: 20,
                    ),
                  ],
                ),
                alignment: Alignment.centerRight,
              ),
            ),
            child: Checkout(cart: cart, index: index),
          );
        },
      );
    });
  }
}
