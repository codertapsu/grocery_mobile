import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:grocery_mobile/controllers/product_controller.dart';
import 'package:grocery_mobile/models/cart_item_model.dart';
import 'package:grocery_mobile/styles/app_theme.dart';

class CartPreview extends StatelessWidget {
  const CartPreview({
    Key? key,
    required this.transformAnimationValue,
    required this.animationValue,
    required this.cart,
  }) : super(key: key);

  final double transformAnimationValue;
  final double animationValue;
  final UnmodifiableListView<CartItem> cart;

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment(0, 0.4),
      //This the Preview Section of the Cart
      child: Transform(
        transform:
            Matrix4.translationValues(0, transformAnimationValue * 30, 0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(bottom: 10),
              child: Opacity(
                opacity: animationValue,
                child: Text(
                  "Cart  ",
                  style: TextStyle(
                    fontSize: 28,
                    color: Colors.white,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
            ),
            Expanded(
              child: Container(
                height: 50,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: cart.length,
                  itemBuilder: (context, index) {
                    return Opacity(
                      opacity: animationValue,
                      child: Transform(
                        transform: Matrix4.translationValues(
                            0,
                            -transformAnimationValue *
                                (index <= 2 ? index : 2) *
                                30,
                            0),
                        child: CartPreviewCard(
                          cart: cart,
                          index: index,
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
            Transform.scale(
              scale: animationValue,
              child: Padding(
                padding: EdgeInsets.all(5),
                child: CircleAvatar(
                  backgroundColor: AppTheme.mainOrangeColor,
                  radius: 20,
                  child: Text(
                    cart.length.toString(),
                    style: TextStyle(
                      fontSize: 16,
                      color: Colors.black,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class CartPreviewCard extends StatelessWidget {
  final UnmodifiableListView<CartItem> cart;
  final int index;
  final _productController = Get.find<ProductController>();

  CartPreviewCard({
    Key? key,
    required this.cart,
    required this.index,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _productController.obx((state) {
      var product =
          (state ?? []).firstWhere((item) => item.id == cart[index].productId);
      return Hero(
        tag: '${product.id}-name',
        child: Stack(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 3),
              child: CircleAvatar(
                backgroundColor: Colors.white,
                radius: 21,
                child: Image.asset(
                  product.images![0],
                  scale: 7,
                ),
              ),
            ),
            cart[index].quantity > 1
                ? Positioned(
                    right: 0,
                    top: 0,
                    child: CircleAvatar(
                      backgroundColor: Colors.orangeAccent,
                      radius: 8,
                      child: Text(
                        cart[index].quantity.toString(),
                        style: TextStyle(fontSize: 10, color: Colors.white),
                      ),
                    ),
                  )
                : Container(),
          ],
        ),
      );
    });
  }
}
