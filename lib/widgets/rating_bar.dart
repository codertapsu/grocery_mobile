import 'package:flutter/material.dart';

class RatingBar extends StatefulWidget {
  final Function(double)? onChanged;
  final bool allowHalfRating;
  final Color color;
  final IconData defaultIconData;
  final IconData filledIconData;
  final IconData halfFilledIconData;
  final double rating;
  final bool readonly;
  final double size;
  final double spacing;
  final int starCount;

  RatingBar({
    Key? key,
    this.onChanged,
    this.allowHalfRating = true,
    this.color = Colors.amber,
    this.defaultIconData = Icons.star_border,
    this.filledIconData = Icons.star,
    this.halfFilledIconData = Icons.star_half,
    this.rating = 0.0,
    this.readonly = false,
    this.size = 20,
    this.spacing = 0.0,
    this.starCount = 5,
  }) : super(key: key);

  @override
  State<RatingBar> createState() => _RatingBarState();
}

class _RatingBarState extends State<RatingBar> {
  late double _rating;
  late Function(double _) _onChanged;

  void _changeRating(double rating) {
    setState(() {
      _rating = rating;
    });
    _onChanged(_rating);
  }

  @override
  void initState() {
    _onChanged = widget.onChanged ?? (_) {};
    _rating = widget.rating;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Wrap(
        alignment: WrapAlignment.start,
        spacing: widget.spacing,
        children: List.generate(
          widget.starCount,
          (index) => buildStar(context, index),
        ),
      ),
    );
  }

  Widget buildStar(BuildContext context, int index) {
    Icon icon;
    if (index >= _rating) {
      icon = Icon(
        widget.defaultIconData,
        color: widget.color,
        size: widget.size,
      );
    } else if (index > _rating - (widget.allowHalfRating ? 0.5 : 1.0) &&
        index < _rating) {
      icon = Icon(
        widget.halfFilledIconData,
        color: widget.color,
        size: widget.size,
      );
    } else {
      icon = Icon(
        widget.filledIconData,
        color: widget.color,
        size: widget.size,
      );
    }

    return GestureDetector(
      onTap: () {
        if (!widget.readonly) {
          _changeRating(index + 1);
        }
      },
      onHorizontalDragUpdate: (dragDetails) {
        var box = context.findRenderObject() as RenderBox;
        var pos = box.globalToLocal(dragDetails.globalPosition);
        var index = pos.dx / widget.size;
        var newRating =
            widget.allowHalfRating ? index : index.round().toDouble();
        if (newRating > widget.starCount) {
          newRating = widget.starCount.toDouble();
        }
        if (newRating < 0) {
          newRating = 0.0;
        }
        if (!widget.readonly) {
          _changeRating(newRating);
        }
      },
      child: icon,
    );
  }
}
