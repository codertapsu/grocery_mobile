import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:grocery_mobile/models/sidebar_menu_item_model.dart';
import 'package:collection/collection.dart';

class SidebarMenu extends StatefulWidget {
  final List<SidebarMenuItem> sideBarMenuItems;
  final Function(SidebarMenuItem, int) onChange;

  const SidebarMenu({
    Key? key,
    required this.onChange,
    required this.sideBarMenuItems,
  }) : super(key: key);

  @override
  State<SidebarMenu> createState() => _SidebarMenuState();
}

class _SidebarMenuState extends State<SidebarMenu> {
  late List<SidebarMenuItem> _sideBarMenuItems;

  void _onChange(SidebarMenuItem item, int index) {
    widget.onChange(item, index);
    setState(() {
      for (int i = 0; i < _sideBarMenuItems.length; i++) {
        if (i == index) {
          _sideBarMenuItems[i].isSelected = true;
        } else {
          _sideBarMenuItems[i].isSelected = false;
        }
      }
    });
  }

  @override
  void initState() {
    _sideBarMenuItems = widget.sideBarMenuItems;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 400,
      width: 150,
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: _sideBarMenuItems
              .mapIndexed(
                (index, item) => InkWell(
                  onTap: () {
                    _onChange(item, index);
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 40),
                    child: Row(
                      children: <Widget>[
                        FaIcon(
                          item.icon,
                          color: item.isSelected
                              ? const Color(0xffEAF5F6)
                              : const Color(0xff88B3B5),
                          size: 20,
                        ),
                        const SizedBox(width: 15),
                        Text(
                          item.title,
                          style: TextStyle(
                            color: item.isSelected
                                ? const Color(0xffEAF5F6)
                                : const Color(0xff88B3B5),
                            fontWeight: FontWeight.w700,
                            fontSize: 17,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              )
              .toList(),
        ),
      ),
    );
  }
}
