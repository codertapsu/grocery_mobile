import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class SignOutBottom extends StatelessWidget {
  const SignOutBottom({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 180,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: const [
          FaIcon(
            Icons.logout,
            color: Color(0xff88B3B5),
          ),
          SizedBox(width: 15),
          Text(
            'Log out',
            style: TextStyle(
              color: Color(0xff88B3B5),
              fontSize: 17,
              fontWeight: FontWeight.w400,
            ),
          ),
        ],
      ),
    );
  }
}
