import 'package:flutter/material.dart';

class SwipeGestureRecognizer extends StatelessWidget {
  final VoidCallback? onSwipeUp;
  final VoidCallback? onSwipeDown;
  final VoidCallback? onSwipeRight;
  final VoidCallback? onSwipeLeft;
  final VoidCallback? onTap;
  final int sensitivity;
  final Widget child;

  const SwipeGestureRecognizer({
    Key? key,
    this.onSwipeUp,
    this.onSwipeDown,
    this.onSwipeRight,
    this.onSwipeLeft,
    this.onTap,
    this.sensitivity = 8,
    required this.child,
  }) : super(key: key);

  void _onVerticalGesture(DragUpdateDetails details) {
    if (details.delta.dy > sensitivity && onSwipeDown != null) {
      onSwipeDown!();
    } else if (details.delta.dy < -sensitivity && onSwipeUp != null) {
      onSwipeUp!();
    }
  }

  void _onHorizontalGesture(DragUpdateDetails details) {
    if (details.delta.dx > sensitivity && onSwipeRight != null) {
      onSwipeRight!();
    } else if (details.delta.dx < -sensitivity && onSwipeLeft != null) {
      onSwipeLeft!();
    }
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox.expand(
      child: GestureDetector(
        onVerticalDragUpdate: _onVerticalGesture,
        onHorizontalDragUpdate: _onHorizontalGesture,
        onTap: () {
          if (onTap != null) {
            onTap!();
          }
        },
        child: child,
      ),
    );
  }
}
