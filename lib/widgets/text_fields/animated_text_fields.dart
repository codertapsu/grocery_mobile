import 'package:flutter/material.dart';
import 'package:grocery_mobile/styles/app_theme.dart';

Interval _getInternalInterval(
  double start,
  double end,
  double externalStart,
  double externalEnd, [
  Curve curve = Curves.linear,
]) {
  return Interval(
    start + (end - start) * externalStart,
    start + (end - start) * externalEnd,
    curve: curve,
  );
}

class AnimatedTextField extends StatefulWidget {
  final double width;
  final double fontSize;
  final String? labelText;
  final String? placeholder;
  final TextEditingController? controller;

  AnimatedTextField({
    Key? key,
    required this.width,
    this.fontSize = 16,
    this.controller,
    this.labelText,
    this.placeholder,
  }) : super(key: key);

  @override
  State<AnimatedTextField> createState() => _AnimatedTextFieldState();
}

class _AnimatedTextFieldState extends State<AnimatedTextField>
    with TickerProviderStateMixin {
  late AnimationController _loadingController;
  late Animation<double> _sizeAnimation;
  late FocusNode _focusNode;
  late TextEditingController _textEditingController;

  void _updateSizeAnimation() {
    final interval = Interval(0, .85);
    _sizeAnimation = Tween<double>(
      begin: 60,
      end: widget.width,
    ).animate(CurvedAnimation(
      parent: _loadingController,
      curve: _getInternalInterval(
          .2, 1.0, interval.begin, interval.end, Curves.linearToEaseOut),
      reverseCurve: Curves.easeInExpo,
    ));
  }

  @override
  void didUpdateWidget(oldWidget) {
    super.didUpdateWidget(oldWidget);

    if (oldWidget.width != widget.width) {
      _updateSizeAnimation();
    }
  }

  @override
  void initState() {
    _textEditingController = widget.controller ?? TextEditingController();
    _focusNode = FocusNode();
    _loadingController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 1150),
      reverseDuration: const Duration(milliseconds: 300),
    );

    final interval = Interval(0, .85);

    _updateSizeAnimation();

    super.initState();

    Future.delayed(Duration.zero, () {
      _loadingController.forward();
    });
  }

  @override
  void dispose() {
    _loadingController.dispose();
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _sizeAnimation,
      builder: (_, child) {
        return ConstrainedBox(
          constraints: BoxConstraints.tightFor(width: _sizeAnimation.value),
          child: TextFormField(
            controller: _textEditingController,
            cursorColor: AppTheme.successColor,
            focusNode: _focusNode,
            decoration: InputDecoration(
              prefixIcon: Padding(
                padding: EdgeInsets.only(left: 10),
                child: Icon(
                  Icons.edit_location_outlined,
                  size: widget.fontSize + 10,
                  color: _focusNode.hasFocus
                      ? AppTheme.successColor
                      : AppTheme.darkGrey,
                ),
              ),
              alignLabelWithHint: true,
              labelStyle: TextStyle(
                fontSize: _focusNode.hasFocus ? 22 : widget.fontSize,
                color: _focusNode.hasFocus
                    ? AppTheme.successColor
                    : AppTheme.darkGrey,
              ),
              labelText: widget.labelText,
              hintText: widget.placeholder,
              filled: true,
              fillColor: Colors.white,
              enabledBorder: AppTheme.defaultInputBorder,
              errorBorder: AppTheme.defaultInputBorder.copyWith(
                borderSide: const BorderSide(color: AppTheme.errorColor),
              ),
              focusedBorder: AppTheme.defaultInputBorder.copyWith(
                borderSide: const BorderSide(color: AppTheme.successColor),
              ),
            ),
            style: TextStyle(
              fontSize: widget.fontSize,
            ),
            validator: (value) {
              if (value!.isEmpty) {
                return 'Please enter a name';
              }
              return null;
            },
          ),
        );
      },
    );
  }
}
