import 'package:flutter/material.dart';

const double _kFlingVelocity = 2.0;

class TwinExpandable extends StatefulWidget {
  final Widget frontLayer;
  final Widget backLayer;

  TwinExpandable({
    Key? key,
    required this.frontLayer,
    required this.backLayer,
  }) : super(key: key);

  @override
  State<TwinExpandable> createState() => _TwinExpandableState();
}

class _TwinExpandableState extends State<TwinExpandable>
    with TickerProviderStateMixin {
  late final AnimationController _controller;

  final GlobalKey _backdropKey = GlobalKey(debugLabel: 'Backdrop key');

  @override
  void didUpdateWidget(TwinExpandable old) {
    super.didUpdateWidget(old);
    // if (widget.currentIndex != old.currentIndex) {
    //   _toggleBackDropLayerVisibility();
    // } else if (!_isFrontLayerExpanded) {
    //   _controller.fling(velocity: _kFlingVelocity);
    // }
  }

  @override
  initState() {
    _controller = AnimationController(
      duration: Duration(milliseconds: 500),
      value: 1.0,
      vsync: this,
    );
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  /// gets the current expansion status of the front layer
  /// this depends on the current progress of the animation
  bool get _isFrontLayerExpanded {
    final AnimationStatus status = _controller.status;
    return status == AnimationStatus.completed ||
        status == AnimationStatus.forward;
  }

  void _toggleBackDropLayerVisibility() {
    _controller.fling(
        velocity: _isFrontLayerExpanded ? -_kFlingVelocity : _kFlingVelocity);
  }

  // our main backdrop stack
  // we need BoxConstraints to get the size of the box
  Widget _buildBackDropStack(BuildContext context, BoxConstraints constrains) {
    const double layerTitleHeight = 56.0;
    final Size layerSize = constrains.biggest;
    final double layerTop = layerSize.height - layerTitleHeight;

    Animation<RelativeRect> layerAnimation = RelativeRectTween(
      begin: RelativeRect.fromLTRB(
          0.0, layerTop, 0.0, layerTop - layerSize.height),
      end: RelativeRect.fromLTRB(0.0, 0.0, 0.0, 0.0),
    ).animate(
      _controller.view,
    );

    return Stack(
      key: _backdropKey,
      children: <Widget>[
        ExcludeSemantics(
          child: widget.backLayer,
          excluding: _isFrontLayerExpanded,
        ),
        PositionedTransition(
          rect: layerAnimation,
          child: GestureDetector(
            onVerticalDragUpdate: (details) {
              if (details.primaryDelta! > 0) {
                _controller.value -= details.primaryDelta! / layerSize.height;
              } else {
                _controller.value -= details.primaryDelta! / layerSize.height;
              }
            },
            onVerticalDragEnd: (DragEndDetails details) {
              if (details.primaryVelocity! < 0) {
                _controller.fling(
                  velocity: -details.primaryVelocity! / layerSize.height,
                );
              } else {
                _controller.fling(
                  velocity: -details.primaryVelocity! / layerSize.height,
                );
              }
            },
            child: Transform(
              alignment: FractionalOffset.center,
              transform: Matrix4.translationValues(0.0, 0.0, 0.0),
              child: Container(
                color: Colors.transparent,
                child: _FrontLayer(child: widget.frontLayer),
              ),
            ),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: _buildBackDropStack);
  }
}

class _FrontLayer extends StatelessWidget {
  final Widget child;

  const _FrontLayer({required this.child});

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 26.0,
      shape: BeveledRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(32.0),
          topRight: Radius.circular(32.0),
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(child: this.child),
        ],
      ),
    );
  }
}
