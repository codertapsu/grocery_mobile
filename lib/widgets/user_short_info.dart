import 'package:flutter/material.dart';

class UserShortInfo extends StatelessWidget {
  const UserShortInfo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 210,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          const CircleAvatar(
            backgroundImage: AssetImage('assets/images/avatar2.jpg'),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: const [
              Text(
                "Nadia Elshazlykaya",
                style: TextStyle(
                  color: Color(0xffEAF5F6),
                  fontWeight: FontWeight.w700,
                  fontSize: 10,
                ),
              ),
              Text(
                "Active Status",
                style: TextStyle(
                  color: Color(0xff88B3B5),
                  fontSize: 13,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
